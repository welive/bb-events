# Instructions for generating and translating the WeLive Project documentation

* Requirements:
    * Python 2.7
    * virtualenv


* Create and activate the virtual environment:



```
#!bash

    $ virtualenv welive-doc
    $ cd welive-doc
    $ source bin/activate
    $ git clone https://bitbucket.org/welive/welive-doc
    $ pip install -r welive-doc/requirements.txt
    $ cd welive-doc
```


* Write documentation pages in **English** in the correspondent \*.rst file.

* Compile the documentation and ensure that there are no errors:



```
#!bash

    $ make html

```

* Extract labels for the new generated documentation:



```
    $ make gettext

```

* Execute the following commands for generating translation files for other languages.



```
#!bash

    $ sphinx-intl update -p build/locale/

```

* Translate the correspondent \*.po files for your language under `source/locale/<language_code>/LC_MESSAGES` folder. You can check examples in Spanish folder.

* Commit and push changes.

* Notify to welive-all that new documentation has been published, to ensure that members from other task forces translate it to their language.

* UDEUSTO will be in charge of compiling and deploying the documentation in different languages.
