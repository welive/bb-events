Acceso
------

Cuando se entra en la web, y el usuario no se encuentra ya identificado, la
aplicación muestra al usuario la siguiente interfaz

.. image:: images/login.png

Tras pulsar el botón entrar, el sistema muestra al usuario las opciones para
realizar el login.

.. image:: images/login_2.png

tras introducir unas credenciales válidas y con privilegios de administrador,
la web muestra al usuario el listado principal de eventos.

.. image:: images/event_list.png

En la parte superior de la web se muestra que la cuenta con la que el
usuario se ha logeado en el sistema, así como el botón 'Salir' que permite que
el usuario actual salga de la aplicación.

.. image:: images/logged_in.png
