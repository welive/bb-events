Borrado de un evento
--------------------

.. warning::

	El borrado de un evento es definitivo y no puede deshacerse.

Para borrar un evento es necesario ir a la pantalla de edición del evento (ver
la sección :ref:`event-list` para más información sobre cómo editar un evento).

Durante la edición del evento, la aplicación web muestra al usuario el botón
'Borrar evento' que permite llevar a cabo el borrado del evento actual. Esto
borrará el evento completamente, tanto en el idioma principal como en cualquiera
de sus traducciones.

.. image:: images/delete_event.png
