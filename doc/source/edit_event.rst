Editar evento
-------------

Tras pulsar el botón para la edición un evento ya existente, la aplicación muestra
la pantalla de edición del evento, que contiene la misma información introducida
durante el proceso del registro del mismo (ver la sección :ref:`new-event` para
más información).

Al editar un evento, el usuario tiene la posibilidad de elegir el idioma para el
que se va a a editar su contenido.

.. image:: images/edit_event.png
