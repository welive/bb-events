Gestión de asistentes
---------------------

Tras pulsar el botón de gestión de asistentes para un evento concreto (ver la
sección :ref:`event-list` para más información sobre cómo editar un evento), la
aplicación muestra la siguiente página al usuario.

.. image:: images/attendant_management.png

La paǵina muestra los datos básicos del evento para el que se ha solicitado la
gestión de los asistentes: título, fechas de comienzo y fin y la localización
del mismo.

Por otro lado, se muestra la siguiente información relativa al evento:

  * **Máximo**: capacidad máxima del evento.
  * **Inscritos**: total de inscritos en el evento.
  * **Confirmados**: total de inscritos que han sido confirmados en el evento.
  * **Asistentes**: total de usuarios que han asistido al evento.
  * **Respuesta**: indica el total de respuesta a la encuesta que se han completado para el evento.

Además, esta página posibilita la gestión de los asistentes al evento. En
concreto permite:

  * **Categoría**: si el usuario está registrado como emprendedor o empresa.
  * **Nombre**: el nombre real del usuario, tal y como se encuentra registrado en en el sistema de WeLive.
  * **E-email**: la dirección de correo electrónico del usuario de acuerdo a su perfil en la aplicación.
  * **Tfno**: numero de teléfono del usuario de acuerdo a su perfil en la aplicación.
  * **Confirmado**: permite que el administrador indique al usuario que está aceptado en el evento.
  * **Asistente**: el administrador puede marcar al usuario indicando su asistencia al evento.
  * **Eliminar**: permitir eliminar al usuario del evento.


Por último, la página contiene varias utilidades para descargar información sobre
el evento.

.. image:: images/download_buttons.png

El botón 'Listado de inscritos' permite obtener el listado de inscritos con su
nombre y dirección de correo en formato CSV (procesable por Excel).

.. image:: images/attendant_list.png

Mientras que el botón 'Respuestas' permite obtener las respuestas proporcionadas
por los usuarios a la encuesta en formato CSV.

.. image:: images/survey_responses.png

.. note::

  El contenido de las respuestas depende de la encuesta asociada al evento. La
  imagen mostrada en esta documentación se incluye solamente como ejemplo.

Importar CSV de de datos (asistentes o respuestas) en Excel
-----------------------------------------------------------

Los siguientes pasos son necesarios:

  1. Abrir Excel “Nuevo libro blanco”.
  2. Seleccionar opción de menú superior “DATOS” y dentro del mismo “Desde texto”.
  3. Seleccionar directorio donde se descargan ficheros. Por ejemplo, con Chrome: "C:\\Users\\Diego\\Downloads".
  4. Seleccionar el fichero descargado. Por ejemplo, "asistentes-evento-4.csv".

    .. image:: images/load_file1.png

  5. Seguir los siguientes tres pasos del wizard:

    .. image:: images/load_file2.png

    a. Hacer clic en siguiente.

    .. image:: images/load_file3.png

    b. Seleccionar "coma" como separador.

    .. image:: images/load_file4.png

    c. Hacer clic en Finalizar.
