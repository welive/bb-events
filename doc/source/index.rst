BIG KLUB - Manual de usuario
============================

En este manual se detalla el uso de la aplicación web BIG KLUB para la gestión
de los eventos. Esta aplicación está dirigida a los administradores de los datos,
mientras que los usuarios acceden y hacen uso de los mismos a través de la
aplicación móvil.

.. note::

	Para poder hacer uso de la aplicación es necesario tener una cuenta en el
	sistema de WeLive con privilegios de administrador para la aplicación BIG KLUB.
	Si no dispone de una cuenta con privilegios de administrador, póngase en
	contacto con unai.aguilera@deusto.es.


Tabla de contenidos
===================

.. toctree::

	access
	list_events
	new_event
	edit_event
	event_translation
	delete_event
	manage_attendants
