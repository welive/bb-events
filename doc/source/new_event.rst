.. _new-event:

Nuevo evento
------------

Para introducir un nuevo evento en la base de datos de la aplicación, se debe
pulsar el botón 'Nuevo evento' que se encuentra en la página principal en la que
se muestra el listado de eventos disponibles.

.. image:: images/new_event.png

Aparece entonces el formulario para la introducción de los datos del nuevo evento
que se quiere registrar. La aplicación permite la introducción de los datos del
evento en castellano y en euskera, siendo el castellano el idioma inicial
obligatorio para el registro de un nuevo evento.

El formulario solicita al usuario los siguientes campos:

 * **Título**: el título principal (corto) del evento.
 * **Descripción**: descripción general del evento.
 * **Máximo asistentes**: máximo número de asistentes (0 si no hay límite).
 * **Fecha de comienzo**: fecha de comienzo del periodo en el que el evento está activo.
 * **Fecha de fin**: fecha de fin del periodo en el que el evento está activo.

La fecha de comienzo y fin del evento determinan el rango en el cual el evento se
encuentra activo. Sin embargo, la aplicación web permite que el usuario pueda
especificar, con un mayor detalle, los días de la semana y horas concretas en
las que se realiza el evento dentro del periodo general especificado anteriormente.

Pulsando sobre el desplegable 'Detalle de horas' el usuario puede introducir
varios rangos de fechas para indicar, por ejemplo, que el evento se realiza
todos los lunes de 17:00 a 18:00 y todos los jueves de 18:00 a 19:00, dentro del
rango de fechas general especificado anteriormente (del 1 al 30 de septiembre en
el caso del ejemplo mostrado).

.. image:: images/new_event_form_1.png

Además de la información anterior, el formulario solicita al administrador que
registra que introduzca los siguientes datos:

	* **URL Material**: una URL donde el usuario puede descargar documentación del curso.
	* **Localización**: una texto que identifique la localización del evento.
	* **Encuesta asociada**: este desplegable permite selecciona la encuesta asociada al evento. Para más información sobre cómo editar y consultar encuestas consultar la documentación asociada a la aplicación web de gestión correspondiente.
	* **Contenidos**: un texto libre que permite introducir una descripción más detallada de los contenidos del curso.

.. image:: images/new_event_form_2.png

Por último, para completar la información sobre un nuevo evento, la aplicación web
permite que el administrador registre las coordenadas del mismo para que puedan
ser representados en un mapa.

Así, haciendo click sobre el desplegable 'Búsqueda de coordenadas', aparece un
buscador en el que el usuario puede introducir un texto para buscar un punto en
el mapa. Para una búsqueda pueden existir varias opciones, apareciendo aquellas
que concuerdan con la búsqueda del usuario en una lista desplegable. Al
seleccionar una de las opciones disponibles, la aplicación obtiene y completa
automáticamente las coordenadas de latitud y longitud en base al punto
seleccionado.

La aplicación permite además que el usuario haga click en un punto del mapa,
actualizandose como coordenadas para el evento la posición elegida, o que estas
coordenadas sean introducidas manualmente en los campos correspondientes.

.. image:: images/new_event_form_3.png

Por último, es necesario que se guarden los cambios realizados para que el evento
quede registrado. El usuario tiene dos opciones siempre que lleva a cabo un
cambio en la información del evento: 'Guardar' o 'Volver'.

.. image:: images/return_save.png

Si existen cambios, el botón 'Guardar' registra el nuevo evento, mientras que
el botón 'Volver' cancela cualquier cambio no guardado y vuelva a la página
principal donde se listan los eventos.

Cuando existen cambios pendientes de guardar, la página de edición muestra el
siguiente mensaje al usuario.

.. image:: images/pending_changes.png

Una vez que los cambios han sido guardados correctamente tras pulsar el botón
'Guardar' y no existen cambios pendientes.

.. image:: images/all_changes_saved.png
