.. _event-list:

Listado de eventos
------------------

La página principal de la aplicación web muestra la lista de eventos registrados
y permite llevar a cabo las acciones de búsqueda, edición y consulta de la
información asociada a cada uno de los eventos.

.. image:: images/event_list.png

Por defecto, los eventos se filtrar a partir de la fecha del día actual, sin
embargo, es posible cambiar las fechas de inicio y fin del filtrado para mostrar
aquellos eventos que suceden dentro del rango indicado. Tras introducir las
fechas de inicio y fin de la búsqueda, siendo esta última opcional, es necesario
pulsar el botón 'Filtrar' para llevar a cabo la búsqueda y mostrar los eventos
coincidentes.

Por otro lado, para cada evento listado se muestran las siguiente información y
opciones:

	* **Título**: el título corto del evento.
	* **Comienzo**: la fecha de inicio del periodo del evento.
	* **Fin**: la fecha de fin del periodo del evento.
	* **Max/Inscritos/Confirmados/Asistentes**: máxima capacidad del evento, total de inscritos, confirmados y asistentes.
	* **Resp. encuestas**: muestra el número de encuestas asociadas que se han rellenado para dicho evento. Haciendo click sobre el número es posible descargar un CSV con las mismas.
	* **Editar**: haciendo click sobre el icono de edición del evento es posible editar las características del mismo, en castellano y euskera.
	* **Lista de inscritos**: permite acceder a la página de gestión de asistentes al evento.


Listado de usuarios registrados
-------------------------------

Desde la página principal se puede descargar el listado CSV con la información
de los usuarios registrados en la aplicación. Para ellos es necesario pulsar
sobre el botón "Lista de usuarios registrados".
