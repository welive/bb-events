Traducción de un evento
-----------------------

Una vez que se ha registrado un evento, es posible llevar a cabo la traducción
del mismo a otro idioma (por ejemplo, euskera). Para ello, tras guardar el nuevo
evento y volver a la pantalla principal, es necesario pulsar en editar el evento
(ver la sección :ref:`event-list` para más información sobre cómo editar un
evento).

Cuando se edita un evento ya registrado, se permite la posibilidad de elegir el
idioma para el cual se va a llevar a cabo la edición del mismo.

.. image:: images/edit_select_language.png

Si el usuario selecciona otro idioma (por ejemplo, EU), la aplicación permite
añadir una nueva para el idioma indicado. Una vez seleccionada el idioma para el
que se va a introducir la nueva traducción, la aplicación muestra al usuario el
formulario de edición de la misma.

.. image:: images/edit_translation.png

Como se trata de una traducción de un evento ya existente, no todos los campos
son editables, ya que algunos se comparten con el idioma principal (ES).
Los campos editables para las traducciones de los datos del evento a otros
idiomas son: 'Título', 'Descripción', 'Localización' y 'Contenidos'.

El resto de campos no son editables en la traducción y deben editarse para el
idioma principal.
