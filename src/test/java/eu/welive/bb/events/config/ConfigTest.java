/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.events.config;

import static org.junit.Assert.assertEquals;
import static org.powermock.api.mockito.PowerMockito.when;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import eu.welive.bb.events.serialization.Event.Language;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ Config.class, Properties.class })
public class ConfigTest {

	private Properties p;
	
	@Before
	public void setUp() throws Exception {
		PowerMockito.mockStatic(Properties.class);
		
		p = PowerMockito.mock(Properties.class);
		PowerMockito.whenNew(Properties.class)
			.withNoArguments()
			.thenReturn(p);
		
		when(p.getProperty("apipath"))
			.thenReturn("/api");
		when(p.getProperty("weliveapi"))
			.thenReturn("http://test.welive.eu/dev/api");
		when(p.getProperty("aacserviceurl"))
			.thenReturn("https://test.welive.eu/aac");
		when(p.getProperty("clientid"))
			.thenReturn("aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee");
		when(p.getProperty("clientsecret"))
			.thenReturn("aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee");
		when(p.getProperty("clientcredentialstoken"))
			.thenReturn("aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee");
		when(p.getProperty("baseurl"))
			.thenReturn("http://localhost:8090/bb-events");
		when(p.getProperty("clientflowtoken"))
			.thenReturn("aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee");
		when(p.getProperty("clientflowtoken"))
			.thenReturn("aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee");
		when(p.getProperty("clientflowtoken"))
			.thenReturn("aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee");
		when(p.getProperty("homepath"))
			.thenReturn("/index");
		when(p.getProperty("bbsurveyurl"))
			.thenReturn("http://bb-survey.cloudfoundry.welive.eu");
		when(p.getProperty("adminusers"))
			.thenReturn("10,14");
		when(p.getProperty("defaultlang"))
			.thenReturn("ES");
		when(p.getProperty("dataset"))
			.thenReturn("somedataset");
		when(p.getProperty("resource"))
			.thenReturn("someresource");
	}
	
	@Test
	public void testGetProperties() {
		final Config c = new Config(); 
		
		assertEquals("http://test.welive.eu/dev/api", c.getWeLiveAPI());
		assertEquals("https://test.welive.eu/aac", c.getAacServiceURL());
		assertEquals("aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee", c.getClientId());
		assertEquals("aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee", c.getClientSecret());
		assertEquals("http://bb-survey.cloudfoundry.welive.eu", c.getBBSurveyURL());
		
		assertEquals("localhost:8090", c.getHost());
		assertEquals("http://localhost:8090/bb-events/index", c.getHomeURL());
		assertEquals("/bb-events/api", c.getSwaggerPath());
		
		assertEquals(new HashSet<String>(Arrays.asList(new String[] {"10", "14"})), c.getAdminUsers());
		assertEquals(Language.ES, c.getDefaultLang());
		
		assertEquals("somedataset", c.getDataset());
		assertEquals("someresource", c.getResource());
		
		assertEquals("localhost:8090", c.getHost());
	}
	
	@Test
	public void testGetHostNoPort() {
		when(p.getProperty("baseurl"))
			.thenReturn("http://localhost/bb-events");
		
		final Config c = new Config();
		assertEquals("localhost", c.getHost());
	}
	
	@Test
	public void testGetAdminUsers() {
		final Set<String> expectedUsers = new HashSet<String>(Arrays.asList(new String[] {"10", "14"}));
		assertEquals(expectedUsers, Config.getAdminUsers("10,14"));
	}

}
