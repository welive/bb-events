/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.events.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.doReturn;

import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.json.Json;
import javax.json.JsonObject;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.Mockito;

import eu.welive.bb.events.data.qm.QMConnector;
import eu.welive.bb.events.data.qm.QMConnectorException;
import eu.welive.bb.events.serialization.Attendant;
import eu.welive.bb.events.serialization.Event;
import eu.welive.bb.events.serialization.Event.Language;
import eu.welive.bb.events.serialization.EventTranslation;
import eu.welive.bb.events.serialization.TimePeriod;
import eu.welive.bb.events.serialization.TimePoint;

public class DataWrapperTest { 
	
	@Rule
	public ExpectedException exception = ExpectedException.none();

	private static final String DATASET = "dataset";
	private static final String RESOURCE = "resource";
	
	private JsonObject event1, event2, event3;
	private JsonObject attendant1, attendant2, attendant3;
	private JsonObject timePeriod1, timePeriod2, timePeriod3;
	
	private DataWrapper dataWrapper;
	private QMConnector qmConnector;
	
	@Before
	public void setUp() {		
		qmConnector = Mockito.mock(QMConnector.class);		
		this.dataWrapper = new DataWrapper(DATASET, RESOURCE, qmConnector);
		this.dataWrapper = spy(this.dataWrapper);
		
		event1 = Json.createObjectBuilder()
			.add("id", 1)
			.add("title", "Title 1")
			.add("description", "Description 1")
			.add("material", "")
			.add("maxattendants", 0)
			.add("location", "Location 1")
			.add("contents", "Contents 1")
			.add("start", "2017-04-24T09:00:00")
			.add("end", "2017-04-24T17:00:00")
			.add("latitude", "43.2712491")
			.add("longitude", "-2.9407463")
			.add("surveyId", 5)
			.add("lang", "EN")
			.build();
		
		event2 = Json.createObjectBuilder()
			.add("id", 2)
			.add("title", "Title 2")
			.add("description", "Description 2")
			.add("material", "http://test.welive.eu")
			.add("maxattendants", 25)
			.add("location", "Location 2")
			.add("contents", "Contents 2")
			.add("start", "2017-04-30T09:00:00")
			.add("end", "2017-04-30T15:00:00")
			.add("latitude", "43.2712491")
			.add("longitude", "-2.9407463")
			.add("surveyId", 5)
			.add("lang", "EN")
			.build();
			
		event3 = Json.createObjectBuilder()
			.add("id", 3)
			.add("title", "Title 3")
			.add("description", "Description 3")
			.add("material", "http://dev.welive.eu")
			.add("maxattendants", 10)
			.add("location", "Location 3")
			.add("contents", "Contents 3")
			.add("start", "2017-05-02T15:00:00")
			.add("end", "2017-05-02T18:30:00")
			.add("latitude", "43.2908398")
			.add("longitude", "-2.7375026")
			.add("surveyId", 5)
			.add("lang", "EN")
			.build();
		
		attendant1 = Json.createObjectBuilder()
			.add("user_id", "10")
			.add("event_id", 1)
			.add("checkin", 0)
			.add("accepted", 0)
			.add("timestamp", "")
			.build();
		
		attendant2 = Json.createObjectBuilder()
			.add("user_id", "14")
			.add("event_id", 1)
			.add("checkin", 1)
			.add("accepted", 0)
			.add("timestamp", "2017-05-02T09:10:00")
			.build();
		
		attendant3 = Json.createObjectBuilder()
			.add("user_id", "20")
			.add("event_id", 1)
			.add("checkin", 1)
			.add("accepted", 1)
			.add("timestamp", "2017-05-02T09:15:00")
			.build();
		
		timePeriod1 = Json.createObjectBuilder()
			.add("id", 1)
			.add("startDay", "Monday")
			.add("startDate", "2017-01-01")
			.add("startTime", "10:30:00")
			.add("endDay", "Monday")
			.add("endDate", "2017-01-01")
			.add("endTime", "12:00:00")					
			.build(); 
		
		timePeriod2 = Json.createObjectBuilder()
			.add("id", 2)
			.add("startDay", "Tuesday")
			.add("startDate", "")
			.add("startTime", "17:30:00")
			.add("endDay", "Tuesday")
			.add("endDate", "")
			.add("endTime", "20:00:00")					
			.build();
		
		timePeriod3 = Json.createObjectBuilder()
			.add("id", 3)
			.add("startDay", "Saturday")
			.add("startDate", "")
			.add("startTime", "10:30:00")
			.add("endDay", "Saturday")
			.add("endDate", "")
			.add("endTime", "12:00:00")					
			.build();
	}
	
	@Test
	public void testGetQMConnector() {
		assertEquals(qmConnector, dataWrapper.getQMConnector());
	}
	
	@Test
	public void testGetEventsEmptyDates() throws DataWrapperException, QMConnectorException {		
		final String expectedEventQuery = "SELECT Event.id, Event.start, Event.end, Event.latitude, Event.longitude, Event.surveyId, "
				+ "Event.material, Event.maxattendants, "
				+ "COALESCE(t1.title, t2.title) as title, "
				+ "COALESCE(t1.description, t2.description) as description, "
				+ "COALESCE(t1.location, t2.location) as location, "
				+ "COALESCE(t1.contents, t2.contents) as contents, "
				+ "COALESCE(t1.lang, t2.lang) as lang "
				+ "FROM Event LEFT JOIN EventTranslation t1 ON (Event.id = t1.event_id AND t1.lang = 'EN') "
				+ "LEFT JOIN EventTranslation t2 ON (Event.id = t2.event_id AND t2.lang = 'ES') "
				+ "ORDER BY Event.start DESC";
		
		final JsonObject jsonResponse = Json.createObjectBuilder()
			.add("count", 3)
			.add("rows", Json.createArrayBuilder()
				.add(event1)
				.add(event2)
				.add(event3)
			).build();
		
		when(qmConnector.queryDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(jsonResponse);
		
		final JsonObject timePeriodResponse = Json.createObjectBuilder()
				.add("count", 1)
				.add("rows", Json.createArrayBuilder()
					.add(timePeriod1)
				).build();
			
		final String expectedTimePeriodQuery1 = "SELECT id, startDay, startDate, startTime, endDay, endDate, endTime FROM TimePeriod WHERE event_id=1";
		when(qmConnector.queryDataset(Matchers.eq(expectedTimePeriodQuery1), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(timePeriodResponse);
		
		final String expectedTimePeriodQuery2 = "SELECT id, startDay, startDate, startTime, endDay, endDate, endTime FROM TimePeriod WHERE event_id=2";
		when(qmConnector.queryDataset(Matchers.eq(expectedTimePeriodQuery2), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(timePeriodResponse);
		
		final String expectedTimePeriodQuery3 = "SELECT id, startDay, startDate, startTime, endDay, endDate, endTime FROM TimePeriod WHERE event_id=3";
		when(qmConnector.queryDataset(Matchers.eq(expectedTimePeriodQuery3), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(timePeriodResponse);
		
		final List<Event> events = dataWrapper.filterEvents("", "", "", "", "", "", Language.EN);
		verify(qmConnector).queryDataset(expectedEventQuery, DATASET, RESOURCE);
		verify(qmConnector).queryDataset(expectedTimePeriodQuery1, DATASET, RESOURCE);
		verify(qmConnector).queryDataset(expectedTimePeriodQuery2, DATASET, RESOURCE);
		verify(qmConnector).queryDataset(expectedTimePeriodQuery3, DATASET, RESOURCE);
		
		assertEquals(3, events.size());
		
		Event e = events.get(0);
		assertEquals(1, e.getId());
		assertEquals("Title 1", e.getTitle());
		assertEquals("Description 1", e.getDescription());
		assertEquals("Location 1", e.getLocation());
		assertEquals("Contents 1", e.getContents());
		assertEquals("2017-04-24T09:00:00", e.getStart());
		assertEquals("2017-04-24T17:00:00", e.getEnd());
		assertEquals("43.2712491", e.getLatitude());
		assertEquals("-2.9407463", e.getLongitude());
		assertEquals(Language.EN, e.getLang());
		
		e = events.get(1);
		assertEquals(2, e.getId());
		assertEquals("Title 2", e.getTitle());
		assertEquals("Description 2", e.getDescription());
		assertEquals("Location 2", e.getLocation());
		assertEquals("Contents 2", e.getContents());
		assertEquals("2017-04-30T09:00:00", e.getStart());
		assertEquals("2017-04-30T15:00:00", e.getEnd());
		assertEquals("43.2712491", e.getLatitude());
		assertEquals("-2.9407463", e.getLongitude());
		assertEquals(Language.EN, e.getLang());
		
		e = events.get(2);
		assertEquals(3, e.getId());
		assertEquals("Title 3", e.getTitle());
		assertEquals("Description 3", e.getDescription());
		assertEquals("Location 3", e.getLocation());
		assertEquals("Contents 3", e.getContents());
		assertEquals("2017-05-02T15:00:00", e.getStart());
		assertEquals("2017-05-02T18:30:00", e.getEnd());	
		assertEquals("43.2908398", e.getLatitude());
		assertEquals("-2.7375026", e.getLongitude());
		assertEquals(Language.EN, e.getLang());
	}
	
	@Test
	public void testFilterEventsByStartDate() throws DataWrapperException, QMConnectorException {
		final String expectedQuery = "SELECT Event.id, Event.start, Event.end, Event.latitude, Event.longitude, Event.surveyId, "
				+ "Event.material, Event.maxattendants, "
				+ "COALESCE(t1.title, t2.title) as title, "
				+ "COALESCE(t1.description, t2.description) as description, "
				+ "COALESCE(t1.location, t2.location) as location, "
				+ "COALESCE(t1.contents, t2.contents) as contents, "
				+ "COALESCE(t1.lang, t2.lang) as lang "
				+ "FROM Event LEFT JOIN EventTranslation t1 ON (Event.id = t1.event_id AND t1.lang = 'EN') "
				+ "LEFT JOIN EventTranslation t2 ON (Event.id = t2.event_id AND t2.lang = 'ES') "
				+ "WHERE Event.start >= '2017-04-30T09:00:00' "
				+ "ORDER BY Event.start DESC";;
		
		final JsonObject jsonResponse = Json.createObjectBuilder()
			.add("count", 2)
			.add("rows", Json.createArrayBuilder()
				.add(event2)
				.add(event3)
			).build();
					
		when(qmConnector.queryDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(jsonResponse);
		
		final JsonObject timePeriodResponse = Json.createObjectBuilder()
				.add("count", 1)
				.add("rows", Json.createArrayBuilder()
					.add(timePeriod1) 
				).build();
			
		final String expectedTimePeriodQuery1 = "SELECT id, startDay, startDate, startTime, endDay, endDate, endTime FROM TimePeriod WHERE event_id=2";	
		when(qmConnector.queryDataset(Matchers.eq(expectedTimePeriodQuery1), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(timePeriodResponse);
		
		final String expectedTimePeriodQuery2 = "SELECT id, startDay, startDate, startTime, endDay, endDate, endTime FROM TimePeriod WHERE event_id=3";
		when(qmConnector.queryDataset(Matchers.eq(expectedTimePeriodQuery2), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(timePeriodResponse);
		
		final List<Event> events = dataWrapper.filterEvents("2017-04-30T09:00:00", "", "", "", "", "", Language.EN);
		verify(qmConnector).queryDataset(expectedQuery, DATASET, RESOURCE);
		verify(qmConnector).queryDataset(expectedTimePeriodQuery1, DATASET, RESOURCE);
		verify(qmConnector).queryDataset(expectedTimePeriodQuery2, DATASET, RESOURCE);
		
		assertEquals(2, events.size());
			
		Event e = events.get(0);
		assertEquals(2, e.getId());
		assertEquals("Title 2", e.getTitle());
		assertEquals("Description 2", e.getDescription());
		assertEquals("Location 2", e.getLocation());
		assertEquals("Contents 2", e.getContents());
		assertEquals("2017-04-30T09:00:00", e.getStart());
		assertEquals("2017-04-30T15:00:00", e.getEnd());
		assertEquals("43.2712491", e.getLatitude());
		assertEquals("-2.9407463", e.getLongitude());
		assertEquals(Language.EN, e.getLang());
		
		e = events.get(1);
		assertEquals(3, e.getId());
		assertEquals("Title 3", e.getTitle());
		assertEquals("Description 3", e.getDescription());
		assertEquals("Location 3", e.getLocation());
		assertEquals("Contents 3", e.getContents());
		assertEquals("2017-05-02T15:00:00", e.getStart());
		assertEquals("2017-05-02T18:30:00", e.getEnd());
		assertEquals("43.2908398", e.getLatitude());
		assertEquals("-2.7375026", e.getLongitude());
		assertEquals(Language.EN, e.getLang());
	}
	
	@Test
	public void testFilterEventsByEndDate() throws DataWrapperException, QMConnectorException {
		final String expectedQuery = "SELECT Event.id, Event.start, Event.end, Event.latitude, Event.longitude, Event.surveyId, "
				+ "Event.material, Event.maxattendants, "
				+ "COALESCE(t1.title, t2.title) as title, "
				+ "COALESCE(t1.description, t2.description) as description, "
				+ "COALESCE(t1.location, t2.location) as location, "
				+ "COALESCE(t1.contents, t2.contents) as contents, "
				+ "COALESCE(t1.lang, t2.lang) as lang "
				+ "FROM Event LEFT JOIN EventTranslation t1 ON (Event.id = t1.event_id AND t1.lang = 'EN') "
				+ "LEFT JOIN EventTranslation t2 ON (Event.id = t2.event_id AND t2.lang = 'ES') "
				+ "WHERE Event.start <= '2017-04-24T23:59:59' "
				+ "ORDER BY Event.start DESC";;
		
		final JsonObject jsonResponse = Json.createObjectBuilder()
			.add("count", 2)
			.add("rows", Json.createArrayBuilder()
				.add(event2)
				.add(event3)
			).build();
					
		when(qmConnector.queryDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(jsonResponse);
		
		final JsonObject timePeriodResponse = Json.createObjectBuilder()
				.add("count", 1)
				.add("rows", Json.createArrayBuilder()
					.add(timePeriod1) 
				).build();
			
		final String expectedTimePeriodQuery1 = "SELECT id, startDay, startDate, startTime, endDay, endDate, endTime FROM TimePeriod WHERE event_id=2";
		when(qmConnector.queryDataset(Matchers.eq(expectedTimePeriodQuery1), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(timePeriodResponse);
		
		final String expectedTimePeriodQuery2 = "SELECT id, startDay, startDate, startTime, endDay, endDate, endTime FROM TimePeriod WHERE event_id=3";
		when(qmConnector.queryDataset(Matchers.eq(expectedTimePeriodQuery2), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(timePeriodResponse);
		
		final List<Event> events = dataWrapper.filterEvents("", "2017-04-24T23:59:59", "", "", "", "", Language.EN);
		verify(qmConnector).queryDataset(expectedQuery, DATASET, RESOURCE);
		verify(qmConnector).queryDataset(expectedTimePeriodQuery1, DATASET, RESOURCE);
		verify(qmConnector).queryDataset(expectedTimePeriodQuery2, DATASET, RESOURCE);
		
		assertEquals(2, events.size());
			
		Event e = events.get(0);
		assertEquals(2, e.getId());
		assertEquals("Title 2", e.getTitle());
		assertEquals("Description 2", e.getDescription());
		assertEquals("Location 2", e.getLocation());
		assertEquals("Contents 2", e.getContents());
		assertEquals("2017-04-30T09:00:00", e.getStart());
		assertEquals("2017-04-30T15:00:00", e.getEnd());
		assertEquals("43.2712491", e.getLatitude());
		assertEquals("-2.9407463", e.getLongitude());
		assertEquals(Language.EN, e.getLang());
		
		e = events.get(1);
		assertEquals(3, e.getId());
		assertEquals("Title 3", e.getTitle());
		assertEquals("Description 3", e.getDescription());
		assertEquals("Location 3", e.getLocation());
		assertEquals("Contents 3", e.getContents());
		assertEquals("2017-05-02T15:00:00", e.getStart());
		assertEquals("2017-05-02T18:30:00", e.getEnd());
		assertEquals("43.2908398", e.getLatitude());
		assertEquals("-2.7375026", e.getLongitude());
		assertEquals(Language.EN, e.getLang());
	}
	
	@Test
	public void testFilterEventsByRange() throws DataWrapperException, QMConnectorException {
		final String expectedQuery = "SELECT Event.id, Event.start, Event.end, Event.latitude, Event.longitude, Event.surveyId, "
				+ "Event.material, Event.maxattendants, " 
				+ "COALESCE(t1.title, t2.title) as title, "
				+ "COALESCE(t1.description, t2.description) as description, "
				+ "COALESCE(t1.location, t2.location) as location, "
				+ "COALESCE(t1.contents, t2.contents) as contents, "
				+ "COALESCE(t1.lang, t2.lang) as lang "
				+ "FROM Event LEFT JOIN EventTranslation t1 ON (Event.id = t1.event_id AND t1.lang = 'EN') "
				+ "LEFT JOIN EventTranslation t2 ON (Event.id = t2.event_id AND t2.lang = 'ES') "
				+ "WHERE Event.start >= '2017-04-24T09:00:00' "
				+ "AND Event.start <= '2017-04-24T09:00:00' "
				+ "ORDER BY Event.start DESC";
		
		final JsonObject jsonResponse = Json.createObjectBuilder()
			.add("count", 1)
			.add("rows", Json.createArrayBuilder()
				.add(event2)
			).build();
					
		when(qmConnector.queryDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(jsonResponse);
		
		final JsonObject timePeriodResponse = Json.createObjectBuilder()
			.add("count", 1)
			.add("rows", Json.createArrayBuilder()
				.add(timePeriod1) 
			).build();
			
		final String expectedTimePeriodQuery = "SELECT id, startDay, startDate, startTime, endDay, endDate, endTime FROM TimePeriod WHERE event_id=2";
		
		when(qmConnector.queryDataset(Matchers.eq(expectedTimePeriodQuery), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(timePeriodResponse);
		
		final List<Event> events = dataWrapper.filterEvents("2017-04-24T09:00:00", "2017-04-24T09:00:00", "", "", "", "", Language.EN);
		verify(qmConnector).queryDataset(expectedQuery, DATASET, RESOURCE);
		verify(qmConnector).queryDataset(expectedTimePeriodQuery, DATASET, RESOURCE);
		
		assertEquals(1, events.size());
			
		Event e = events.get(0);
		assertEquals(2, e.getId());
		assertEquals("Title 2", e.getTitle());
		assertEquals("Description 2", e.getDescription());
		assertEquals("Location 2", e.getLocation());
		assertEquals("Contents 2", e.getContents());
		assertEquals("2017-04-30T09:00:00", e.getStart());
		assertEquals("2017-04-30T15:00:00", e.getEnd());
		assertEquals("43.2712491", e.getLatitude());
		assertEquals("-2.9407463", e.getLongitude());
		assertEquals(Language.EN, e.getLang());
	}
	
	@Test
	public void testFilterEventsByCoords() throws DataWrapperException, QMConnectorException {
		final String expectedQuery = "SELECT Event.id, Event.start, Event.end, Event.latitude, Event.longitude, Event.surveyId, "
				+ "Event.material, Event.maxattendants, "
				+ "COALESCE(t1.title, t2.title) as title, "
				+ "COALESCE(t1.description, t2.description) as description, "
				+ "COALESCE(t1.location, t2.location) as location, "
				+ "COALESCE(t1.contents, t2.contents) as contents, "
				+ "COALESCE(t1.lang, t2.lang) as lang "
				+ "FROM Event LEFT JOIN EventTranslation t1 ON (Event.id = t1.event_id AND t1.lang = 'EN') "
				+ "LEFT JOIN EventTranslation t2 ON (Event.id = t2.event_id AND t2.lang = 'ES') "
				+ "WHERE CAST(Event.latitude AS FLOAT) <= 2.7 "
				+ "AND CAST(Event.latitude AS FLOAT) >= 1.5 "
				+ "AND CAST(Event.longitude AS FLOAT) <= 3.4 "
				+ "AND CAST(Event.longitude AS FLOAT) >= 2.3 "
				+ "ORDER BY Event.start DESC";
		
		final JsonObject jsonResponse = Json.createObjectBuilder()
			.add("count", 1)
			.add("rows", Json.createArrayBuilder()
				.add(event2)
			).build();
					
		when(qmConnector.queryDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(jsonResponse);
		
		final JsonObject timePeriodResponse = Json.createObjectBuilder()
				.add("count", 1)
				.add("rows", Json.createArrayBuilder()
					.add(timePeriod1) 
				).build();
			
		final String expectedTimePeriodQuery = "SELECT id, startDay, startDate, startTime, endDay, endDate, endTime FROM TimePeriod WHERE event_id=2";
		
		when(qmConnector.queryDataset(Matchers.eq(expectedTimePeriodQuery), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(timePeriodResponse);
		
		final List<Event> events = dataWrapper.filterEvents("", "", "2.7", "1.5", "3.4", "2.3", Language.EN);
		verify(qmConnector).queryDataset(expectedQuery, DATASET, RESOURCE);
		
		assertEquals(1, events.size());
			
		Event e = events.get(0);
		assertEquals(2, e.getId());
		assertEquals("Title 2", e.getTitle());
		assertEquals("Description 2", e.getDescription());
		assertEquals("Location 2", e.getLocation());
		assertEquals("Contents 2", e.getContents());
		assertEquals("2017-04-30T09:00:00", e.getStart());
		assertEquals("2017-04-30T15:00:00", e.getEnd());
		assertEquals("43.2712491", e.getLatitude());
		assertEquals("-2.9407463", e.getLongitude());
		assertEquals(Language.EN, e.getLang());
	}
	
	@Test
	public void testGetEventsUnexpectedError() throws QMConnectorException, DataWrapperException {
		exception.expect(DataWrapperException.class);
	    exception.expectMessage("Unexpected error");
		
		when(qmConnector.queryDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
			.thenThrow(new QMConnectorException("Unexpected error"));
		
		dataWrapper.filterEvents("2017-04-24T09:00:00", "2017-04-24T09:00:00", "", "", "", "", Language.ES);
	}
	
	@Test
	public void testSaveEvent() throws DataWrapperException, QMConnectorException {		
		final Event event = new Event();
		event.setTitle("Title 4");
		event.setDescription("Description 4");
		event.setMaxAttendants(25);
		event.setMaterial("http://test.welive.eu");
		event.setLocation("Location 4");
		event.setContents("Contents 4");
		event.setStart("2017-06-05T08:30:00");
		event.setEnd("2017-06-05T18:00:00");
		event.setLatitude("43.2712491");
		event.setLongitude("-2.9407463");
		event.setLang(Language.ES);
		
		final TimePeriod timePeriod = new TimePeriod();
		final TimePoint start = new TimePoint();
		start.setDay(DayOfWeek.MONDAY);
		start.setDate("2017-01-01");
		start.setTime("10:00:00");
		
		timePeriod.setStart(start);
		
		final TimePoint end = new TimePoint();
		end.setDay(DayOfWeek.MONDAY);
		end.setDate("2017-01-01");
		end.setTime("12:00:00");
		timePeriod.setEnd(end);
		
		final List<TimePeriod> schedule = new ArrayList<TimePeriod>();
		schedule.add(timePeriod);
		
		event.setSchedule(schedule);
		
		when(qmConnector.updateDatasetTransactional(Matchers.anyListOf(String.class), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(3);
		
		final int updatedRows = dataWrapper.saveEvent(event);
		
		final ArgumentCaptor<List> insertsCaptor = ArgumentCaptor.forClass(List.class);
		verify(qmConnector).updateDatasetTransactional(insertsCaptor.capture(), Matchers.anyString(), Matchers.anyString());
		
		assertEquals(3, updatedRows);
		
		final List<String> inserts = insertsCaptor.getValue();
		
		final String insertEvent = String.format("INSERT INTO Event (start, end, maxattendants, material, latitude, longitude, surveyId) VALUES ('%s', '%s', %d, '%s', '%s', '%s', %d)",
			event.getStart(), event.getEnd(), event.getMaxAttendants(), event.getMaterial(), event.getLatitude(), event.getLongitude(), event.getSurveyId());
		assertEquals(insertEvent, inserts.get(0));
		
		final String insertTimePeriod = String.format("INSERT INTO TimePeriod (id, startDay, startDate, startTime, endDay, endDate, endTime, event_id) SELECT null, '%s', '%s', '%s', '%s', '%s', '%s', MAX(id) FROM Event",
			timePeriod.getStart().getDay(), timePeriod.getStart().getDate(), timePeriod.getStart().getTime(), timePeriod.getEnd().getDay(), timePeriod.getEnd().getDate(), timePeriod.getEnd().getTime()); 
			assertEquals(insertTimePeriod, inserts.get(1));
		
		final String insertTranslation = String.format("INSERT INTO EventTranslation (id, title, description, location, contents, lang, event_id) SELECT null, '%s', '%s', '%s', '%s', '%s', MAX(id) FROM Event",
			event.getTitle(), event.getDescription(), event.getLocation(), event.getContents(), event.getLang());
		assertEquals(insertTranslation, inserts.get(2));
	}
	
	@Test
	public void testSaveEventUnexpectedError() throws DataWrapperException, QMConnectorException {		
		final Event event = new Event();
		event.setTitle("Title 4");
		event.setDescription("Description 4");
		event.setLocation("Location 4");
		event.setContents("Contents 4");
		event.setStart("2017-06-05T08:30:00");
		event.setEnd("2017-06-05T18:00:00");
		event.setLang(Language.ES);
		
		exception.expect(DataWrapperException.class);
	    exception.expectMessage("Unexpected error");
		
		when(qmConnector.updateDatasetTransactional(Matchers.anyListOf(String.class), Matchers.anyString(), Matchers.anyString()))
			.thenThrow(new QMConnectorException("Unexpected error"));
		
		dataWrapper.saveEvent(event);
	}
	
	@Test
	public void testSaveTranslation() throws DataWrapperException, QMConnectorException {		
		final EventTranslation eventTranslation = new EventTranslation();
		eventTranslation.setEventId(4);
		eventTranslation.setTitle("Title 4");
		eventTranslation.setDescription("Description 4");
		eventTranslation.setLocation("Location 4");
		eventTranslation.setContents("Contents 4");
		
		doReturn(Collections.<Language>emptyList())
			.when(dataWrapper).getAvailableTranslations(eventTranslation.getEventId());
		
		when(qmConnector.updateDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(1);
		
		final int updatedRows = dataWrapper.saveTranslation(eventTranslation, Language.ES);
		
		final String insertTranslation = String.format("INSERT INTO EventTranslation (title, description, location, contents, lang, event_id) VALUES ('%s', '%s', '%s', '%s', '%s', %d)",
				eventTranslation.getTitle(), eventTranslation.getDescription(), eventTranslation.getLocation(), eventTranslation.getContents(), eventTranslation.getLang(), eventTranslation.getEventId());
		
		verify(qmConnector).updateDataset(insertTranslation, DATASET, RESOURCE);
		
		assertEquals(1, updatedRows);
	}
	
	@Test
	public void testSaveTranslationExistingLanguage() throws DataWrapperException {		
		final EventTranslation eventTranslation = new EventTranslation();
		eventTranslation.setEventId(4);
		eventTranslation.setTitle("Title 4");
		eventTranslation.setDescription("Description 4");
		eventTranslation.setLocation("Location 4");
		eventTranslation.setContents("Contents 4");
		
		final List<Language> languages = new ArrayList<Language>();
		languages.add(Language.EN);
		
		doReturn(languages).when(dataWrapper).getAvailableTranslations(eventTranslation.getEventId());
		
		exception.expect(DataWrapperException.class);
	    exception.expectMessage("Translation for lang 'EN' already exists");
		
		dataWrapper.saveTranslation(eventTranslation, Language.EN);
	}
	
	@Test
	public void testSaveTranslationUnexpectedError() throws DataWrapperException, QMConnectorException {		
		final EventTranslation eventTranslation = new EventTranslation();
		eventTranslation.setTitle("Title 4");
		eventTranslation.setDescription("Description 4");
		eventTranslation.setLocation("Location 4");
		eventTranslation.setContents("Contents 4");
		
		exception.expect(DataWrapperException.class);
	    exception.expectMessage("Unexpected error");
	    
	    doReturn(Collections.<Language>emptyList())
			.when(dataWrapper).getAvailableTranslations(eventTranslation.getEventId());
		
		when(qmConnector.updateDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
			.thenThrow(new QMConnectorException("Unexpected error"));
		
		dataWrapper.saveTranslation(eventTranslation, Language.ES);
	}
	
	@Test
	public void testUpdateEvent() throws QMConnectorException, DataWrapperException {
		final Event event = new Event();
		event.setId(4);
		event.setTitle("Title 4");
		event.setDescription("Description 4");
		event.setMaterial("http://test.welive.eu");
		event.setMaxAttendants(25);
		event.setLocation("Location 4");
		event.setContents("Contents 4");
		event.setStart("2017-06-05T08:30:00");
		event.setEnd("2017-06-05T18:00:00");
		event.setLatitude("43.2712491");
		event.setLongitude("-2.9407463");
		event.setSurveyId(5);
		event.setLang(Language.ES);
		
		final List<TimePeriod> timePeriods = new ArrayList<TimePeriod>();
		timePeriods.add(dataWrapper.processTimePeriod(timePeriod2));
		timePeriods.add(dataWrapper.processTimePeriod(timePeriod3));
		timePeriods.get(1).setId(0);
		
		event.setSchedule(timePeriods);
		
		when(qmConnector.updateDatasetTransactional(Matchers.anyListOf(String.class), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(2);
		
		final JsonObject timePeriodResponse = Json.createObjectBuilder()
			.add("count", 1)
			.add("rows", Json.createArrayBuilder()
				.add(timePeriod1) 
			).build();
		
		when(qmConnector.queryDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(timePeriodResponse);
		
		final int updatedRows = dataWrapper.updateEvent(event);
				
		assertEquals(2, updatedRows);
		
		final List<String> updates = new ArrayList<String>();
		final String updateEvent = String.format("UPDATE Event SET start='%s', end='%s', maxattendants=%d, material='%s', latitude='%s', longitude='%s', surveyId=%d WHERE id=%d", 
			event.getStart(), event.getEnd(), event.getMaxAttendants(), event.getMaterial(),  event.getLatitude(), event.getLongitude(), event.getSurveyId(), event.getId());		
		
		updates.add(updateEvent);
		
		final String updateTimePeriod = String.format("UPDATE TimePeriod SET startDay='%s', startDate='%s', startTime='%s', endDay='%s', endDate='%s', endTime='%s' WHERE id=%d AND event_id=%d",
			timePeriods.get(0).getStart().getDay(), timePeriods.get(0).getStart().getDate(), timePeriods.get(0).getStart().getTime(),
			timePeriods.get(0).getEnd().getDay(), timePeriods.get(0).getEnd().getDate(), timePeriods.get(0).getEnd().getTime(), 2, 4);
		
		updates.add(updateTimePeriod);
		
		final String insertTimePeriod = String.format("INSERT INTO TimePeriod (id, startDay, startDate, startTime, endDay, endDate, endTime, event_id) SELECT null, '%s', '%s', '%s', '%s', '%s', '%s', MAX(id) FROM Event",
				timePeriods.get(1).getStart().getDay(), timePeriods.get(1).getStart().getDate(), timePeriods.get(1).getStart().getTime(),
				timePeriods.get(1).getEnd().getDay(), timePeriods.get(1).getEnd().getDate(), timePeriods.get(1).getEnd().getTime());
			
		updates.add(insertTimePeriod);
		
		updates.add("DELETE FROM TimePeriod WHERE event_id=4 AND id=1");
		
		verify(qmConnector).updateDatasetTransactional(updates, DATASET, RESOURCE);	
	}
	
	@Test
	public void testUpdateEventUnexpectedError() throws QMConnectorException, DataWrapperException {
		final Event event = new Event();
		event.setId(4);
		event.setTitle("Title 4");
		event.setDescription("Description 4");
		event.setLocation("Location 4");
		event.setContents("Contents 4");
		event.setStart("2017-06-05T08:30:00");
		event.setEnd("2017-06-05T18:00:00");
		event.setLang(Language.ES);
		
		exception.expect(DataWrapperException.class);
	    exception.expectMessage("Unexpected error");
	    
	    final JsonObject timePeriodResponse = Json.createObjectBuilder()
			.add("count", 1)
			.add("rows", Json.createArrayBuilder()
				.add(timePeriod1) 
			).build();
	    
	    when(qmConnector.queryDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(timePeriodResponse);
		
		when(qmConnector.updateDatasetTransactional(Matchers.anyListOf(String.class), Matchers.anyString(), Matchers.anyString()))
			.thenThrow(new QMConnectorException("Unexpected error"));
		
		dataWrapper.updateEvent(event);
	}
	
	@Test
	public void testUpdateTranslation() throws QMConnectorException, DataWrapperException {
		final EventTranslation eventTranslation = new EventTranslation();
		eventTranslation.setEventId(4);
		eventTranslation.setTitle("Title 4");
		eventTranslation.setDescription("Description 4");
		eventTranslation.setLocation("Location 4");
		eventTranslation.setContents("Contents 4");
		
		when(qmConnector.updateDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(2);
		
		final int updatedRows = dataWrapper.updateTranslation(eventTranslation, Language.EU);
				
		assertEquals(2, updatedRows);
		
		final String updateEventTranslation = String.format("UPDATE EventTranslation SET title='%s', description='%s', location='%s', contents='%s' WHERE lang='%s' AND event_id=%d",
				eventTranslation.getTitle(), eventTranslation.getDescription(), eventTranslation.getLocation(), eventTranslation.getContents(), Language.EU, eventTranslation.getEventId());
		
		verify(qmConnector).updateDataset(updateEventTranslation, DATASET, RESOURCE);	
	}
	
	@Test
	public void testUpdateTranslationUnexpectedError() throws QMConnectorException, DataWrapperException {
		exception.expect(DataWrapperException.class);
	    exception.expectMessage("Unexpected error");
	    
		final EventTranslation eventTranslation = new EventTranslation();
		eventTranslation.setEventId(4);
		eventTranslation.setTitle("Title 4");
		eventTranslation.setDescription("Description 4");
		eventTranslation.setLocation("Location 4");
		eventTranslation.setContents("Contents 4");
		
		when(qmConnector.updateDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
			.thenThrow(new QMConnectorException("Unexpected error"));
		
		dataWrapper.updateTranslation(eventTranslation, Language.ES);	
	}
	
	@Test
	public void testCountTranslations() throws QMConnectorException, DataWrapperException {
		final JsonObject jsonResponse = Json.createObjectBuilder()
			.add("count", 1)
			.add("rows", Json.createArrayBuilder()
				.add(Json.createObjectBuilder()
					.add("num", 2)
				)
			)
			.build();
		
		when(qmConnector.queryDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(jsonResponse);
		
		dataWrapper.countTranslations(4);
		
		final String expectedQuery = "SELECT COUNT(id) as num FROM EventTranslation WHERE event_id=4";		
		verify(qmConnector).queryDataset(expectedQuery, DATASET, RESOURCE);
	}
	
	@Test
	public void testCountTranslationsUnexpectedError() throws QMConnectorException, DataWrapperException {
		exception.expect(DataWrapperException.class);
	    exception.expectMessage("Unexpected error");
		
		when(qmConnector.queryDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
			.thenThrow(new QMConnectorException("Unexpected error"));
		
		dataWrapper.countTranslations(4);
	}
	
	@Test
	public void testDeleteTranslation() throws DataWrapperException, QMConnectorException {
		final int eventId = 4;
		when(qmConnector.updateDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(1);
		
		final JsonObject jsonResponse = Json.createObjectBuilder()
				.add("count", 1)
				.add("rows", Json.createArrayBuilder()
					.add(Json.createObjectBuilder()
						.add("num", 1)
					)
				)
				.build();
		
		when(qmConnector.queryDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(jsonResponse);
		
		final int updatedRows = dataWrapper.deleteTranslation(eventId, Language.ES);
		assertEquals(1, updatedRows);
		
		final String delete = "DELETE FROM EventTranslation WHERE lang='ES' AND event_id=4";
		verify(qmConnector).updateDataset(delete, DATASET, RESOURCE);
	}
	
	@Test
	public void testDeleteTranslationRemoveEvent() throws DataWrapperException, QMConnectorException {
		final int eventId = 4;
		when(qmConnector.updateDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(1);
		
		final JsonObject jsonResponse = Json.createObjectBuilder()
				.add("count", 1)
				.add("rows", Json.createArrayBuilder()
					.add(Json.createObjectBuilder()
						.add("num", 0)
					)
				)
				.build();
		
		when(qmConnector.updateDatasetTransactional(Matchers.anyListOf(String.class), Matchers.anyString(), Matchers.anyString()))
		.thenReturn(1);
		
		when(qmConnector.queryDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(jsonResponse);
		
		final int updatedRows = dataWrapper.deleteTranslation(eventId, Language.ES);
		assertEquals(2, updatedRows);
		
		verify(qmConnector).updateDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString());
		verify(qmConnector).updateDatasetTransactional(Matchers.anyListOf(String.class), Matchers.anyString(), Matchers.anyString());
	}
	
	@Test
	public void testDeleteTranslationNonExistent() throws DataWrapperException, QMConnectorException {
		final int eventId = 4;
		when(qmConnector.updateDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(0);
		
		final int updatedRows = dataWrapper.deleteTranslation(eventId, Language.ES);
		assertEquals(0, updatedRows);
		
		final String delete = "DELETE FROM EventTranslation WHERE lang='ES' AND event_id=4";
		verify(qmConnector).updateDataset(delete, DATASET, RESOURCE);
	}
	
	@Test
	public void testDeleteTranslationUnexpectedError() throws DataWrapperException, QMConnectorException {
		exception.expect(DataWrapperException.class);
	    exception.expectMessage("Unexpected error");
		
		when(qmConnector.updateDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
			.thenThrow(new QMConnectorException("Unexpected error"));
		
		dataWrapper.deleteTranslation(4, Language.ES);
	}
	
	@Test
	public void testDeleteEvent() throws QMConnectorException, DataWrapperException {
		when(qmConnector.updateDatasetTransactional(Matchers.anyListOf(String.class), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(1);
		
		final int updatedRows = dataWrapper.deleteEvent(4);
		assertEquals(1, updatedRows);
		
		final String deleteEvent = "DELETE FROM Event WHERE id=4";
		final String deleteTranslations = "DELETE FROM EventTranslation WHERE event_id=4";
		final String deleteAttendants = "DELETE FROM Attendant WHERE event_id=4";
		
		final ArgumentCaptor<List> deletesCaptor = ArgumentCaptor.forClass(List.class);
		verify(qmConnector).updateDatasetTransactional(deletesCaptor.capture(), Matchers.anyString(), Matchers.anyString());
		
		final List<String> deletes = deletesCaptor.getValue();
		
		assertEquals(deleteEvent, deletes.get(0));
		assertEquals(deleteTranslations, deletes.get(1));
		assertEquals(deleteAttendants, deletes.get(2));
	}
	
	@Test
	public void testDeleteEventUnexpectedError() throws QMConnectorException, DataWrapperException {
		exception.expect(DataWrapperException.class);
	    exception.expectMessage("Unexpected error");
		
		when(qmConnector.updateDatasetTransactional(Matchers.anyListOf(String.class), Matchers.anyString(), Matchers.anyString()))
			.thenThrow(new QMConnectorException("Unexpected error"));
		
		dataWrapper.deleteEvent(4);
	}
	
	@Test
	public void testGetEvent() throws QMConnectorException, DataWrapperException {
		final JsonObject eventResponse = Json.createObjectBuilder()
			.add("count", 1)
			.add("rows", Json.createArrayBuilder()
				.add(event1)
			).build();
		
		final String expectedEventQuery = "SELECT Event.id, Event.start, Event.end, Event.material, Event.latitude, Event.longitude, Event.surveyId, "
				+ "Event.maxattendants, "
				+ "COALESCE(t1.title, t2.title) as title, "
				+ "COALESCE(t1.description, t2.description) as description, "
				+ "COALESCE(t1.location, t2.location) as location, "
				+ "COALESCE(t1.contents, t2.contents) as contents, "
				+ "COALESCE(t1.lang, t2.lang) as lang "
				+ "FROM Event LEFT JOIN EventTranslation t1 ON (Event.id = t1.event_id AND t1.lang = 'EN') "
				+ "LEFT JOIN EventTranslation t2 ON (Event.id = t2.event_id AND t2.lang = 'ES') "
				+ "WHERE Event.id=1";
		
		when(qmConnector.queryDataset(Matchers.eq(expectedEventQuery), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(eventResponse);
		
		final JsonObject timePeriodResponse = Json.createObjectBuilder()
			.add("count", 1)
			.add("rows", Json.createArrayBuilder()
				.add(timePeriod1) 
			).build();
		
		final String expectedTimePeriodQuery = "SELECT id, startDay, startDate, startTime, endDay, endDate, endTime FROM TimePeriod WHERE event_id=1";
		
		when(qmConnector.queryDataset(Matchers.eq(expectedTimePeriodQuery), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(timePeriodResponse);
		
		final Event e = dataWrapper.getEvent(1, Language.EN);
		assertEquals(1, e.getId());
		assertEquals("Title 1", e.getTitle());
		assertEquals("Description 1", e.getDescription());
		assertEquals("", e.getMaterial());
		assertEquals(0, e.getMaxAttendants());
		assertEquals("Location 1", e.getLocation());
		assertEquals("Contents 1", e.getContents());
		assertEquals("2017-04-24T09:00:00", e.getStart());
		assertEquals("2017-04-24T17:00:00", e.getEnd());
		assertEquals("43.2712491", e.getLatitude());
		assertEquals("-2.9407463", e.getLongitude());
		assertEquals(5, e.getSurveyId());
		assertEquals(Language.EN, e.getLang());
		
		verify(qmConnector, times(1)).queryDataset(expectedEventQuery, DATASET, RESOURCE);
		verify(qmConnector, times(1)).queryDataset(expectedTimePeriodQuery, DATASET, RESOURCE);
	}
	
	@Test
	public void testGetEventNoResponse() throws QMConnectorException, DataWrapperException {
		final JsonObject jsonResponse = Json.createObjectBuilder()
			.add("count", 0)
			.build();
		
		when(qmConnector.queryDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(jsonResponse);
		
		final Event e = dataWrapper.getEvent(1, Language.EN);
		assertEquals(0, e.getId());
	}
	
	@Test
	public void testGetEventUnexpectedError() throws QMConnectorException, DataWrapperException {
		exception.expect(DataWrapperException.class);
	    exception.expectMessage("Unexpected error");
	    
	    when(qmConnector.queryDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
			.thenThrow(new QMConnectorException("Unexpected error"));
	    
	    dataWrapper.getEvent(1, Language.EN);
	}
	
	@Test
	public void testGetAttendants() throws QMConnectorException, DataWrapperException {
		final JsonObject jsonResponse = Json.createObjectBuilder()
			.add("count", 2)
			.add("rows", Json.createArrayBuilder()
				.add(attendant1)
				.add(attendant2)
				.add(attendant3)
			).build();
		
		when(qmConnector.queryDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(jsonResponse);
		
		final List<Attendant> attendants = dataWrapper.getAttendants(1);
		
		final String expectedQuery = "SELECT att.user_id, att.event_id, att.checkin, att.accepted, att.timestamp FROM Attendant att WHERE event_id=1";
		verify(qmConnector).queryDataset(expectedQuery, DATASET, RESOURCE);
		
		assertEquals(3, attendants.size());
	}
	
	@Test
	public void testGetAttendantsUnexpectedError() throws QMConnectorException, DataWrapperException {
		exception.expect(DataWrapperException.class);
	    exception.expectMessage("Unexpected error");
		
		when(qmConnector.queryDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
			.thenThrow(new QMConnectorException("Unexpected error"));
		
		dataWrapper.getAttendants(1);
	}
	
	@Test
	public void testAddAttendant() throws DataWrapperException, QMConnectorException {
		when(qmConnector.updateDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(1);
		
		when(qmConnector.createTimestamp())
			.thenReturn("2017-04-25T16:34:22");
		
		final int updatedRows = dataWrapper.addAttendant(1, "10");
		
		final String expectedInsert = "INSERT INTO Attendant (user_id, checkin, accepted, timestamp, event_id) VALUES ('10', 0, 0, '2017-04-25T16:34:22', 1)";
		verify(qmConnector).updateDataset(expectedInsert, DATASET, RESOURCE);
		
		assertEquals(1, updatedRows);
	}
	
	@Test
	public void testAddAttendantUnexpectedError() throws DataWrapperException, QMConnectorException {
		exception.expect(DataWrapperException.class);
	    exception.expectMessage("Unexpected error");
		
		when(qmConnector.updateDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
			.thenThrow(new QMConnectorException("Unexpected error"));
		
		dataWrapper.addAttendant(1, "10");
	}
	
	@Test
	public void testRemoveAttendant() throws DataWrapperException, QMConnectorException {
		when(qmConnector.updateDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(1);
		
		final int updatedRows = dataWrapper.removeAttendant(1, "10");
		
		final String expectedDelete = "DELETE FROM Attendant WHERE user_id='10' AND event_id=1";
		verify(qmConnector).updateDataset(expectedDelete, DATASET, RESOURCE);
		
		assertEquals(1, updatedRows);
	}
	
	@Test
	public void testRemoveAttendantUnexpectedError() throws DataWrapperException, QMConnectorException {
		exception.expect(DataWrapperException.class);
	    exception.expectMessage("Unexpected error");
		
		when(qmConnector.updateDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
			.thenThrow(new QMConnectorException("Unexpected error"));
		
		dataWrapper.removeAttendant(1, "10");
	}
	
	@Test
	public void testCheckinAttendant() throws QMConnectorException, DataWrapperException {
		when(qmConnector.updateDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(1);
		
		when(qmConnector.createTimestamp())
			.thenReturn("2017-04-25T16:34:22");
		
		final int updatedRows = dataWrapper.checkinAttendant(1, "10");
		
		final String expectedUpdate = "UPDATE Attendant SET checkin=1, timestamp='2017-04-25T16:34:22' WHERE user_id='10' AND event_id=1";
		verify(qmConnector).updateDataset(expectedUpdate, DATASET, RESOURCE);
		
		assertEquals(1, updatedRows);
	}
	
	@Test
	public void testCheckoutAttendant() throws QMConnectorException, DataWrapperException {
		when(qmConnector.updateDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(1);
		
		when(qmConnector.createTimestamp())
			.thenReturn("2017-04-25T16:34:22");
		
		final int updatedRows = dataWrapper.checkoutAttendant(1, "10");
		
		final String expectedUpdate = "UPDATE Attendant SET checkin=0, timestamp='2017-04-25T16:34:22' WHERE user_id='10' AND event_id=1";
		verify(qmConnector).updateDataset(expectedUpdate, DATASET, RESOURCE);
		
		assertEquals(1, updatedRows);
	}
	
	@Test
	public void testAcceptAttendant() throws QMConnectorException, DataWrapperException {
		when(qmConnector.updateDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(1);
		
		when(qmConnector.createTimestamp())
			.thenReturn("2017-04-25T16:34:22");
		
		final int updatedRows = dataWrapper.acceptAttendant(1, "10");
		
		final String expectedUpdate = "UPDATE Attendant SET accepted=1, timestamp='2017-04-25T16:34:22' WHERE user_id='10' AND event_id=1";
		verify(qmConnector).updateDataset(expectedUpdate, DATASET, RESOURCE);
		
		assertEquals(1, updatedRows);
	}
	
	@Test
	public void testRejectAttendant() throws QMConnectorException, DataWrapperException {
		when(qmConnector.updateDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(1);
		
		when(qmConnector.createTimestamp())
			.thenReturn("2017-04-25T16:34:22");
		
		final int updatedRows = dataWrapper.rejectAttendant(1, "10");
		
		final String expectedUpdate = "UPDATE Attendant SET accepted=0, timestamp='2017-04-25T16:34:22' WHERE user_id='10' AND event_id=1";
		verify(qmConnector).updateDataset(expectedUpdate, DATASET, RESOURCE);
		
		assertEquals(1, updatedRows);
	}
	
	@Test
	public void testCheckoutAttendantUnexpectedError() throws QMConnectorException, DataWrapperException {
		exception.expect(DataWrapperException.class);
	    exception.expectMessage("Unexpected error");
		
		when(qmConnector.updateDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
			.thenThrow(new QMConnectorException("Unexpected error"));
		
		when(qmConnector.createTimestamp())
			.thenReturn("2017-04-25T16:34:22");
		
		dataWrapper.checkoutAttendant(1, "10");
	}
	
	@Test
	public void testIsAttendantUnexpectedError() throws QMConnectorException, DataWrapperException {
		exception.expect(DataWrapperException.class);
	    exception.expectMessage("Unexpected error");
	    
	    when(qmConnector.queryDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
			.thenThrow(new QMConnectorException("Unexpected error"));
	    
	    dataWrapper.isAttendant(1, "10");
	}
	
	@Test
	public void testIsAttendantTrue() throws QMConnectorException, DataWrapperException {
		final JsonObject response = Json.createObjectBuilder()
			.add("count", 1)
			.add("rows", Json.createArrayBuilder()
				.add(Json.createObjectBuilder()
					.add("num", 1)
				)
			).build();
		
		when(qmConnector.queryDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(response);
		
		assertTrue(dataWrapper.isAttendant(1, "10"));
		
		final String query = "SELECT COUNT(*) as num FROM Attendant WHERE user_id='10' AND event_id=1";
		verify(qmConnector).queryDataset(query, DATASET, RESOURCE);
	}
	
	@Test
	public void testIsAttendantFalse() throws QMConnectorException, DataWrapperException {
		final JsonObject response = Json.createObjectBuilder()
			.add("count", 1)
			.add("rows", Json.createArrayBuilder()
				.add(Json.createObjectBuilder()
					.add("num", 0)
				)
			).build();
		
		when(qmConnector.queryDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(response);
		
		assertFalse(dataWrapper.isAttendant(1, "10"));
		
		final String query = "SELECT COUNT(*) as num FROM Attendant WHERE user_id='10' AND event_id=1";
		verify(qmConnector).queryDataset(query, DATASET, RESOURCE);
	}
	
	@Test
	public void testGetTimePeriods() throws DataWrapperException, QMConnectorException {
		final JsonObject timePeriodResponse = Json.createObjectBuilder()
			.add("count", 2)
			.add("rows", Json.createArrayBuilder()
				.add(timePeriod1)
				.add(timePeriod2)
			).build();
		
		when(qmConnector.queryDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(timePeriodResponse);
		
		final List<TimePeriod> timePeriods = dataWrapper.getTimePeriods(1);
		assertEquals(2, timePeriods.size());
		
		final String query = "SELECT id, startDay, startDate, startTime, endDay, endDate, endTime FROM TimePeriod WHERE event_id=1";
		verify(qmConnector).queryDataset(query, DATASET, RESOURCE);
	}
	
	@Test
	public void testSaveTimePeriods() throws DataWrapperException, QMConnectorException {
		final List<TimePeriod> timePeriods = new ArrayList<TimePeriod>();
		timePeriods.add(dataWrapper.processTimePeriod(timePeriod1));
		timePeriods.add(dataWrapper.processTimePeriod(timePeriod2));
		
		when(qmConnector.updateDatasetTransactional(Matchers.anyListOf(String.class), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(2);
		
		dataWrapper.saveTimePeriods(timePeriods, 2);
		
		final List<String> updates = new ArrayList<String>();
		updates.add("INSERT INTO TimePeriod (id, startDay, startDate, startTime, endDay, endDate, endTime, event_id) VALUES (null, 'MONDAY', '2017-01-01', '10:30:00', 'MONDAY', '2017-01-01', '12:00:00', 2)");
		updates.add("INSERT INTO TimePeriod (id, startDay, startDate, startTime, endDay, endDate, endTime, event_id) VALUES (null, 'TUESDAY', '', '17:30:00', 'TUESDAY', '', '20:00:00', 2)");
		
		verify(qmConnector).updateDatasetTransactional(updates, DATASET, RESOURCE);
	}
	
	@Test
	public void testSaveTimePeriodsUnexpectedError() throws DataWrapperException, QMConnectorException {
		final List<TimePeriod> timePeriods = new ArrayList<TimePeriod>();
		timePeriods.add(dataWrapper.processTimePeriod(timePeriod1));
		timePeriods.add(dataWrapper.processTimePeriod(timePeriod2));
		
		exception.expect(DataWrapperException.class);
	    exception.expectMessage("Unexpected error");
		
		when(qmConnector.updateDatasetTransactional(Matchers.anyListOf(String.class), Matchers.anyString(), Matchers.anyString()))
			.thenThrow(new QMConnectorException("Unexpected error"));
		
		dataWrapper.saveTimePeriods(timePeriods, 2);
	}
	
	@Test
	public void testGetTimePeriodsUnexpectedError() throws QMConnectorException, DataWrapperException {
		exception.expect(DataWrapperException.class);
	    exception.expectMessage("Unexpected error");
		
		when(qmConnector.queryDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
			.thenThrow(new QMConnectorException("Unexpected error"));
		
		dataWrapper.getTimePeriods(1);
	}
	
	@Test
	public void testDeleteTimePeriods() throws DataWrapperException, QMConnectorException {
		final List<TimePeriod> timePeriods = new ArrayList<TimePeriod>();
		timePeriods.add(dataWrapper.processTimePeriod(timePeriod1));
		timePeriods.add(dataWrapper.processTimePeriod(timePeriod2));
		
		when(qmConnector.updateDatasetTransactional(Matchers.anyListOf(String.class), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(2);
		
		final int updatedRows = dataWrapper.deleteTimePeriods(timePeriods, 2);
		assertEquals(2, updatedRows);
		
		final List<String> deletes = new ArrayList<String>();
		deletes.add("DELETE FROM TimePeriod WHERE id=1 AND event_id=2");
		deletes.add("DELETE FROM TimePeriod WHERE id=2 AND event_id=2");
		
		verify(qmConnector).updateDatasetTransactional(deletes, DATASET, RESOURCE);
	}
	
	@Test
	public void testDeleteTimePeriodsUnexpectedError() throws QMConnectorException, DataWrapperException {
		final List<TimePeriod> timePeriods = new ArrayList<TimePeriod>();
		timePeriods.add(dataWrapper.processTimePeriod(timePeriod1));
		timePeriods.add(dataWrapper.processTimePeriod(timePeriod2));
		
		exception.expect(DataWrapperException.class);
	    exception.expectMessage("Unexpected error");
		
		when(qmConnector.updateDatasetTransactional(Matchers.anyListOf(String.class), Matchers.anyString(), Matchers.anyString()))
			.thenThrow(new QMConnectorException("Unexpected error"));
		
		dataWrapper.deleteTimePeriods(timePeriods, 2);
	}
	
	@Test
	public void testSetAttendantCheckinStatus() throws DataWrapperException, QMConnectorException {
		when(qmConnector.updateDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(1);
		
		when(qmConnector.createTimestamp())
			.thenReturn("2017-04-25T16:34:22");
		
		final int updatedRows = dataWrapper.setAttendantCheckinStatus(2, "10", true);
		assertEquals(1, updatedRows);
		
		final String update = "UPDATE Attendant SET checkin=1, timestamp='2017-04-25T16:34:22' WHERE user_id='10' AND event_id=2"; 
		verify(qmConnector).updateDataset(update, DATASET, RESOURCE);
	}
	
	@Test
	public void testSetAttendantCheckinStatusUnexpectedError() throws DataWrapperException, QMConnectorException {
		exception.expect(DataWrapperException.class);
	    exception.expectMessage("Unexpected error");
	    
	    when(qmConnector.updateDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
	    	.thenThrow(new QMConnectorException("Unexpected error"));
	    
	    dataWrapper.setAttendantCheckinStatus(2, "10", true);
	}
	
	@Test
	public void testSetAttendantAcceptedStatus() throws DataWrapperException, QMConnectorException {
		when(qmConnector.updateDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(1);
		
		when(qmConnector.createTimestamp())
			.thenReturn("2017-04-25T16:34:22");
		
		final int updatedRows = dataWrapper.setAttendantAcceptedStatus(2, "10", true);
		assertEquals(1, updatedRows);
		
		final String update = "UPDATE Attendant SET accepted=1, timestamp='2017-04-25T16:34:22' WHERE user_id='10' AND event_id=2"; 
		verify(qmConnector).updateDataset(update, DATASET, RESOURCE);
	}
	
	@Test
	public void testSetAttendantAcceptedStatusUnexpectedError() throws DataWrapperException, QMConnectorException {
		exception.expect(DataWrapperException.class);
	    exception.expectMessage("Unexpected error");
	    
	    when(qmConnector.updateDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
	    	.thenThrow(new QMConnectorException("Unexpected error"));
	    
	    dataWrapper.setAttendantAcceptedStatus(2, "10", true);
	}
	
	@Test
	public void testProcessLanguageList() {
		final JsonObject languageResponse = Json.createObjectBuilder()
			.add("count", 2)
			.add("rows", Json.createArrayBuilder()
				.add(Json.createObjectBuilder()
					.add("lang", "ES")
				)
				.add(Json.createObjectBuilder()
					.add("lang", "EU")
				)
			).build();
		
		final List<Language> languages = dataWrapper.processLanguageList(languageResponse);
		
		final List<Language> expected = new ArrayList<Language>();
		expected.add(Language.ES);
		expected.add(Language.EU);
		
		assertEquals(expected, languages);
	}
	
	@Test
	public void testGetAvailableTranslations() throws QMConnectorException, DataWrapperException {
		final JsonObject languageResponse = Json.createObjectBuilder()
				.add("count", 2)
				.add("rows", Json.createArrayBuilder()
					.add(Json.createObjectBuilder()
						.add("lang", "ES")
					)
					.add(Json.createObjectBuilder()
						.add("lang", "EU")
					)
				).build();
		
		when(qmConnector.queryDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(languageResponse);
		
		final List<Language> languages = dataWrapper.getAvailableTranslations(2);
		
		final List<Language> expected = new ArrayList<Language>();
		expected.add(Language.ES);
		expected.add(Language.EU);
		
		assertEquals(expected, languages);
		
		final String query = "SELECT lang FROM EventTranslation WHERE event_id=2";
		verify(qmConnector).queryDataset(query, DATASET, RESOURCE);
	}
	
	@Test
	public void testGetAvailableTranslationsUnexpectedError() throws QMConnectorException, DataWrapperException {
		exception.expect(DataWrapperException.class);
	    exception.expectMessage("Unexpected error");
	    
	    when(qmConnector.queryDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
	    	.thenThrow(new QMConnectorException("Unexpected error"));
		
		dataWrapper.getAvailableTranslations(2);
	}
	
	@Test
	public void testGetUserEvents() throws QMConnectorException, DataWrapperException {
		final JsonObject attendantResponse = Json.createObjectBuilder()
			.add("count", 2)
			.add("rows", Json.createArrayBuilder()
				.add(attendant1)
				.add(attendant2)
			).build();
		
		when(qmConnector.queryDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(attendantResponse);
		
		final List<Attendant> attendants = dataWrapper.getUserEvents("10");
		assertEquals(2, attendants.size());
	}
	
	@Test
	public void testGetUserEventsUnexpectedEror() throws QMConnectorException, DataWrapperException {
		exception.expect(DataWrapperException.class);
	    exception.expectMessage("Unexpected error");
	    
	    when(qmConnector.queryDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
	    	.thenThrow(new QMConnectorException("Unexpected error"));
	    
	    dataWrapper.getUserEvents("10");
	}
}
