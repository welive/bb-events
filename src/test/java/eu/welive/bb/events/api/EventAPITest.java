/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.events.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.core.SecurityContext;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import eu.welive.bb.events.auth.user.OAuthUserInfo;
import eu.welive.bb.events.data.DataWrapper;
import eu.welive.bb.events.data.DataWrapperException;
import eu.welive.bb.events.data.qm.QMConnector;
import eu.welive.bb.events.exception.EntityNotFoundException;
import eu.welive.bb.events.exception.EventException;
import eu.welive.bb.events.exception.ForbiddenErrorException;
import eu.welive.bb.events.profile.ProfileManager;
import eu.welive.bb.events.serialization.Attendant;
import eu.welive.bb.events.serialization.Event;
import eu.welive.bb.events.serialization.Event.Language;
import eu.welive.bb.events.serialization.EventTranslation;
import eu.welive.bb.events.serialization.Message;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ProfileManager.class, EventAPI.class })
public class EventAPITest {
	
	@Rule
	public ExpectedException exception = ExpectedException.none();
	
	private static final String invalidDate = "2050-03-01TAA-BB-CC";
	private static final String startDate = "2017-04-24T09:00:00";
	private static final String endDate = "2017-04-24T18:00:00";
	
	private static final String[] users = new String[] {"10", "14"};
	private static final List<String> adminUsers = Arrays.asList(users);
	
	private static final String AUTH_HEADER = "Bearer 1111-1111-1111-1111";
	
	private DataWrapper dataWrapper;
	private EventAPI api;
	private QMConnector qmConnector;
	
	@Before
	public void setUp() throws IOException {
		dataWrapper = Mockito.mock(DataWrapper.class);
		
		qmConnector = Mockito.mock(QMConnector.class);
		when(dataWrapper.getQMConnector())
			.thenReturn(qmConnector);
		
		api = new EventAPI(dataWrapper, adminUsers);
	}
	
	@Test
	public void testIsValidRange() {
		assertTrue(api.isValidRange(startDate, endDate));
	}
	
	@Test
	public void testIsValidRangeParsingError() {
		assertFalse(api.isValidRange(invalidDate, endDate));
	}
	
	@Test
	public void testInvalidStartDate() throws EventException { 
		exception.expect(EventException.class);
	    exception.expectMessage("Invalid range starting date. Use yyyy-MM-ddTHH:mm:ss");
		
		api.getEventList(invalidDate, endDate, "", "", "", "", Event.Language.ES);
	}
	
	@Test
	public void testInvalidEndDate() throws EventException { 
		exception.expect(EventException.class);
	    exception.expectMessage("Invalid range ending date. Use yyyy-MM-ddTHH:mm:ss");
		
		api.getEventList(startDate, invalidDate, "", "", "", "", Event.Language.ES);
	}
	
	@Test
	public void testInvalidRange() throws EventException { 
		exception.expect(EventException.class);
	    exception.expectMessage("Invalid date range. Ending date must be greater than starting date");
		
		api.getEventList(endDate, startDate, "", "", "", "", Event.Language.ES);
	}
	
	private List<Event> createEventList(int size) {
		final List<Event> events = new ArrayList<>();
		
		for (int i = 1; i <= size; i++) {
			final Event e = new Event();
			e.setId(i);
			e.setTitle("Title " + i);			
			events.add(e);
		}
		
		return events;
	}
	
	@Test
	public void testGetAllEvents() throws EventException, DataWrapperException {
		when(dataWrapper.filterEvents("", "", "", "", "", "", Language.ES))
			.thenReturn(createEventList(3));
		
		final List<Event> event = api.getEventList("", "", "", "", "", "", Language.ES).getEvents();
		
		assertEquals(3, event.size());
	}
	
	@Test
	public void testFilterEvents() throws EventException, DataWrapperException {
		when(dataWrapper.filterEvents(startDate, endDate, "", "", "", "", Language.ES))
			.thenReturn(createEventList(2));
		
		final List<Event> event = api.getEventList(startDate, endDate, "", "", "", "", Language.ES).getEvents();
		
		assertEquals(2, event.size());
	}
	
	@Test
	public void testGetEventsDataWrapperError() throws EventException, DataWrapperException {
		when(dataWrapper.filterEvents(startDate, endDate, "", "", "", "",  Language.ES))
			.thenThrow(new DataWrapperException("Some internal error"));
		
		exception.expect(EventException.class);
	    exception.expectMessage("Some internal error");
		
		api.getEventList(startDate, endDate, "", "", "", "", Language.ES);
	}
	
	@Test
	public void testGetEventsInvalidRange() throws EventException, DataWrapperException {
		exception.expect(EventException.class);
	    exception.expectMessage("Invalid date range. Ending date must be greater than starting date");
		
		api.getEventList(endDate, startDate, "", "", "", "", Language.ES);
	}
	
	@Test
	public void testCheckEventDataEmptyStart() throws EventException {
		exception.expect(EventException.class);
	    exception.expectMessage("Invalid starting date. Use yyyy-MM-ddTHH:mm:ss");
	    
	    final Event event = new Event();
		api.checkEventData(event);
	}
	
	@Test
	public void testCheckEventDataInvalidStart() throws EventException {
		exception.expect(EventException.class);
	    exception.expectMessage("Invalid starting date. Use yyyy-MM-ddTHH:mm:ss");
	    
	    final Event event = new Event();
	    event.setStart("2017-03-15TAA:00:00");
		api.checkEventData(event);
	}
	
	@Test
	public void testCheckEventDataEmptyEnd() throws EventException {
		exception.expect(EventException.class);
	    exception.expectMessage("Invalid ending date. Use yyyy-MM-ddTHH:mm:ss");
	    
	    final Event event = new Event();
	    event.setStart("2017-04-25T08:00:00");
		api.checkEventData(event);
	}
	
	@Test
	public void testCheckEventDataInvalidEnd() throws EventException {
		exception.expect(EventException.class);
	    exception.expectMessage("Invalid ending date. Use yyyy-MM-ddTHH:mm:ss");
	    
	    final Event event = new Event();
	    event.setStart("2017-04-25T08:00:00");
	    event.setEnd("2017-03-15TAA:00:00");
		api.checkEventData(event);
	}
	
//	@Test
//	public void testCheckEventDataEmptyCoordinates() throws EventException {
//		exception.expect(EventException.class);
//	    exception.expectMessage("Invalid coordinates");
//	    
//	    final Event event = new Event();
//	    event.setStart("2017-04-25T08:00:00");
//	    event.setEnd("2017-04-25T15:00:00");
//		api.checkEventData(event);
//	}
	
	@Test
	public void testCheckEventData() throws EventException {    
	    final Event event = new Event();
	    event.setStart("2017-04-25T08:00:00");
	    event.setEnd("2017-04-25T15:00:00");
	    event.setLatitude("43.2712491");
	    event.setLongitude("-2.9407463,17");
		api.checkEventData(event);
	}
	
	private SecurityContext prepareSecurityContextUser() {
		final SecurityContext sc = Mockito.mock(SecurityContext.class);
		final OAuthUserInfo userInfo = new OAuthUserInfo("10", "", AUTH_HEADER);
		
		when(sc.getUserPrincipal())
			.thenReturn(userInfo);
		
		return sc;
	}
	
	private SecurityContext prepareSecurityContextInvalidUser() {
		final SecurityContext sc = Mockito.mock(SecurityContext.class);
		final OAuthUserInfo userInfo = new OAuthUserInfo("20", "", "");
		
		when(sc.getUserPrincipal())
			.thenReturn(userInfo);
		
		return sc;
	}
	
//	@Test(expected=ForbiddenErrorException.class)
//	public void testUpdateTranslationInvalidUser() throws DataWrapperException, EventException {
//		when(dataWrapper.updateTranslation(Matchers.any(Event.class), Matchers.eq(Language.EN)))
//			.thenReturn(1);
//		
//		final Event event = new Event();
//		event.setTitle("Title 1");
//		event.setDescription("Description 1");
//		event.setLocation("Location 1");
//		event.setContents("Contents 1");
//		event.setStart("2017-04-25T08:00:00");
//	    event.setEnd("2017-04-25T15:00:00");
//	    event.setLatitude("43.2712491");
//	    event.setLongitude("-2.9407463");
//		
//		api.updateTranslation(prepareSecurityContextInvalidUser(), 1, Language.EN, event);
//	}
	
	@Test
	public void testUpdateTranslation() throws DataWrapperException, EventException {
		when(dataWrapper.updateTranslation(Matchers.any(EventTranslation.class), Matchers.eq(Language.EN)))
			.thenReturn(1);
		
		final EventTranslation eventTranslation = new EventTranslation();
		eventTranslation.setTitle("Title 1");
		eventTranslation.setDescription("Description 1");
		eventTranslation.setLocation("Location 1");
		eventTranslation.setContents("Contents 1");
		
		api.updateTranslation(prepareSecurityContextUser(), 1, Language.EN, eventTranslation);
		
		verify(qmConnector).setAuthHeader(AUTH_HEADER);
	    verify(dataWrapper).updateTranslation(eventTranslation, Language.EN);
	}
	
	@Test
	public void testUpdateTranslationUnexpectedError() throws DataWrapperException, EventException {
		exception.expect(EventException.class);
	    exception.expectMessage("Unexpected error");
		
		when(dataWrapper.updateTranslation(Matchers.any(EventTranslation.class), Matchers.eq(Language.EN)))
			.thenThrow(new DataWrapperException("Unexpected error"));
		
		api.updateTranslation(prepareSecurityContextUser(), 1, Language.EN, new EventTranslation());
	}
	
//	@Test(expected=ForbiddenErrorException.class)
//	public void testUpdateEventInvalidUser() throws DataWrapperException, EventException {
//		when(dataWrapper.updateEvent(Matchers.any(Event.class)))
//			.thenReturn(1);
//		
//		final Event event = new Event();
//		event.setTitle("Title 1");
//		event.setDescription("Description 1");
//		event.setLocation("Location 1");
//		event.setContents("Contents 1");
//		event.setStart("2017-04-25T08:00:00");
//	    event.setEnd("2017-04-25T15:00:00");
//	    event.setLatitude("43.2712491");
//	    event.setLongitude("-2.9407463");
//		
//		api.updateEvent(prepareSecurityContextInvalidUser(), 4, event);
//	}
	
	@Test
	public void testUpdateEvent() throws DataWrapperException, EventException {
		when(dataWrapper.updateEvent(Matchers.any(Event.class)))
			.thenReturn(1);
		
		final Event event = new Event();
		event.setTitle("Title 1");
		event.setDescription("Description 1");
		event.setLocation("Location 1");
		event.setContents("Contents 1");
		event.setStart("2017-04-25T08:00:00");
	    event.setEnd("2017-04-25T15:00:00");
	    event.setLatitude("43.2712491");
	    event.setLongitude("-2.9407463");
		
		api.updateEvent(prepareSecurityContextUser(), 4, event);
		
		verify(qmConnector).setAuthHeader(AUTH_HEADER);
	    verify(dataWrapper).updateEvent(event);
	}
	
	@Test
	public void testUpdateEventUnexpectedError() throws DataWrapperException, EventException {
		exception.expect(EventException.class);
	    exception.expectMessage("Unexpected error");
		
		when(dataWrapper.updateEvent(Matchers.any(Event.class)))
			.thenThrow(new DataWrapperException("Unexpected error"));
		
		final Event event = new Event();
		event.setTitle("Title 1");
		event.setDescription("Description 1");
		event.setLocation("Location 1");
		event.setContents("Contents 1");
		event.setStart("2017-04-25T08:00:00");
	    event.setEnd("2017-04-25T15:00:00");
	    event.setLatitude("43.2712491");
	    event.setLongitude("-2.9407463");
		
		api.updateEvent(prepareSecurityContextUser(), 4, event);
	}
	
	@Test
	public void testSaveEvent() throws DataWrapperException, EventException {
		when(dataWrapper.saveEvent(Matchers.any(Event.class)))
			.thenReturn(1);
		
		final Event event = new Event();
		event.setTitle("Title 1");
		event.setDescription("Description 1");
		event.setLocation("Location 1");
		event.setContents("Contents 1");
		event.setStart("2017-04-25T08:00:00");
	    event.setEnd("2017-04-25T15:00:00");
	    event.setLatitude("43.2712491");
	    event.setLongitude("-2.9407463");
	    
	    api.saveEvent(prepareSecurityContextUser(), event);
	    
	    verify(qmConnector).setAuthHeader(AUTH_HEADER);
	    verify(dataWrapper).saveEvent(event);
	}
	
//	@Test(expected=ForbiddenErrorException.class)
//	public void testSaveEventInvalidUser() throws DataWrapperException, EventException {
//		when(dataWrapper.saveEvent(Matchers.any(Event.class)))
//			.thenReturn(1);
//		
//		final Event event = new Event();
//		event.setTitle("Title 1");
//		event.setDescription("Description 1");
//		event.setLocation("Location 1");
//		event.setContents("Contents 1");
//		event.setStart("2017-04-25T08:00:00");
//	    event.setEnd("2017-04-25T15:00:00");
//	    event.setLatitude("43.2712491");
//	    event.setLongitude("-2.9407463");
//	    
//	    api.saveEvent(prepareSecurityContextInvalidUser(), event);
//	}
	
	@Test
	public void testSaveEventUnexpectedError() throws DataWrapperException, EventException {
		exception.expect(EventException.class);
	    exception.expectMessage("Unexpected error");
		
		when(dataWrapper.saveEvent(Matchers.any(Event.class)))
			.thenThrow(new DataWrapperException("Unexpected error"));
		
		final Event event = new Event();
		event.setTitle("Title 1");
		event.setDescription("Description 1");
		event.setLocation("Location 1");
		event.setContents("Contents 1");
		event.setStart("2017-04-25T08:00:00");
	    event.setEnd("2017-04-25T15:00:00");
	    event.setLatitude("43.2712491");
	    event.setLongitude("-2.9407463");
	    
	    api.saveEvent(prepareSecurityContextUser(), event);
	}
	
	@Test
	public void testGetEvent() throws DataWrapperException, EventException {
		final Event event = new Event();
		event.setId(1);
		event.setTitle("Title 1");
		event.setDescription("Description 1");
		event.setLocation("Location 1");
		event.setContents("Contents 1");
		event.setStart("2017-04-25T08:00:00");
	    event.setEnd("2017-04-25T15:00:00");
	    event.setLatitude("43.2712491");
	    event.setLongitude("-2.9407463");
	    event.setLang(Language.EN);
		
		when(dataWrapper.getEvent(1, Language.EN))
			.thenReturn(event);
		
		final Event responseEvent = api.getEvent(1, Language.EN);
		assertEquals(responseEvent, event);
	
	    verify(dataWrapper).getEvent(1, Language.EN);
	}
	
	@Test(expected=EntityNotFoundException.class)
	public void testGetEventNotFound() throws DataWrapperException, EventException {		
		when(dataWrapper.getEvent(1, Language.EN))
			.thenReturn(new Event());
		
		api.getEvent(1, Language.EN);
	}
	
	@Test
	public void testGetEventUnexpectedError() throws DataWrapperException, EventException {
		exception.expect(EventException.class);
	    exception.expectMessage("Unexpected error");
	    
		when(dataWrapper.getEvent(1, Language.EN))
			.thenThrow(new DataWrapperException("Unexpected error"));
		
		api.getEvent(1, Language.EN);
	}
	
	@Test
	public void testDeleteEvent() throws DataWrapperException, EventException {
		when(dataWrapper.deleteEvent(4))
			.thenReturn(1);
		
		final Message message = api.deleteEvent(prepareSecurityContextUser(), 4);
		
		assertEquals("Operation successful", message.getMsg());
		
		verify(qmConnector).setAuthHeader(AUTH_HEADER);
	    verify(dataWrapper).deleteEvent(4);
	}
	
	@Test(expected=ForbiddenErrorException.class)
	public void testDeleteEventInvalidUser() throws DataWrapperException, EventException {
		when(dataWrapper.deleteEvent(4))
			.thenReturn(1);
		
		final Message message = api.deleteEvent(prepareSecurityContextInvalidUser(), 4);
		
		assertEquals("Operation successful", message.getMsg());
	}
	
	@Test(expected=EntityNotFoundException.class)
	public void testDeleteEventNotFound() throws DataWrapperException, EventException {
		when(dataWrapper.deleteEvent(4))
			.thenReturn(0);
		
		api.deleteEvent(prepareSecurityContextUser(), 4);
	}
	
	@Test
	public void testDeleteEventUnexpectedError() throws DataWrapperException, EventException {
		exception.expect(EventException.class);
	    exception.expectMessage("Unexpected error");
	    
		when(dataWrapper.deleteEvent(4))
			.thenThrow(new DataWrapperException("Unexpected error"));
		
		api.deleteEvent(prepareSecurityContextUser(), 4);
	}
	
	@Test(expected=EntityNotFoundException.class)
	public void testSaveTranslationEventNotFound() throws DataWrapperException, EventException {		
		when(dataWrapper.saveTranslation(Matchers.any(EventTranslation.class), Matchers.eq(Language.ES)))
			.thenReturn(0);
		
		final EventTranslation eventTranslation = new EventTranslation();
		eventTranslation.setTitle("Title 1");
		eventTranslation.setDescription("Description 1");
		eventTranslation.setLocation("Location 1");
		eventTranslation.setContents("Contents 1");
		
		when(dataWrapper.getEvent(1, Language.ES))
			.thenReturn(new Event());
		
		api.saveTranslation(prepareSecurityContextUser(), 1, Language.ES, eventTranslation);
	}
	
	@Test
	public void testSaveTranslation() throws DataWrapperException, EventException {
		final EventTranslation eventTranslation = new EventTranslation();
		eventTranslation.setTitle("Title 1");
		eventTranslation.setDescription("Description 1");
		eventTranslation.setLocation("Location 1");
		eventTranslation.setContents("Contents 1");
		
		when(dataWrapper.saveTranslation(eventTranslation, Language.ES))
			.thenReturn(1);
		
		final Event defaultEvent = new Event();
	    defaultEvent.setId(1);
	    
	    when(dataWrapper.getEvent(1, Language.ES))
	    	.thenReturn(defaultEvent);
		
		final Message message = api.saveTranslation(prepareSecurityContextUser(), 1, Language.ES, eventTranslation);
		
		assertEquals("Operation successful", message.getMsg());
		
		verify(qmConnector).setAuthHeader(AUTH_HEADER);
	    
	    final ArgumentCaptor<EventTranslation> eventCaptor = ArgumentCaptor.forClass(EventTranslation.class);
		verify(dataWrapper).saveTranslation(eventCaptor.capture(), Matchers.eq(Language.ES));
		
		assertEquals(1, eventCaptor.getValue().getEventId());
	}
	
	@Test
	public void testSaveTranslationUnexpectedError() throws DataWrapperException, EventException {
		exception.expect(EventException.class);
	    exception.expectMessage("Unexpected error");
	    
	    final Event defaultEvent = new Event();
	    defaultEvent.setId(1);
	    
	    when(dataWrapper.getEvent(1, Language.ES))
	    	.thenReturn(defaultEvent);
		
		when(dataWrapper.saveTranslation(Matchers.any(EventTranslation.class), Matchers.eq(Language.ES)))
			.thenThrow(new DataWrapperException("Unexpected error"));
		
		api.saveTranslation(prepareSecurityContextUser(), 1, Language.ES, new EventTranslation());
	}
	
//	@Test(expected=ForbiddenErrorException.class)
//	public void testSaveTranslationInvalidUser() throws DataWrapperException, EventException {
//		final Event event = new Event();
//		event.setTitle("Title 1");
//		event.setDescription("Description 1");
//		event.setLocation("Location 1");
//		event.setContents("Contents 1");
//		
//		when(dataWrapper.saveTranslation(event, Language.ES))
//			.thenReturn(1);
//		
//		api.saveTranslation(prepareSecurityContextInvalidUser(), 1, Language.ES, event);
//	}
	
	@Test
	public void testDeleteTranslation() throws DataWrapperException, EventException {
		when(dataWrapper.deleteTranslation(4, Language.EN))
			.thenReturn(1);
		
		final Message message = api.deleteTranslation(prepareSecurityContextUser(), 4, Language.EN);
		
		assertEquals("Operation successful", message.getMsg());
		
		verify(qmConnector).setAuthHeader(AUTH_HEADER);
	    verify(dataWrapper).deleteTranslation(4, Language.EN);
	}
	
	@Test(expected=ForbiddenErrorException.class)
	public void testDeleteTranslationInvalidUser() throws DataWrapperException, EventException {
		when(dataWrapper.deleteTranslation(4, Language.EN))
			.thenReturn(1);
		
		api.deleteTranslation(prepareSecurityContextInvalidUser(), 4, Language.EN);
	}
	
	@Test(expected=EntityNotFoundException.class)
	public void testDeleteTranslationNotFound() throws DataWrapperException, EventException {
		when(dataWrapper.deleteTranslation(4, Language.EN))
			.thenReturn(0);
		
		api.deleteTranslation(prepareSecurityContextUser(), 4, Language.EN);
	}
	
	@Test
	public void testDeleteTranslationUnexpectedError() throws DataWrapperException, EventException {
		exception.expect(EventException.class);
	    exception.expectMessage("Unexpected error");
	    
	    when(dataWrapper.deleteTranslation(4, Language.EN))
			.thenThrow(new DataWrapperException("Unexpected error"));
		
	    api.deleteTranslation(prepareSecurityContextUser(), 4, Language.EN);
	}
	
	private List<Attendant> createAttendantList() {
		final List<Attendant> attendants = new ArrayList<Attendant>();
		
		final Attendant att1 = new Attendant();
		att1.setUserId("10");
		att1.setCheckin(false);
		att1.setTimestamp("2017-04-02T09:10:00");
		
		attendants.add(att1);
		
		final Attendant att2 = new Attendant();
		att2.setUserId("14");
		att2.setCheckin(true);
		att2.setTimestamp("2017-05-02T09:10:00");
		
		attendants.add(att2);
		
		final Attendant att3 = new Attendant();
		att3.setUserId("20");
		att3.setCheckin(true);
		att3.setTimestamp("2017-05-02T09:15:00");
		
		attendants.add(att3);
		
		return attendants;
	}
	
	@Test
	public void testGetAttendants() throws DataWrapperException, EventException {
		when(dataWrapper.getAttendants(1))
			.thenReturn(createAttendantList());
		
		final Event event = new Event();
	    event.setId(1);
	    
	    when(dataWrapper.getEvent(1, Language.ES))
	    	.thenReturn(event);
		
		final List<Attendant> attendants = api.getAttendantList(prepareSecurityContextUser(), 1).getAttendants();
		assertEquals(3, attendants.size());
		
		verify(qmConnector).setAuthHeader(AUTH_HEADER);
	}
	
	@Test(expected=EntityNotFoundException.class)
	public void testGetAttendantsEventNotFound() throws DataWrapperException, EventException {
		when(dataWrapper.getEvent(1, Language.ES))
			.thenReturn(new Event());
		
		api.getAttendantList(prepareSecurityContextUser(), 1);
	}
	
	@Test
	public void testGetAttendantsUnexpectedError() throws DataWrapperException, EventException {
		exception.expect(EventException.class);
	    exception.expectMessage("Unexpected error");
		
		when(dataWrapper.getAttendants(1))
			.thenThrow(new DataWrapperException("Unexpected error"));
		
		final Event event = new Event();
	    event.setId(1);
	    
	    when(dataWrapper.getEvent(1, Language.ES))
	    	.thenReturn(event);
		
		api.getAttendantList(prepareSecurityContextUser(), 1);
	}
	
	@Test
	public void testAddAttendant() throws DataWrapperException, EventException {
		final Event event = new Event();
		event.setId(1); 
	    
		when(dataWrapper.getEvent(1, Language.ES))
			.thenReturn(event);
		
		when(dataWrapper.isAttendant(1, "10"))
			.thenReturn(false);
		
		final Message message = api.addAttendant(AUTH_HEADER, 1, "10");
		verify(dataWrapper).addAttendant(1, "10");
		
		assertEquals("Operation successful", message.getMsg());
	}
	
	@Test
	public void testAddAttendantAlreadyAdded() throws DataWrapperException, EventException {
		final Event event = new Event();
		event.setId(1); 
	    
		when(dataWrapper.getEvent(1, Language.ES))
			.thenReturn(event);
		
		when(dataWrapper.isAttendant(1, "10"))
			.thenReturn(true);
		
		final Message message = api.addAttendant(AUTH_HEADER, 1, "10");
		verify(dataWrapper, times(0)).addAttendant(1, "10");
		
		assertEquals("Operation successful", message.getMsg());
	}
	
	@Test
	public void testAddUserAttendant() throws DataWrapperException, EventException {
		final Event event = new Event();
		event.setId(1); 
	    
		when(dataWrapper.getEvent(1, Language.ES))
			.thenReturn(event);
		
		final Message message = api.addUserAsAttendant(prepareSecurityContextUser(), 1);
		
		assertEquals("Operation successful", message.getMsg());
		
		verify(qmConnector).setAuthHeader(AUTH_HEADER);
	}
	
	@Test(expected=EntityNotFoundException.class)
	public void testAddAttendantEventNotFound() throws DataWrapperException, EventException {
		when(dataWrapper.getEvent(1, Language.ES))
			.thenReturn(new Event());
		
		api.addAttendant(AUTH_HEADER, 1, "10");
	}
	
	@Test
	public void testAddAttendantUnexpectedError() throws DataWrapperException, EventException {
		exception.expect(EventException.class);
	    exception.expectMessage("Unexpected error");
	    
		when(dataWrapper.getEvent(1, Language.ES))
			.thenThrow(new DataWrapperException("Unexpected error"));
		
		api.addAttendant(AUTH_HEADER, 1, "10");
	}
	
	@Test
	public void testRemoveAttendant() throws DataWrapperException, EventException {
		when(dataWrapper.removeAttendant(1, "10"))
			.thenReturn(1);
		
		final Event event = new Event();
	    event.setId(1);
	    
	    when(dataWrapper.getEvent(1, Language.ES))
	    	.thenReturn(event);
	    
	    when(dataWrapper.isAttendant(1, "10"))
			.thenReturn(true);
		
		final Message message = api.removeAttendant(AUTH_HEADER, 1, "10");
		
		assertEquals("Operation successful", message.getMsg());
	}
	
	@Test(expected=EntityNotFoundException.class)
	public void testRemoveAttendantNotExists() throws DataWrapperException, EventException {
		when(dataWrapper.removeAttendant(1, "10"))
			.thenReturn(1);
		
		final Event event = new Event();
	    event.setId(1);
	    
	    when(dataWrapper.getEvent(1, Language.ES))
	    	.thenReturn(event);
	    
	    when(dataWrapper.isAttendant(1, "10"))
			.thenReturn(false);
		
		api.removeAttendant(AUTH_HEADER, 1, "10");
	}
	
	@Test
	public void testRemoveUserAsAttendant() throws DataWrapperException, EventException {
		when(dataWrapper.removeAttendant(1, "10"))
			.thenReturn(1);
		
		final Event event = new Event();
	    event.setId(1);
	    
	    when(dataWrapper.getEvent(1, Language.ES))
	    	.thenReturn(event);
	    
	    when(dataWrapper.isAttendant(1, "10"))
			.thenReturn(true);
		
		final Message message = api.removeUserAsAttendant(prepareSecurityContextUser(), 1);
		
		assertEquals("Operation successful", message.getMsg());
		
		verify(qmConnector).setAuthHeader(AUTH_HEADER);
	}
	
	@Test(expected=EntityNotFoundException.class)
	public void testRemoveAttendantNotFound() throws DataWrapperException, EventException {
		when(dataWrapper.removeAttendant(1, "10"))
			.thenReturn(0);
		
		final Event event = new Event();
	    event.setId(1);
	    
	    when(dataWrapper.getEvent(1, Language.ES))
	    	.thenReturn(event);
		
		api.removeAttendant(AUTH_HEADER, 1, "10");
	}
	
	@Test(expected=EntityNotFoundException.class)
	public void testRemoveAttendantEventNotFound() throws DataWrapperException, EventException {
		when(dataWrapper.removeAttendant(1, "10"))
			.thenReturn(1);
			    
	    when(dataWrapper.getEvent(1, Language.ES))
	    	.thenReturn(new Event());
		
		api.removeAttendant(AUTH_HEADER, 1, "10");
	}
	
	@Test
	public void testRemoveAttendantUnexpectedError() throws DataWrapperException, EventException {
		exception.expect(EventException.class);
	    exception.expectMessage("Unexpected error");
	    
	    final Event event = new Event();
	    event.setId(1);
	    
	    when(dataWrapper.getEvent(1, Language.ES))
	    	.thenReturn(event);
	    
		when(dataWrapper.removeAttendant(1, "10"))
			.thenThrow(new DataWrapperException("Unexpected error"));
		
		when(dataWrapper.isAttendant(1, "10"))
			.thenReturn(true);
		
		api.removeAttendant(AUTH_HEADER, 1, "10");
	}
	
	@Test(expected=EntityNotFoundException.class)
	public void testCheckinAttendantEventNotFound() throws DataWrapperException, EventException {
		when(dataWrapper.checkinAttendant(1, "10"))
		.thenReturn(1);
		    
	    when(dataWrapper.getEvent(1, Language.ES))
	    	.thenReturn(new Event());
		
		api.checkinAttendant(prepareSecurityContextUser(), 1, "10");
	}
	
	@Test(expected=EntityNotFoundException.class)
	public void testCheckinAttendantNotFound() throws DataWrapperException, EventException {
		when(dataWrapper.checkinAttendant(1, "10"))
		.thenReturn(0);
		    
		final Event event = new Event();
	    event.setId(1);
	    
	    when(dataWrapper.getEvent(1, Language.ES))
	    	.thenReturn(event);
		
	    api.checkinAttendant(prepareSecurityContextUser(), 1, "10");
	}
	
	@Test
	public void testCheckinAttendantUnexpectedError() throws DataWrapperException, EventException {
		exception.expect(EventException.class);
	    exception.expectMessage("Unexpected error");
	    
		when(dataWrapper.checkinAttendant(1, "10"))
			.thenThrow(new DataWrapperException("Unexpected error"));
		    
		final Event event = new Event();
	    event.setId(1);
	    
	    when(dataWrapper.getEvent(1, Language.ES))
	    	.thenReturn(event);
	    
	    when(dataWrapper.isAttendant(1, "10"))
    		.thenReturn(true);
		
	    api.checkinAttendant(prepareSecurityContextUser(), 1, "10");
	}
	
	@Test
	public void testCheckinAttendant() throws DataWrapperException, EventException {
		when(dataWrapper.checkinAttendant(1, "10"))
			.thenReturn(1);
		    
		final Event event = new Event();
	    event.setId(1);
	    
	    when(dataWrapper.getEvent(1, Language.ES))
	    	.thenReturn(event);
	    
	    when(dataWrapper.isAttendant(1, "10"))
    		.thenReturn(true);
		
	    final Message message = api.checkinAttendant(prepareSecurityContextUser(), 1, "10");
	    
	    assertEquals("Operation successful", message.getMsg());
	    
	    verify(qmConnector).setAuthHeader(AUTH_HEADER);
	}
	
	@Test(expected=ForbiddenErrorException.class)
	public void testCheckinAttendantInvalidUser() throws DataWrapperException, EventException {
		when(dataWrapper.checkinAttendant(1, "10"))
			.thenReturn(1);
		    
		final Event event = new Event();
	    event.setId(1);
	    
	    when(dataWrapper.getEvent(1, Language.ES))
	    	.thenReturn(event);
		
	    api.checkinAttendant(prepareSecurityContextInvalidUser(), 1, "10");
	}
	
	@Test(expected=EntityNotFoundException.class)
	public void testCheckoutAttendantEventNotFound() throws DataWrapperException, EventException {
		when(dataWrapper.checkinAttendant(1, "10"))
		.thenReturn(1);
		    
	    when(dataWrapper.getEvent(1, Language.ES))
	    	.thenReturn(new Event());
		
		api.checkoutAttendant(prepareSecurityContextUser(), 1, "10");
	}
	
	@Test(expected=EntityNotFoundException.class)
	public void testCheckoutAttendantNotFound() throws DataWrapperException, EventException {
		when(dataWrapper.checkinAttendant(1, "10"))
		.thenReturn(0);
		    
		final Event event = new Event();
	    event.setId(1);
	    
	    when(dataWrapper.getEvent(1, Language.ES))
	    	.thenReturn(event);
		
	    api.checkoutAttendant(prepareSecurityContextUser(), 1, "10");
	}
	
	@Test
	public void testCheckoutAttendantUnexpectedError() throws DataWrapperException, EventException {
		exception.expect(EventException.class);
	    exception.expectMessage("Unexpected error");
	    
		when(dataWrapper.checkoutAttendant(1, "10"))
			.thenThrow(new DataWrapperException("Unexpected error"));
		    
		final Event event = new Event();
	    event.setId(1);
	    
	    when(dataWrapper.getEvent(1, Language.ES))
	    	.thenReturn(event);
	    
	    when(dataWrapper.isAttendant(1, "10"))
	    	.thenReturn(true);
		
	    api.checkoutAttendant(prepareSecurityContextUser(), 1, "10");
	}
	
	@Test
	public void testCheckoutAttendant() throws DataWrapperException, EventException {
		when(dataWrapper.checkoutAttendant(1, "10"))
			.thenReturn(1);
		    
		final Event event = new Event();
	    event.setId(1);
	    
	    when(dataWrapper.getEvent(1, Language.ES))
	    	.thenReturn(event);
	    
	    when(dataWrapper.isAttendant(1, "10"))
    		.thenReturn(true);
		
	    final Message message = api.checkoutAttendant(prepareSecurityContextUser(), 1, "10");
	    
	    assertEquals("Operation successful", message.getMsg());
	    
	    verify(qmConnector).setAuthHeader(AUTH_HEADER);
	}
	
	@Test(expected=ForbiddenErrorException.class)
	public void testCheckoutAttendantInvalidUser() throws DataWrapperException, EventException {
		when(dataWrapper.checkoutAttendant(1, "10"))
			.thenReturn(1);
		    
		final Event event = new Event();
	    event.setId(1);
	    
	    when(dataWrapper.getEvent(1, Language.ES))
	    	.thenReturn(event);
		
	    api.checkoutAttendant(prepareSecurityContextInvalidUser(), 1, "10");
	}
	
//	private BasicProfile createBasicProfile(String userId, String name, String surname) {
//		final BasicProfile basicProfile = new BasicProfile();
//		
//		basicProfile.setUserId(userId);
//		basicProfile.setName(name);
//		basicProfile.setSurname(surname);
//		
//		return basicProfile;
//	}
//	
//	private AccountProfile createAccountProfile(String name, String surname, String email) {
//		final AccountProfile accountProfile = new AccountProfile();
//		
//		final GoogleProvider provider = new GoogleProvider(name, surname, email);
//		accountProfile.addProvider(provider);
//		
//		return accountProfile;
//	}
	
//	@Test
//	public void testGetCSV() throws Exception {
//		final Event event = new Event();
//		event.setId(1);
//		event.setTitle("Title 1");
//		event.setDescription("Description 1");
//		event.setLocation("Location 1");
//		event.setContents("Contents 1");
//		event.setStart("2017-04-25T08:00:00");
//	    event.setEnd("2017-04-25T15:00:00");
//	    event.setLatitude("43.2712491");
//	    event.setLongitude("-2.9407463");
//	    event.setLang(Language.EN);
//	    
//		when(dataWrapper.getEvent(2, Language.ES))
//			.thenReturn(event);
//		
//		when(dataWrapper.getAttendants(2))
//			.thenReturn(createAttendantList());
//		
//		final ProfileManager profileManager = PowerMockito.mock(ProfileManager.class);
//		PowerMockito.whenNew(ProfileManager.class)
//			.withNoArguments()
//			.thenReturn(profileManager);
//		
//		when(profileManager.getBasicInfo("10"))
//			.thenReturn(createBasicProfile("10", "User-10", "Surname-10"));
//		
//		when(profileManager.getBasicInfo("14"))
//			.thenReturn(createBasicProfile("14", "User-14", "Surname-14"));
//		
//		when(profileManager.getBasicInfo("20"))
//			.thenReturn(createBasicProfile("20", "User-20", "Surname-20"));
//		
//		when(profileManager.getExtendedInfo("10"))
//			.thenReturn(createAccountProfile("User-10", "Surname-10", "email-10"));
//		
//		when(profileManager.getExtendedInfo("14"))
//			.thenReturn(createAccountProfile("User-14", "Surname-14", "email-14"));
//		
//		when(profileManager.getExtendedInfo("20"))
//			.thenReturn(createAccountProfile("User-20", "Surname-20", "email-20"));
//		
//		final String expectedCSV = "Inscrito,E-mail,Aceptado,Asistido\r\n"
//			+ "User-10 Surname-10,email-10,No,No\r\n"
//			+ "User-14 Surname-14,email-14,No,Si\r\n"
//			+ "User-20 Surname-20,email-20,No,Si\r\n"; 
//		
//		final String csv = api.getCSV(AUTH_HEADER, 2);
//		assertEquals(expectedCSV, csv);
//	}
}
