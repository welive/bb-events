/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.thymeleaf.ITemplateEngine;

import eu.welive.bb.web.app.Application;
import eu.welive.bb.web.controller.IController;

@WebFilter(urlPatterns = { "/*" })
public class ApplicationFilter implements Filter {

	private ServletContext servletContext;
	private Application application;

	public void init(final FilterConfig filterConfig) throws ServletException {
		this.servletContext = filterConfig.getServletContext();
		this.application = new Application(this.servletContext);
	}

	public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain)
			throws IOException, ServletException {
		if (!process((HttpServletRequest) request, (HttpServletResponse) response)) {
			chain.doFilter(request, response);
		}
	}

	@Override
	public void destroy() {

	}

	private boolean process(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		try {
			if (request.getRequestURI().contains("css") || request.getRequestURI().contains("images") ||
				request.getRequestURI().contains("js") || request.getRequestURI().contains("favicon")) {
				return false;
			}

			IController controller = this.application.resolveControllerForRequest(request);
			if (controller == null) {
				return false;
			}

			ITemplateEngine templateEngine = this.application.getTemplateEngine();

			response.setContentType("text/html;charset=UTF-8");
			response.setHeader("Pragma", "no-cache");
			response.setHeader("Cache-Control", "no-cache");
			response.setDateHeader("Expires", 0);

			controller.process(request, response, this.servletContext, templateEngine);

			return true;

		} catch (Exception e) {
			try {
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			} catch (final IOException ignored) {
				// Just ignore this
			}
			throw new ServletException(e);
		}

	}
}
