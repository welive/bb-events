/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.web.survey;

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import eu.welive.bb.events.config.Config;

public class SurveyManager {

	public List<Survey> geSurveys(String token) {
		final Client client = ClientBuilder.newClient();
		final WebTarget target = client.target(Config.getInstance().getBBSurveyURL()).path("/api/survey/all");
		
		final List<Survey> surveys = target
			.request(MediaType.APPLICATION_JSON)
			.header("Authorization", "Bearer " + token)
			.get(new GenericType<List<Survey>>() {});
		
		return surveys;
	}
	
	public NumResponses getNumResponses(String token, int surveyId, int objectId) {
		final Client client = ClientBuilder.newClient();
		
		final String path = String.format("/api/survey/%d/response/%d/count", surveyId, objectId);
		final WebTarget target = client.target(Config.getInstance().getBBSurveyURL()).path(path);
		
		final NumResponses numResponse = target
			.request(MediaType.APPLICATION_JSON)
			.header("Authorization", "Bearer " + token)
			.get(NumResponses.class);
		
		return numResponse;
	}
	
	public String getCSV(String token, int surveyId, int objectId) {
		final Client client = ClientBuilder.newClient();
		
		final String path = String.format("/api/survey/%d/response/%d/csv", surveyId, objectId);
		final WebTarget target = client.target(Config.getInstance().getBBSurveyURL()).path(path);
		
		final String csv = target
			.request(MediaType.TEXT_PLAIN)
			.header("Authorization", "Bearer " + token)
			.get(String.class);
		
		return csv;
	}
}
