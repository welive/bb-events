/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.web.controller.events;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringWriter;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.WebContext;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import eu.welive.bb.events.api.EventAPI;
import eu.welive.bb.events.serialization.Event;
import eu.welive.bb.events.serialization.EventTranslation;
import eu.welive.bb.web.controller.CommonController;
import eu.welive.bb.web.controller.notifier.Notifier;

public class SaveEventController extends CommonController {

	@Override
	public void processLogged(HttpServletRequest request, HttpServletResponse response, ServletContext servletContext,
			ITemplateEngine templateEngine, WebContext ctx) throws Exception {
		
		final EventAPI eventAPI = new EventAPI();
		final Event event = parseEvent(request);
		
		final Notifier notifier = new Notifier();
		
		boolean edit = false;
		if (request.getParameter("edit") != null) {
			edit = Boolean.parseBoolean(request.getParameter("edit"));
		}
			
		if (!edit) {
			if (event.getLang() == eventAPI.getDefaultLang()) {
				eventAPI.saveEvent(getBearerToken(), event);
				notifier.notifyNewEvent(event);
			} else {
				final EventTranslation eventTranslation = new EventTranslation(event);
				eventAPI.saveTranslation(getBearerToken(), event.getId(), event.getLang(), eventTranslation);
			}
		} else {
			eventAPI.updateEvent(getBearerToken(), event.getId(), event);
			final EventTranslation eventTranslation = new EventTranslation(event);
			eventAPI.updateTranslation(getBearerToken(), event.getId(), event.getLang(), eventTranslation);
		}
	}

	private Event parseEvent(HttpServletRequest request) throws IOException {
		final StringWriter strWriter = new StringWriter();
		
		String line = null;
		final BufferedReader reader = request.getReader();
		while ((line = reader.readLine()) != null) {
			strWriter.append(line);
		}
		
		final Gson gson = new GsonBuilder().create();
		final Event event = gson.fromJson(strWriter.toString(), Event.class);
		return event;
	}
}
