/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.web.controller;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import org.json.JSONObject;
import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.WebContext;

import eu.welive.bb.events.config.Config;

public abstract class CommonController implements IController {
	
	public static final String TOKEN = "token";
	public static final String USER_ID = "userid";
	public static final String FULL_NAME = "fullname";
	public static final String EMAIL = "email";
	public static final String WELIVE_API = "weliveapi";
	
	protected boolean isLogged = false;
	protected String token = "";
	protected String userId = "";
	protected String fullName = "";
	protected String email = "";
	
	@Override
	public void process(HttpServletRequest request, HttpServletResponse response, ServletContext servletContext,
			ITemplateEngine templateEngine) throws Exception {
		
		if (request.getSession(true).getAttribute(TOKEN) != null) {
			token = (String) request.getSession(true).getAttribute(TOKEN);
		}
		
		if (token.isEmpty() || !validToken(token)) {
			isLogged = false;
			response.sendRedirect("index");
		} else {
			isLogged = true;
			
			userId = (String) request.getSession(true).getAttribute(USER_ID);
			fullName = (String) request.getSession(true).getAttribute(FULL_NAME);
			email = (String) request.getSession(true).getAttribute(EMAIL);
			
			WebContext ctx = new WebContext(request, response, servletContext, request.getLocale());
			ctx.setVariable(TOKEN, token);
			ctx.setVariable(FULL_NAME, fullName);
			ctx.setVariable(EMAIL, email);
			ctx.setVariable(WELIVE_API, Config.getInstance().getWeLiveAPI());
			processLogged(request, response, servletContext, templateEngine, ctx);
		}
	}
	
	public String getBearerToken() {
		return "Bearer " + token;
	}
	
	public abstract void processLogged(HttpServletRequest request, HttpServletResponse response, ServletContext servletContext,
			ITemplateEngine templateEngine, WebContext ctx) throws Exception;
	
	public void invalidateSessionToken(HttpServletRequest request) {
		request.getSession(true).removeAttribute(TOKEN);
		token = "";
	}
	
	private boolean validToken(String token) {
		final Client client = ClientBuilder.newClient();
		final WebTarget target = client.target(Config.getInstance().getAacServiceURL()).path("/eauth/check_token");
		final String response = target
			.queryParam("token", token)
			.request()
			.get(String.class);
		
		final JSONObject jsonResponse = new JSONObject(response);
		return jsonResponse.has("client_id");
	}
}
