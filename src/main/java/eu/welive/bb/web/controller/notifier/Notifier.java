/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.web.controller.notifier;

import java.io.IOException;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import eu.welive.bb.events.config.Config;
import eu.welive.bb.events.serialization.Event;

class Notification {

	private String title;
	private String body;
	private String click_action;
	private String icon;
	private String color;
	private String sound;
	
	public Notification() {
		
	}
	
	public Notification(String title, String body) {
		this.title = title;
		this.body = body;
		this.click_action = "FCM_PLUGIN_ACTIVITY";
		this.icon = "fcm_push_icon";
		this.color = "blue";
		this.sound = "default";
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getClick_action() {
		return click_action;
	}

	public void setClick_action(String click_action) {
		this.click_action = click_action;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getSound() {
		return sound;
	}

	public void setSound(String sound) {
		this.sound = sound;
	}
}

class Message {
	
	private String to;
	private Notification notification;
	
	public Message() {
		
	}
	
	public Message(Notification notification) {
		this.to = "/topics/bigklubapp";
		this.notification = notification;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public Notification getNotification() {
		return notification;
	}

	public void setNotification(Notification notification) {
		this.notification = notification;
	}
}

public class Notifier {
	
	private static final String NEW_EVENT = "Nuevo evento creado";

	private static final String AUTH_HEADER = "Authorization";
	
	private final WebTarget target; 

	public Notifier() throws IOException {
		final Client client = ClientBuilder.newClient();
		target = client.target("https://fcm.googleapis.com/fcm/send");
	}
	
	public void notifyNewEvent(Event event) {
		Builder builder = target.request(MediaType.APPLICATION_JSON);
		builder = builder.header(AUTH_HEADER, "key=" + Config.getInstance().getNotifierToken());
		
		final Notification notification = new Notification(NEW_EVENT, event.getTitle());
		final Message message = new Message(notification);
		
		final Entity<Message> entity = Entity.entity(message, MediaType.APPLICATION_JSON);
		final Response response = builder.post(entity);
		if (response.getStatus() != Status.OK.getStatusCode()) {
			System.out.println("Could not send notification for new event");
			final String responseMsg = response.readEntity(String.class);
			System.out.println(responseMsg);
		} else {
			System.out.println("Notification for new event correctly sent");
		}
	}
}
