/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.web.controller.attendants;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.WebContext;

import eu.welive.bb.events.api.EventAPI;
import eu.welive.bb.web.controller.CommonController;

public class AddNewAttendant extends CommonController {

	@Override
	public void processLogged(HttpServletRequest request, HttpServletResponse response, ServletContext servletContext,
			ITemplateEngine templateEngine, WebContext ctx) throws Exception {
		if (request.getParameter("event_id") != null && request.getParameter("user_id") != null) {
			final int eventId = Integer.parseInt(request.getParameter("event_id"));
			final String userId = request.getParameter("user_id");
			
			final EventAPI eventAPI = new EventAPI();
			eventAPI.addAttendant(getBearerToken(), eventId, userId);
			
			response.sendRedirect(String.format("listAttendants?event_id=%d", eventId));
		} else {
			response.sendRedirect("listEvents");
		}
	}

}
