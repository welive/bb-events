/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.web.controller.general;

import java.util.Arrays;

import eu.welive.bb.events.serialization.Event.Language;
import eu.welive.bb.events.serialization.LanguageList;

public class SupportedLanguages {

	public static LanguageList getSupportedLanguages() {
		final LanguageList languageList = new LanguageList();
		languageList.setLanguages(Arrays.asList(new Language[] { Language.ES, Language.EU }));
		return languageList;
	}
}
