/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.web.controller.attendants;

import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.WebContext;

import eu.welive.bb.events.api.EventAPI;
import eu.welive.bb.events.profile.ProfileManager;
import eu.welive.bb.events.serialization.Attendant;
import eu.welive.bb.events.serialization.Event;
import eu.welive.bb.events.serialization.ExtendedAttendant;
import eu.welive.bb.web.controller.CommonController;
import eu.welive.bb.web.survey.NumResponses;
import eu.welive.bb.web.survey.SurveyManager;

public class ListAttendantsController extends CommonController {
	
	private int getNumChecked(List<ExtendedAttendant> attendants) {
		int counter = 0;
		for (Attendant att : attendants) {
			if (att.isCheckin()) {
				counter++;
			}
		}
		return counter;
	}
	
	private int getNumAccepted(List<ExtendedAttendant> attendants) {
		int counter = 0;
		for (Attendant att : attendants) {
			if (att.isAccepted()) {
				counter++;
			}
		}
		return counter;
	}

	@Override
	public void processLogged(HttpServletRequest request, HttpServletResponse response, ServletContext servletContext,
			ITemplateEngine templateEngine, WebContext ctx) throws Exception {
		if (request.getParameter("event_id") != null) {
			final int eventId = Integer.parseInt(request.getParameter("event_id"));
			
			final EventAPI eventAPI = new EventAPI();
			final Event event = eventAPI.getEvent(eventId, eventAPI.getDefaultLang());
			
			final ProfileManager profileManager = new ProfileManager();
			
			final List<ExtendedAttendant> attendants = profileManager.getExtendedAttendants("Bearer " + token, event.getId());
			final SurveyManager surveyManager = new SurveyManager();
			final NumResponses numResponses = surveyManager.getNumResponses(token, event.getSurveyId(), event.getId());
			
			ctx.setVariable("event", event);
			ctx.setVariable("attendants", attendants);
			ctx.setVariable("numattendants", attendants.size());
			ctx.setVariable("numresponses", numResponses.getNum());
			ctx.setVariable("numchecked", getNumChecked(attendants));
			ctx.setVariable("numaccepted", getNumAccepted(attendants));
			templateEngine.process("listAttendants", ctx, response.getWriter());
		} else {
			response.sendRedirect("listEvents");
		}
	}

}
