/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.web.controller.events;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.WebContext;

import eu.welive.bb.events.api.EventAPI;
import eu.welive.bb.events.serialization.Event.Language;
import eu.welive.bb.web.controller.CommonController;

public class DeleteEventController extends CommonController {

	@Override
	public void processLogged(HttpServletRequest request, HttpServletResponse response, ServletContext servletContext,
			ITemplateEngine templateEngine, WebContext ctx) throws Exception {
		
		final EventAPI eventAPI = new EventAPI();
		if (request.getParameter("deletedEventId") != null && request.getParameter("lang") != null) {
			final int eventId = Integer.parseInt(request.getParameter("deletedEventId"));
			final String lang = request.getParameter("lang");
			
			if (lang.equals(eventAPI.getDefaultLang().toString())) {
				eventAPI.deleteEvent(getBearerToken(), userId, eventId);
				response.sendRedirect("listEvents");
			} else {
				eventAPI.deleteTranslation(getBearerToken(), userId, eventId, Language.valueOf(lang));
				response.sendRedirect("editEvent?event_id=" + eventId);
			}
		}
		
		templateEngine.process("listEvents", ctx, response.getWriter());
	}
}
