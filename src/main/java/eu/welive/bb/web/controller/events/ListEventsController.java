/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.web.controller.events;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.WebContext;

import eu.welive.bb.events.api.EventAPI;
import eu.welive.bb.events.exception.EventException;
import eu.welive.bb.events.serialization.Attendant;
import eu.welive.bb.events.serialization.AttendantList;
import eu.welive.bb.events.serialization.Event;
import eu.welive.bb.events.serialization.Event.Language;
import eu.welive.bb.web.controller.CommonController;
import eu.welive.bb.web.survey.NumResponses;
import eu.welive.bb.web.survey.SurveyManager;

public class ListEventsController extends CommonController {

	@Override
	public void processLogged(HttpServletRequest request, HttpServletResponse response, ServletContext servletContext,
			ITemplateEngine templateEngine, WebContext ctx) throws Exception {
		
		String fromDate = request.getParameter("fromDate");
		if (fromDate == null) {
			final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
			final Date now = new Date();
			final String today = dateFormatter.format(now);
			fromDate = today;
		}
		
		final String toDate = request.getParameter("toDate");
		
		final EventAPI eventAPI = new EventAPI();
		final List<Event> events = eventAPI.getEventList(prepareFromDate(fromDate), prepareToDate(toDate), "", "", "", "", Language.ES).getEvents();
		final List<AttendantsInfo> attendantsInfo = getAttendantsInfo(eventAPI, events);
		final List<NumResponses> responsesInfo = getNumResponses(eventAPI, events);
		
		ctx.setVariable("events", events);
		ctx.setVariable("attendants", attendantsInfo);
		ctx.setVariable("responses", responsesInfo);
		ctx.setVariable("fromDate", fromDate);
		ctx.setVariable("toDate", toDate);
		templateEngine.process("listEvents", ctx, response.getWriter());
	}
	
	private String prepareFromDate(String fromDate) {
		if (fromDate == null || (fromDate != null && fromDate.length() == 0)) {
			return "";
		} else {
			return fromDate + "T00:00:00";
		}
	}
	
	private String prepareToDate(String toDate) {
		if (toDate == null || (toDate != null && toDate.length() == 0)) {
			return "";
		} else {
			return toDate + "T23:59:59";
		}
	}
	
	public List<NumResponses> getNumResponses(EventAPI eventAPI, List<Event> events) throws EventException {
		final List<NumResponses> responsesInfo = new ArrayList<NumResponses>();
		
		final SurveyManager surveyManager = new SurveyManager();
		for (Event event : events) {
			responsesInfo.add(surveyManager.getNumResponses(token, event.getSurveyId(), event.getId()));
		}
		
		return responsesInfo;
	}
	
	public List<AttendantsInfo> getAttendantsInfo(EventAPI eventAPI, List<Event> events) throws EventException {
		final List<AttendantsInfo> attendantsInfo = new ArrayList<AttendantsInfo>();
		
		for (Event event : events) {
			int checked = 0;
			int accepted = 0;
			
			final AttendantList attendantList = eventAPI.getAttendantList("Bearer " + token, event.getId());
			for (Attendant att : attendantList.getAttendants()) {
				if (att.isCheckin()) {
					checked += 1;
				}
				if (att.isAccepted()) {
					accepted += 1;
				}
			}
			
			final AttendantsInfo info = new AttendantsInfo();
			info.setNum(attendantList.getAttendants().size());
			info.setChecked(checked);
			info.setAccepted(accepted);
			attendantsInfo.add(info);
		}
		
		return attendantsInfo;
	}
}
