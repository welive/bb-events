/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.web.controller.events;

import java.time.DayOfWeek;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.WebContext;

import eu.welive.bb.events.api.EventAPI;
import eu.welive.bb.events.serialization.Event;
import eu.welive.bb.events.serialization.Event.Language;
import eu.welive.bb.events.serialization.LanguageList;
import eu.welive.bb.web.controller.CommonController;
import eu.welive.bb.web.controller.general.SupportedLanguages;
import eu.welive.bb.web.survey.Survey;
import eu.welive.bb.web.survey.SurveyManager;

public class EditEventController extends CommonController {

	@Override
	public void processLogged(HttpServletRequest request, HttpServletResponse response, ServletContext servletContext,
			ITemplateEngine templateEngine, WebContext ctx) throws Exception {
		
		final EventAPI eventAPI = new EventAPI();
		
		if (request.getParameter("event_id") != null) {
			final int eventId = Integer.parseInt(request.getParameter("event_id"));
			
			Language lang = eventAPI.getDefaultLang(); 
			if (request.getParameter("lang") != null) { 
				lang = Language.valueOf(request.getParameter("lang"));
			}
			
			final LanguageList languageList = eventAPI.getTranslations(eventId);
			boolean editing = true; 
			Event event = eventAPI.getEvent(eventId, lang);
			if (!languageList.getLanguages().contains(lang)) {
				event.setTitle("");
				event.setDescription("");
				event.setLocation("");
				event.setDescription("");
				event.setLang(lang);
				
				editing = false;
			}
			
			final boolean defaultLang = lang.equals(eventAPI.getDefaultLang());
			
			final SurveyManager surveyManager = new SurveyManager();
			final List<Survey> surveys = surveyManager.geSurveys(token); 
			
			ctx.setVariable("event", event);
			ctx.setVariable("langs", SupportedLanguages.getSupportedLanguages().getLanguages());
			ctx.setVariable("editing", editing);
			ctx.setVariable("surveys", surveys);
			ctx.setVariable("defaultlang", defaultLang);
			ctx.setVariable("weekDaysMap", getWeeKDaysMap());
			
			templateEngine.process("editEvent", ctx, response.getWriter());
		} else {
			response.sendRedirect("listEvents");
		}
	} 
	
	private Map<String, String> getWeeKDaysMap() {
		final Map<String, String> weekDaysMap = new LinkedHashMap<String, String>();
		
		weekDaysMap.put(DayOfWeek.SUNDAY.toString(), "Domingo");
		weekDaysMap.put(DayOfWeek.MONDAY.toString(), "Lunes");
		weekDaysMap.put(DayOfWeek.TUESDAY.toString(), "Martes");
		weekDaysMap.put(DayOfWeek.WEDNESDAY.toString(), "Miércoles");
		weekDaysMap.put(DayOfWeek.THURSDAY.toString(), "Jueves");
		weekDaysMap.put(DayOfWeek.FRIDAY.toString(), "Viernes");
		weekDaysMap.put(DayOfWeek.SATURDAY.toString(), "Sábado");
		
		return weekDaysMap; 
	}
}
