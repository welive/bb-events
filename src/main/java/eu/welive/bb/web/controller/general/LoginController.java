/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.web.controller.general;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.thymeleaf.ITemplateEngine;

import eu.welive.bb.events.config.Config;
import eu.welive.bb.web.controller.IController;

public class LoginController implements IController {

	@Override
	public void process(HttpServletRequest request, HttpServletResponse response, ServletContext servletContext,
			ITemplateEngine templateEngine) throws Exception {
		
		final String loginURLTemplate = "%s/eauth/authorize?client_id=%s&response_type=code&redirect_uri=%s&scope=profile.basicprofile.me,profile.accountprofile.me,cdv.profile.me,cdv.customdata.read";
		final String loginURL = String.format(loginURLTemplate, Config.getInstance().getAacServiceURL(), Config.getInstance().getClientId(), Config.getInstance().getHomeURL());
		
		response.sendRedirect(loginURL);
	}

}
