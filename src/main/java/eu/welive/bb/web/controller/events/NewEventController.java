/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.web.controller.events;

import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.thymeleaf.ITemplateEngine;
import org.thymeleaf.context.WebContext;

import eu.welive.bb.events.serialization.Event;
import eu.welive.bb.events.serialization.Event.Language;
import eu.welive.bb.web.controller.CommonController;
import eu.welive.bb.web.survey.Survey;
import eu.welive.bb.web.survey.SurveyManager;

public class NewEventController extends CommonController {

	@Override
	public void processLogged(HttpServletRequest request, HttpServletResponse response, ServletContext servletContext,
			ITemplateEngine templateEngine, WebContext ctx) throws Exception {

		final SurveyManager surveyManager = new SurveyManager();
		final List<Survey> surveys = surveyManager.geSurveys(token);
		
		ctx.setVariable("event", new Event());
		ctx.setVariable("langs", Arrays.asList(new Language[] { Language.ES } ));
		ctx.setVariable("editing", false);
		ctx.setVariable("surveys", surveys);
		ctx.setVariable("defaultlang", true);
		
		templateEngine.process("editEvent", ctx, response.getWriter());
		
	}
}
