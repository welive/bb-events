/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.web.controller.users;

public class UserProfile {

	private String id;
	private String contactPerson;
	private String email;
	private String phone;
	private String category;
	private String categoryId;
	private String webOrRss;
	private String activitySector;
	private String companyActivity;
	private String corporateName;
	private String commercialName;
	
	public UserProfile() {
		this.contactPerson = "";
		this.email = "";
		this.phone = "";
		this.category = "";
		this.categoryId = "";
		this.webOrRss = "";
		this.activitySector = "";
		this.companyActivity = "";
		this.corporateName = "";
		this.commercialName = "";
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getWebOrRss() {
		return webOrRss;
	}

	public void setWebOrRss(String webOrRss) {
		this.webOrRss = webOrRss;
	}

	public String getActivitySector() {
		return activitySector;
	}

	public void setActivitySector(String activitySector) {
		this.activitySector = activitySector;
	}

	public String getCompanyActivity() {
		return companyActivity;
	}

	public void setCompanyActivity(String companyActivity) {
		this.companyActivity = companyActivity;
	}

	public String getCorporateName() {
		return corporateName;
	}

	public void setCorporateName(String corporateName) {
		this.corporateName = corporateName;
	}

	public String getCommercialName() {
		return commercialName;
	}

	public void setCommercialName(String commercialName) {
		this.commercialName = commercialName;
	}
}
