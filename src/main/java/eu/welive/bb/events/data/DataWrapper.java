/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.events.data;

import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.List;

import javax.json.JsonArray;
import javax.json.JsonObject;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;

import eu.welive.bb.events.data.qm.QMConnector;
import eu.welive.bb.events.data.qm.QMConnectorException;
import eu.welive.bb.events.serialization.Attendant;
import eu.welive.bb.events.serialization.BasicEvent;
import eu.welive.bb.events.serialization.Event;
import eu.welive.bb.events.serialization.Event.Language;
import eu.welive.bb.events.serialization.EventTranslation;
import eu.welive.bb.events.serialization.TimePeriod;
import eu.welive.bb.events.serialization.TimePoint;

public class DataWrapper {
	
	final static Logger logger = Logger.getLogger(DataWrapper.class);
	
	private final QMConnector qmConnector;
	
	private final String dataset;
	private final String resource;
	
	public DataWrapper(String dataset, String resource, QMConnector qmConnector) {
		this.dataset = dataset;
		this.resource = resource;
		this.qmConnector = qmConnector;
	}
	
	public QMConnector getQMConnector() {
		return qmConnector;
	}
	
	public List<Event> filterEvents(String rangeStart, String rangeEnd,
			String north, String south, String east, String west,
			Language lang) throws DataWrapperException {
		logger.debug(String.format("Filtering events by start: %s and end: %s with lang: %s", rangeStart, rangeEnd, lang));
		try {
			String queryTemplate = "SELECT Event.id, Event.start, Event.end, Event.latitude, Event.longitude, Event.surveyId, "
					+ "Event.material, Event.maxattendants, "
					+ "COALESCE(t1.title, t2.title) as title, "
					+ "COALESCE(t1.description, t2.description) as description, "
					+ "COALESCE(t1.location, t2.location) as location, "
					+ "COALESCE(t1.contents, t2.contents) as contents, "
					+ "COALESCE(t1.lang, t2.lang) as lang "
					+ "FROM Event LEFT JOIN EventTranslation t1 ON (Event.id = t1.event_id AND t1.lang = '%s') "
					+ "LEFT JOIN EventTranslation t2 ON (Event.id = t2.event_id AND t2.lang = 'ES')";
			
			String query = String.format(queryTemplate, lang);
			
			final List<String> clauses = new ArrayList<String>();
			
			if (!rangeStart.isEmpty()) {
				clauses.add(String.format("Event.start >= '%s'", rangeStart));
			} 
			
			if (!rangeEnd.isEmpty()) {
				clauses.add(String.format("Event.start <= '%s'", rangeEnd));
			}
			
			if (!north.isEmpty() && !south.isEmpty() && !east.isEmpty() && !west.isEmpty()) {
				final String template = "CAST(Event.latitude AS FLOAT) <= %s AND "
										+ "CAST(Event.latitude AS FLOAT) >= %s AND "
										+ "CAST(Event.longitude AS FLOAT) <= %s AND "
										+ "CAST(Event.longitude AS FLOAT) >= %s";
				clauses.add(String.format(template, north, south, east, west));
			}
			
			if (!clauses.isEmpty()) {
				query += " WHERE ";
				for (int i = 0; i < clauses.size(); i++) {
					if (i > 0) {
						query += " AND ";
					}
					
					query += clauses.get(i);
				}
			}
			
			query += " ORDER BY Event.start DESC";
			
			logger.debug("Executing query:");
			logger.debug(query);
			
			final JsonObject result = qmConnector.queryDataset(query, dataset, resource);			
			final List<Event> events = processEventList(result);
			return events;			
		} catch (QMConnectorException e) {
			throw new DataWrapperException(e.getMessage());
		}
	}
	
	public List<Event> processEventList(JsonObject data) throws DataWrapperException {
		final List<Event> events = new ArrayList<>();
		
		final JsonArray values = data.getJsonArray("rows");
		for (int i = 0; i < values.size(); i++) {
			final JsonObject json = values.getJsonObject(i);
			final Event event = processEvent(json);
			events.add(event);
		}
		
		return events;
	}

	private Event processEvent(JsonObject json) throws DataWrapperException {
		final Event event = new Event();
		event.setId(json.getInt("id"));
		event.setTitle(json.getString("title"));
		event.setDescription(json.getString("description"));
		event.setMaxAttendants(json.getInt("maxattendants"));
		event.setMaterial(json.getString("material"));
		event.setLocation(json.getString("location"));
		event.setContents(json.getString("contents"));
		event.setStart(json.getString("start"));
		event.setEnd(json.getString("end"));
		event.setLatitude(json.getString("latitude"));
		event.setLongitude(json.getString("longitude"));
		event.setSurveyId(json.getInt("surveyId"));
		event.setLang(Language.valueOf(json.getString("lang")));
		
		final List<TimePeriod> timePeriods = getTimePeriods(event.getId());
		event.setSchedule(timePeriods);
		
		return event;
	}
	
	public List<TimePeriod> getTimePeriods(int eventId) throws DataWrapperException {
		logger.debug("Obtaining time periods with event_id: " + eventId);
		
		final String template = "SELECT id, startDay, startDate, startTime, endDay, endDate, endTime FROM TimePeriod WHERE event_id=%d";
		final String query = String.format(template, eventId);
		
		logger.debug("Executing query:");
		logger.debug(query);
		
		try {
			final JsonObject result = qmConnector.queryDataset(query, dataset, resource);			
			final List<TimePeriod> timePeriods = processTimePeriodList(result);
			return timePeriods;		
		} catch (QMConnectorException e) {
			throw new DataWrapperException(e.getMessage());
		}
	}
	
	protected List<TimePeriod> processTimePeriodList(JsonObject data) {
		final List<TimePeriod> timePeriods = new ArrayList<>();
		
		final JsonArray values = data.getJsonArray("rows");
		for (int i = 0; i < values.size(); i++) {
			final JsonObject json = values.getJsonObject(i);
			final TimePeriod timePeriod = processTimePeriod(json);
			timePeriods.add(timePeriod);
		}
		
		return timePeriods;
	}
	
	protected TimePeriod processTimePeriod(JsonObject json) {
		final TimePeriod timePeriod = new TimePeriod();
		timePeriod.setId(json.getInt("id"));
		
		final TimePoint start = new TimePoint();
		start.setDay(DayOfWeek.valueOf(json.getString("startDay").toUpperCase()));
		start.setDate(json.getString("startDate"));
		start.setTime(json.getString("startTime"));
		timePeriod.setStart(start);
		
		final TimePoint end = new TimePoint();
		end.setDay(DayOfWeek.valueOf(json.getString("endDay").toUpperCase()));
		end.setDate(json.getString("endDate"));
		end.setTime(json.getString("endTime"));
		timePeriod.setEnd(end);
		
		return timePeriod;
	}

	public int saveEvent(Event event) throws DataWrapperException {
		logger.debug("Saving event: " + event);
		
		final List<String> inserts = new ArrayList<String>();
		
		final String insertEventTemplate = "INSERT INTO Event (start, end, maxattendants, material, latitude, longitude, surveyId) VALUES ('%s', '%s', %d, '%s', '%s', '%s', %d)";
		final String insertEvent = String.format(insertEventTemplate, 
			StringEscapeUtils.escapeSql(event.getStart()),
			StringEscapeUtils.escapeSql(event.getEnd()), 
			event.getMaxAttendants(),
			StringEscapeUtils.escapeSql(event.getMaterial()),
			StringEscapeUtils.escapeSql(event.getLatitude()), 
			StringEscapeUtils.escapeSql(event.getLongitude()), 
			event.getSurveyId());
		
		inserts.add(insertEvent);
		
		final String insertTimePeriodTemplate = "INSERT INTO TimePeriod (id, startDay, startDate, startTime, endDay, endDate, endTime, event_id) SELECT null, '%s', '%s', '%s', '%s', '%s', '%s', MAX(id) FROM Event";
		for (final TimePeriod timePeriod : event.getSchedule()) {
			final String insert = String.format(insertTimePeriodTemplate,
				StringEscapeUtils.escapeSql(timePeriod.getStart().getDay().toString()),
				StringEscapeUtils.escapeSql(timePeriod.getStart().getDate()),
				StringEscapeUtils.escapeSql(timePeriod.getStart().getTime()),
				StringEscapeUtils.escapeSql(timePeriod.getEnd().getDay().toString()),
				StringEscapeUtils.escapeSql(timePeriod.getEnd().getDate()),
				StringEscapeUtils.escapeSql(timePeriod.getEnd().getTime()));
			
			inserts.add(insert);
		}
		
		final String insertTranslationTemplate = "INSERT INTO EventTranslation (id, title, description, location, contents, lang, event_id) SELECT null, '%s', '%s', '%s', '%s', '%s', MAX(id) FROM Event";
		final String insertTranslation = String.format(insertTranslationTemplate, 
			StringEscapeUtils.escapeSql(event.getTitle()), 
			StringEscapeUtils.escapeSql(event.getDescription()), 
			StringEscapeUtils.escapeSql(event.getLocation()), 
			StringEscapeUtils.escapeSql(event.getContents()), 
			event.getLang());
		
		inserts.add(insertTranslation);
		
		logger.debug("Executing inserts: ");
		logger.debug(inserts);
		
		try {
			final int updatedRows = qmConnector.updateDatasetTransactional(inserts, dataset, resource);
			return updatedRows;
		} catch (QMConnectorException e) {
			throw new DataWrapperException(e.getMessage());
		}
	}
	
	public int saveTimePeriods(List<TimePeriod> timePeriods, int eventId) throws DataWrapperException {
		final List<String> inserts = new ArrayList<String>();
		
		final String insertTimePeriodTemplate = "INSERT INTO TimePeriod (id, startDay, startDate, startTime, endDay, endDate, endTime, event_id) VALUES (null, '%s', '%s', '%s', '%s', '%s', '%s', %d)";
		for (final TimePeriod timePeriod : timePeriods) {
			final String insert = String.format(insertTimePeriodTemplate,
				StringEscapeUtils.escapeSql(timePeriod.getStart().getDay().toString()),
				StringEscapeUtils.escapeSql(timePeriod.getStart().getDate()),
				StringEscapeUtils.escapeSql(timePeriod.getStart().getTime()),
				StringEscapeUtils.escapeSql(timePeriod.getEnd().getDay().toString()),
				StringEscapeUtils.escapeSql(timePeriod.getEnd().getDate()),
				StringEscapeUtils.escapeSql(timePeriod.getEnd().getTime()),
				eventId);
			
			inserts.add(insert);
		}
		
		logger.debug("Executing inserts: ");
		logger.debug(inserts);
		
		try {
			final int updatedRows = qmConnector.updateDatasetTransactional(inserts, dataset, resource);
			return updatedRows;
		} catch (QMConnectorException e) {
			throw new DataWrapperException(e.getMessage());
		}
	}
	
	public int deleteTimePeriods(List<TimePeriod> timePeriods, int eventId) throws DataWrapperException {
		final List<String> deletes = new ArrayList<String>();
		
		final String deleteTimePeriodTemplate = "DELETE FROM TimePeriod WHERE id=%d AND event_id=%d";
		for (final TimePeriod timePeriod : timePeriods) {
			final String delete = String.format(deleteTimePeriodTemplate, timePeriod.getId(), eventId);
			deletes.add(delete);
		}
		
		logger.debug("Executing deletes: ");
		logger.debug(deletes);
		
		try {
			final int updatedRows = qmConnector.updateDatasetTransactional(deletes, dataset, resource);
			return updatedRows;
		} catch (QMConnectorException e) {
			throw new DataWrapperException(e.getMessage());
		}
	}
	
	public int saveTranslation(EventTranslation eventTranslation, Language lang) throws DataWrapperException {
		logger.debug("Saving translation: " + eventTranslation);
		
		final List<Language> translations = getAvailableTranslations(eventTranslation.getEventId());
		if (translations.contains(lang)) {
			throw new DataWrapperException(String.format("Translation for lang '%s' already exists", lang));
		}
		
		final String insertTranslationTemplate = "INSERT INTO EventTranslation (title, description, location, contents, lang, event_id) VALUES ('%s', '%s', '%s', '%s', '%s', %d)";
		final String insert = String.format(insertTranslationTemplate, 
			StringEscapeUtils.escapeSql(eventTranslation.getTitle()), 
			StringEscapeUtils.escapeSql(eventTranslation.getDescription()), 
			StringEscapeUtils.escapeSql(eventTranslation.getLocation()), 
			StringEscapeUtils.escapeSql(eventTranslation.getContents()), 
			lang, 
			eventTranslation.getEventId());
		
		logger.debug("Executing update: ");
		logger.debug(insert);
		
		try {
			final int updatedRows = qmConnector.updateDataset(insert, dataset, resource);
			return updatedRows;
		} catch (QMConnectorException e) {
			throw new DataWrapperException(e.getMessage());
		}
	}
	
	public int updateEvent(BasicEvent event) throws DataWrapperException {
		final List<String> updates = new ArrayList<String>();
		
		logger.debug("Updating event: " + event);
		
		final String updateEventTemplate = "UPDATE Event SET start='%s', end='%s', maxattendants=%d, material='%s', latitude='%s', longitude='%s', surveyId=%d WHERE id=%d";
		final String updateEvent = String.format(updateEventTemplate, 
			StringEscapeUtils.escapeSql(event.getStart()),
			StringEscapeUtils.escapeSql(event.getEnd()),
			event.getMaxAttendants(),
			StringEscapeUtils.escapeSql(event.getMaterial()),
			StringEscapeUtils.escapeSql(event.getLatitude()),
			StringEscapeUtils.escapeSql(event.getLongitude()),
			event.getSurveyId(),
			event.getId());
		
		updates.add(updateEvent);
		
		final String updateTimePeriodTemplate = "UPDATE TimePeriod SET startDay='%s', startDate='%s', startTime='%s', endDay='%s', endDate='%s', endTime='%s' WHERE id=%d AND event_id=%d";
		final String insertTimePeriodTemplate = "INSERT INTO TimePeriod (id, startDay, startDate, startTime, endDay, endDate, endTime, event_id) SELECT null, '%s', '%s', '%s', '%s', '%s', '%s', MAX(id) FROM Event";
		
		final List<Integer> currentTimePeriods = new ArrayList<Integer>();
		
		for (final TimePeriod timePeriod : event.getSchedule()) {
			if (timePeriod.getId() != 0) {
				final String update = String.format(updateTimePeriodTemplate,
					StringEscapeUtils.escapeSql(timePeriod.getStart().getDay().toString()),
					StringEscapeUtils.escapeSql(timePeriod.getStart().getDate()),
					StringEscapeUtils.escapeSql(timePeriod.getStart().getTime()),
					StringEscapeUtils.escapeSql(timePeriod.getEnd().getDay().toString()),
					StringEscapeUtils.escapeSql(timePeriod.getEnd().getDate()),
					StringEscapeUtils.escapeSql(timePeriod.getEnd().getTime()),
					timePeriod.getId(),
					event.getId());
			
				updates.add(update);
				
				currentTimePeriods.add(timePeriod.getId());
			} else {
				final String insert = String.format(insertTimePeriodTemplate,
					StringEscapeUtils.escapeSql(timePeriod.getStart().getDay().toString()),
					StringEscapeUtils.escapeSql(timePeriod.getStart().getDate()),
					StringEscapeUtils.escapeSql(timePeriod.getStart().getTime()),
					StringEscapeUtils.escapeSql(timePeriod.getEnd().getDay().toString()),
					StringEscapeUtils.escapeSql(timePeriod.getEnd().getDate()),
					StringEscapeUtils.escapeSql(timePeriod.getEnd().getTime()));
				
				updates.add(insert);
			}
		}
		
		final List<TimePeriod> schedule = getTimePeriods(event.getId());
		
		final String deleteTimePeriodTemplate = "DELETE FROM TimePeriod WHERE event_id=%d AND id=%d";
		for (final TimePeriod timePeriod : schedule) {
			if (!currentTimePeriods.contains(timePeriod.getId())) {
				final String delete = String.format(deleteTimePeriodTemplate, event.getId(), timePeriod.getId());
				updates.add(delete);
			}
		}
		
		logger.debug("Executing updates: ");
		logger.debug(updates);
		
		try {
			final int updatedRows = qmConnector.updateDatasetTransactional(updates, dataset, resource);
			return updatedRows;
		} catch (QMConnectorException e) {
			throw new DataWrapperException(e.getMessage());
		}
	} 
	
	public int updateTranslation(EventTranslation eventTranslation, Language lang) throws DataWrapperException {
		logger.debug("Updating event translation: " + eventTranslation);
		
		final String updateEventTranslationTemplate = "UPDATE EventTranslation SET title='%s', description='%s', location='%s', contents='%s' WHERE lang='%s' AND event_id=%d";
		final String updateEventTranslation = String.format(updateEventTranslationTemplate, 
			StringEscapeUtils.escapeSql(eventTranslation.getTitle()), 
			StringEscapeUtils.escapeSql(eventTranslation.getDescription()), 
			StringEscapeUtils.escapeSql(eventTranslation.getLocation()), 
			StringEscapeUtils.escapeSql(eventTranslation.getContents()), 
			lang, 
			eventTranslation.getEventId());
		
		logger.debug("Executing update: ");
		logger.debug(updateEventTranslation);
		
		try {
			final int updatedRows = qmConnector.updateDataset(updateEventTranslation, dataset, resource);
			return updatedRows;
		} catch (QMConnectorException e) {
			throw new DataWrapperException(e.getMessage());
		}
	}
	
	public int deleteTranslation(int eventId, Language lang) throws DataWrapperException {
		logger.debug("Deleting translation with eventId: " + eventId + " lang: " + lang);
		
		final String deleteTemplate = "DELETE FROM EventTranslation WHERE lang='%s' AND event_id=%d";
		final String delete = String.format(deleteTemplate, lang, eventId);
		
		logger.debug("Executing delete:");
		logger.debug(delete);
		
		try {
			final int deletedTranslations = qmConnector.updateDataset(delete, dataset, resource);
			if (deletedTranslations > 0) {
				final int numTranslations = countTranslations(eventId);
				if (numTranslations == 0) {
					final int deletedEvents = deleteEvent(eventId);
					return deletedTranslations + deletedEvents;
				}
			}
			return deletedTranslations;
		} catch (QMConnectorException e) {
			throw new DataWrapperException(e.getMessage());
		}
	}
	
	public int deleteEvent(int eventId) throws DataWrapperException {
		logger.debug("Deleting event with eventId: " + eventId);
		
		final List<String> deletes = new ArrayList<String>();
		
		final String deleteEventTemplate = "DELETE FROM Event WHERE id=%d";
		final String deleteEvent = String.format(deleteEventTemplate, eventId);
		deletes.add(deleteEvent);
		
		final String deleteTranslationsTemplate = "DELETE FROM EventTranslation WHERE event_id=%d";
		final String deleteTranslations = String.format(deleteTranslationsTemplate, eventId); 
		deletes.add(deleteTranslations);		
		
		final String deleteAttendantsTemplate = "DELETE FROM Attendant WHERE event_id=%d";
		final String deleteAttendants = String.format(deleteAttendantsTemplate, eventId); 
		deletes.add(deleteAttendants);
		
		final String deleteTimePeriodTemplate = "DELETE FROM TimePeriod WHERE event_id=%d";
		final String delete = String.format(deleteTimePeriodTemplate, eventId);
		deletes.add(delete);
		
		logger.debug("Executing delete:");
		logger.debug(deletes);

		try {
			final int updatedRows = qmConnector.updateDatasetTransactional(deletes, dataset, resource);
			return updatedRows;
		} catch (QMConnectorException e) {
			throw new DataWrapperException(e.getMessage());
		}
	}
	
	public int countTranslations(int eventId) throws DataWrapperException {
		logger.debug("Counting translations for eventId: " + eventId);
		
		final String queryTemplate = "SELECT COUNT(id) as num FROM EventTranslation WHERE event_id=%d";
		final String query = String.format(queryTemplate, eventId);
		
		logger.debug("Executing query:");
		logger.debug(query);
		
		try {
			final JsonObject result = qmConnector.queryDataset(query, dataset, resource);
			final int num = result.getJsonArray("rows").getJsonObject(0).getInt("num");
			return num;
		} catch (QMConnectorException e) {
			throw new DataWrapperException(e.getMessage());
		}
	}
	
	public Event getEvent(int id, Language lang) throws DataWrapperException {
		logger.debug("Obtaining event with id: " + id);
		
		final String queryTemplate = "SELECT Event.id, Event.start, Event.end, Event.material, Event.latitude, Event.longitude, Event.surveyId, "
				+ "Event.maxattendants, "
				+ "COALESCE(t1.title, t2.title) as title, "
				+ "COALESCE(t1.description, t2.description) as description, "
				+ "COALESCE(t1.location, t2.location) as location, "
				+ "COALESCE(t1.contents, t2.contents) as contents, "
				+ "COALESCE(t1.lang, t2.lang) as lang "
				+ "FROM Event LEFT JOIN EventTranslation t1 ON (Event.id = t1.event_id AND t1.lang = '%s') "
				+ "LEFT JOIN EventTranslation t2 ON (Event.id = t2.event_id AND t2.lang = 'ES') "
				+ "WHERE Event.id=%d";
		
		final String query = String.format(queryTemplate, lang, id); 
		
		logger.debug("Executing query:");
		logger.debug(query);
		
		try {
			final JsonObject result = qmConnector.queryDataset(query, dataset, resource);
			if (result.getInt("count") > 0) {
				final Event event = processEvent(result.getJsonArray("rows").getJsonObject(0));
				return event;
			} else {
				return new Event();
			}
		} catch (QMConnectorException e) {
			throw new DataWrapperException(e.getMessage());
		}
	}
	
	public List<Attendant> getAttendants(int eventId) throws DataWrapperException {
		logger.debug("Obtaining attendant list for event: " + eventId);
		
		final String queryTemplate = "SELECT att.user_id, att.event_id, att.checkin, att.accepted, att.timestamp "
				+ "FROM Attendant att "
				+ "WHERE event_id=%d";
		
		final String query = String.format(queryTemplate, eventId);
		
		logger.debug("Executing query:");
		logger.debug(query);
		
		try {
			final JsonObject result = qmConnector.queryDataset(query, dataset, resource);
			final List<Attendant> attendants = processAttendantList(result);
			return attendants;
		} catch (QMConnectorException e) {
			throw new DataWrapperException(e.getMessage());
		}
	}
	
	public List<Attendant> processAttendantList(JsonObject data) {
		final List<Attendant> attendants = new ArrayList<>();
		
		final JsonArray values = data.getJsonArray("rows");
		for (int i = 0; i < values.size(); i++) {
			final JsonObject json = values.getJsonObject(i);
			final Attendant attendant = processAttendant(json);
			attendants.add(attendant);
		}
		
		return attendants;
	}
	
	public Attendant processAttendant(JsonObject jsonObject) {
		final Attendant attendant = new Attendant();
		attendant.setUserId(jsonObject.getString("user_id")); 
		attendant.setEventId(jsonObject.getInt("event_id"));
		attendant.setCheckin(jsonObject.getInt("checkin") != 0);
		attendant.setAccepted(jsonObject.getInt("accepted") != 0);
		attendant.setTimestamp(jsonObject.getString("timestamp"));
		return attendant;
	}
	
	public int addAttendant(int eventId, String userId) throws DataWrapperException {
		logger.debug("Adding attendant with user_id: " + userId + " to event: " + eventId);
		
		final String insertTemplate = "INSERT INTO Attendant (user_id, checkin, accepted, timestamp, event_id) VALUES ('%s', %d, %d, '%s', %d)";
		final String insert = String.format(insertTemplate, userId, 0, 0, qmConnector.createTimestamp(), eventId);
		
		logger.debug("Executing insert: ");
		logger.debug(insert);
		
		try {
			final int updatedRows = qmConnector.updateDataset(insert, dataset, resource);
			return updatedRows;
		} catch (QMConnectorException e) {
			throw new DataWrapperException(e.getMessage());
		}
	}
	
	public int removeAttendant(int eventId, String userId) throws DataWrapperException {
		logger.debug("Removing attendant with user_id: " + userId + " from event: " + eventId);
		
		final String insertTemplate = "DELETE FROM Attendant WHERE user_id='%s' AND event_id=%d";
		final String insert = String.format(insertTemplate, userId, eventId);
		
		logger.debug("Executing insert: ");
		logger.debug(insert);
		
		try {
			final int updatedRows = qmConnector.updateDataset(insert, dataset, resource);
			return updatedRows;
		} catch (QMConnectorException e) {
			throw new DataWrapperException(e.getMessage());
		}
	}
	
	public boolean isAttendant(int eventId, String userId) throws DataWrapperException {
		logger.debug("Checking if user " + userId + " is attendant of event " + eventId);
		
		final String queryTemplate = "SELECT COUNT(*) as num FROM Attendant WHERE user_id='%s' AND event_id=%d";
		final String query = String.format(queryTemplate, userId, eventId);
		
		logger.debug("Executing query: ");
		logger.debug(query);
		
		try {
			final JsonObject response = qmConnector.queryDataset(query, dataset, resource);
			return response.getJsonArray("rows").getJsonObject(0).getInt("num") > 0;
		} catch (QMConnectorException e) {
			throw new DataWrapperException(e.getMessage());
		}
	}
	
	protected int setAttendantCheckinStatus(int eventId, String userId, boolean checkinStatus) throws DataWrapperException {
		logger.debug("Changing attendant with user_id: " + userId + " checking status to: " + checkinStatus);
		
		final String updateTemplate = "UPDATE Attendant SET checkin=%d, timestamp='%s' WHERE user_id='%s' AND event_id=%d";
		final String update = String.format(updateTemplate, checkinStatus?1:0, qmConnector.createTimestamp(), userId, eventId);
		
		logger.debug("Executing update: ");
		logger.debug(update);
		
		try {
			final int updatedRows = qmConnector.updateDataset(update, dataset, resource);
			return updatedRows;
		} catch (QMConnectorException e) {
			throw new DataWrapperException(e.getMessage());
		}
	}
	
	public int checkinAttendant(int eventId, String userId) throws DataWrapperException {
		return setAttendantCheckinStatus(eventId, userId, true);
	}
	
	public int checkoutAttendant(int eventId, String userId) throws DataWrapperException {
		return setAttendantCheckinStatus(eventId, userId, false);
	}
	
	protected int setAttendantAcceptedStatus(int eventId, String userId, boolean acceptedStatus) throws DataWrapperException {
		logger.debug("Changing attendant with user_id: " + userId + " accepted status to: " + acceptedStatus);
		
		final String updateTemplate = "UPDATE Attendant SET accepted=%d, timestamp='%s' WHERE user_id='%s' AND event_id=%d";
		final String update = String.format(updateTemplate, acceptedStatus?1:0, qmConnector.createTimestamp(), userId, eventId);
		
		logger.debug("Executing update: ");
		logger.debug(update);
		
		try {
			final int updatedRows = qmConnector.updateDataset(update, dataset, resource);
			return updatedRows;
		} catch (QMConnectorException e) {
			throw new DataWrapperException(e.getMessage());
		}
	}
	
	public int acceptAttendant(int eventId, String userId) throws DataWrapperException {
		return setAttendantAcceptedStatus(eventId, userId, true);
	}
	
	public int rejectAttendant(int eventId, String userId) throws DataWrapperException {
		return setAttendantAcceptedStatus(eventId, userId, false);
	}
	
	public List<Language> getAvailableTranslations(int eventId) throws DataWrapperException {
		logger.debug("Obtaining available translations for event: " + eventId);
		
		final String queryTemplate = "SELECT lang FROM EventTranslation WHERE event_id=%d";
		final String query = String.format(queryTemplate, eventId);
		
		logger.debug("Executing query: ");
		logger.debug(query);
		
		try {
			final JsonObject response = qmConnector.queryDataset(query, dataset, resource);
			return processLanguageList(response);
		} catch (QMConnectorException e) {
			throw new DataWrapperException(e.getMessage());
		}
	}
	
	public List<Language> processLanguageList(JsonObject data) {
		final List<Language> languages = new ArrayList<>();

		final JsonArray values = data.getJsonArray("rows");
		for (int i = 0; i < values.size(); i++) {
			final JsonObject json = values.getJsonObject(i);
			final String lang = json.getString("lang");
			languages.add(Language.valueOf(lang));
		}
		
		return languages;
	}
	
	public List<Attendant> getUserEvents(String userId) throws DataWrapperException { 
		logger.debug("Obtaining registered events for user " + userId );
		
		final String queryTemplate = "SELECT att.event_id, att.user_id, att.checkin, att.accepted, att.timestamp "
				+ "FROM Attendant att "
				+ "WHERE user_id='%s'";
		
		final String query = String.format(queryTemplate, StringEscapeUtils.escapeSql(userId));
		
		logger.debug("Executing query: ");
		logger.debug(query);
		
		try {
			final JsonObject response = qmConnector.queryDataset(query, dataset, resource);
			final List<Attendant> attendants = processAttendantList(response);
			return attendants;
		} catch (QMConnectorException e) {
			throw new DataWrapperException(e.getMessage());
		}
	}
}
