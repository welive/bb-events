/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.events.serialization;

public class Event extends BasicEvent {
	
	public enum Language { EN, ES, IT, SR, FI, EU };

	private String title;
	private String description;
	private String location;
	private String contents;
	private Language lang;
	
	public Event() {
		this.lang = Language.ES;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public String getLocation() {
		return location;
	}
	
	public String getContents() {
		return contents;
	}

	public Language getLang() {
		return lang;
	}

	public void setLang(Language lang) {
		this.lang = lang;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Event)) {
			return false;
		}
		
		final Event event = (Event)o;
		return super.equals(o) && 
			this.description == event.description && 
			this.location == event.location && 
			this.contents == event.contents;
	}

	@Override
	public String toString() {
		return "Event [id=" + getId() + ", title=" + title + ", description=" + description + ", material=" + getMaterial()
				+ ", start=" + getStart() + ", end=" + getEnd() + ", maxAttendants=" + getMaxAttendants() + ", location=" + location
				+ ", latitude=" + getLatitude() + ", longitude=" + getLongitude() + ", contents=" + contents + ", surveyId="
				+ getSurveyId() + ", lang=" + lang + ", schedule=" + getSchedule() + "]";
	}
}
