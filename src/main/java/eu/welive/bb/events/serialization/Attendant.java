/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.events.serialization;

public class Attendant {

	private int eventId;
	private String userid;
	private boolean checkedin;
	private boolean accepted;
	private String timestamp;
	
	public Attendant() {
		this.userid = "";
		this.checkedin = false;
		this.timestamp = "";
	}

	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	public String getUserId() {
		return userid;
	}

	public void setUserId(String userid) {
		this.userid = userid;
	}

	public boolean isCheckin() {
		return checkedin;
	}

	public void setCheckin(boolean checkin) { 
		this.checkedin = checkin;
	}

	public boolean isAccepted() {
		return accepted;
	}

	public void setAccepted(boolean accepted) {
		this.accepted = accepted;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String checkinDate) {
		this.timestamp = checkinDate;
	}
}
