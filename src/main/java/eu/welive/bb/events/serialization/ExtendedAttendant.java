/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.events.serialization;

public class ExtendedAttendant extends Attendant {

	private String contactPerson;
	private String email;
	private String phone;
	private String avatar;
	private String category;
	
	public ExtendedAttendant() {
		this.contactPerson = "";
		this.email = "";
		this.category = "";
	}
	
	public ExtendedAttendant(Attendant att) {
		setUserId(att.getUserId());
		setCheckin(att.isCheckin());
		setAccepted(att.isAccepted());
		setTimestamp(att.getTimestamp());
	}

	public String getContactPerson() {
		return contactPerson;
	}

	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getCategory() {
		return category;
	}
	
	public String getFullName() {
		return contactPerson;
	}

	public void setCategory(String category) {
		this.category = category;
	}
}
