/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.events.serialization;

import eu.welive.bb.events.serialization.Event.Language;

public class EventTranslation {
	
	private int eventId;
	private String title;
	private String description;
	private String location;
	private String contents;
	private Language lang;
	
	public EventTranslation() {
		this.eventId = 0;
		this.lang = Language.ES;
	}
	
	public EventTranslation(Event event) {
		this.eventId = event.getId();
		this.title = event.getTitle();
		this.description = event.getDescription();
		this.location = event.getLocation();
		this.contents = event.getContents();
		this.lang = event.getLang();
	}

	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public String getLocation() {
		return location;
	}
	
	public String getContents() {
		return contents;
	}

	public Language getLang() {
		return lang;
	}

	public void setLang(Language lang) {
		this.lang = lang;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof EventTranslation)) {
			return false;
		}
		
		final EventTranslation event = (EventTranslation)o;
		return this.eventId == event.eventId && 
			this.description == event.description && 
			this.location == event.location && 
			this.contents == event.contents &&
			this.lang == event.lang;
	}

	@Override
	public String toString() {
		return "Event [id=" + getEventId() + ", title=" + title + ", description=" + description
				+ ", location=" + location + ", contents=" + contents + ", surveyId="
				+ ", lang=" + lang + "]";
	}
}
