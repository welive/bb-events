/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.events.serialization;

import java.util.ArrayList;
import java.util.List;

public class BasicEvent {
	
	private int id;
	private String material;
	private String start;
	private String end;
	private int maxAttendants;
	private String latitude;
	private String longitude;
	private int surveyId;
	
	private List<TimePeriod> schedule;
	
	public BasicEvent() {
		this.id = 0;
		this.start = "";
		this.end = "";
		this.schedule = new ArrayList<TimePeriod>();
		this.latitude = "";
		this.longitude = "";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getMaxAttendants() {
		return maxAttendants;
	}

	public void setMaxAttendants(int maxAttendants) {
		this.maxAttendants = maxAttendants;
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public String getStart() {
		return start;
	}

	public String getEnd() {
		return end;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public void setEnd(String end) {
		this.end = end;
	}
	
	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public int getSurveyId() {
		return surveyId;
	}

	public void setSurveyId(int surveyId) {
		this.surveyId = surveyId;
	}

	public List<TimePeriod> getSchedule() {
		return schedule;
	}

	public void setSchedule(List<TimePeriod> schedule) {
		this.schedule = schedule;
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof BasicEvent)) {
			return false;
		}
		
		final BasicEvent event = (BasicEvent)o;
		return this.id == event.id &&
			this.latitude == event.latitude &&
			this.longitude == event.longitude && 
			this.start == event.start && 
			this.end == event.end;
	}

	@Override
	public String toString() {
		return "Event [id=" + id + ", material=" + material + ", start=" + start + ", end=" + end + ", maxAttendants=" + maxAttendants +
			", latitude=" + latitude + ", longitude=" + longitude + ", surveyId="
			+ surveyId + ", schedule=" + schedule + "]";
	}
}
