/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.events.exception;

import javax.ws.rs.core.Response.Status;

import eu.welive.bb.events.data.DataWrapperException;

@SuppressWarnings("serial")
public class EventException extends Exception {

	private final int status; 
	private final String message;
	
	public EventException(Status status, String message) {
		this.status = status.getStatusCode();
		this.message = message;
	}
	
	public EventException(int status, String message) {
		super(message);
		this.status = status;
		this.message = message;
	}
	
	public EventException(DataWrapperException e) {
		super(e.getMessage() + " " + e.getReason());
		this.status = e.getStatus();
		this.message = e.getMessage() + " " + e.getReason();
	} 
	
	public int getStatus() {
		return status;
	}
	
	public String getMessage() {
		return message;
	}
}