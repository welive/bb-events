/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.events.profile;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.json.JSONObject;

import eu.welive.bb.events.api.EventAPI;
import eu.welive.bb.events.config.Config;
import eu.welive.bb.events.exception.EventException;
import eu.welive.bb.events.exception.UnexpectedErrorException;
import eu.welive.bb.events.serialization.Attendant;
import eu.welive.bb.events.serialization.AttendantList;
import eu.welive.bb.events.serialization.ExtendedAttendant;
import eu.welive.bb.web.controller.users.UserProfile;

public class ProfileManager {
	
	private String getValue(JSONArray cdvData, String dataId) {	
		for (int i = 0; i < cdvData.length(); i++) {
			final JSONObject entry = cdvData.getJSONObject(i);
			if (Integer.toString(entry.getInt("dataId")).equals(dataId)) {
				return entry.getJSONObject("data").getString("value");
			}
		}
		
		return "";
	}
	
	public ExtendedAttendant getExtendedAttendant(Attendant att, JSONArray cdvData) throws UnexpectedErrorException {
		final Client client = ClientBuilder.newClient();
		final WebTarget target = client.target(Config.getInstance().getBigKlubBBURL());
		
		final Response response = target.path("user/profile")
			.queryParam("userID", att.getUserId())
			.request(MediaType.APPLICATION_JSON)
			.get();
		
		if (response.getStatus() == Status.OK.getStatusCode()) {
			final String data = response.readEntity(String.class);
			final JSONObject json = new JSONObject(data).getJSONObject("d");
			
			final ExtendedAttendant extendedAttendant = new ExtendedAttendant(att);
			extendedAttendant.setContactPerson(getValue(cdvData, json.getString("contactPerson")));
			extendedAttendant.setEmail(getValue(cdvData, json.getString("eMail")));
			extendedAttendant.setPhone(getValue(cdvData, json.getString("phone")));
			extendedAttendant.setCategory(json.getString("category"));
			return extendedAttendant;			
		} else {
			throw new UnexpectedErrorException("Could not retrieve information from BIG Kluba BB");
		}
	}
	
	public List<UserProfile> getAllUsers(String authHeader) throws UnexpectedErrorException {
		final List<UserProfile> users = new ArrayList<UserProfile>();
		
		final Client client = ClientBuilder.newClient();
		final WebTarget target = client.target(Config.getInstance().getBigKlubBBURL());
		
		final Response response = target.path("user/all")
			.request(MediaType.APPLICATION_JSON)
			.get();
		
		final JSONArray cdvData = getCDVData(authHeader);
		
		if (response.getStatus() == Status.OK.getStatusCode()) {
			final String data = response.readEntity(String.class);
			final JSONArray usersArray = new JSONObject(data).getJSONArray("d");
			for (int i = 0; i < usersArray.length(); i++) {
				final JSONObject json = usersArray.getJSONObject(i);
				
				final UserProfile userProfile = new UserProfile();
				userProfile.setCategory(json.getString("category"));
				
				userProfile.setContactPerson(getValue(cdvData, json.getString("contactPerson")));

				userProfile.setEmail(getValue(cdvData, json.getString("eMail")));
				userProfile.setPhone(getValue(cdvData, json.getString("phone")));
				userProfile.setCategoryId(getValue(cdvData, json.getString("categoryID")));
				if (!json.isNull("webOrRRSS")) {
					userProfile.setWebOrRss(getValue(cdvData, json.getString("webOrRRSS")));
				}
				userProfile.setActivitySector(getValue(cdvData, json.getString("activitySector")));
				
				if (userProfile.getCategory().equals("company") || userProfile.getCategory().equals("bigklub_company")) {
					userProfile.setCompanyActivity(getValue(cdvData, json.getString("companyActivity")));
					userProfile.setCorporateName(getValue(cdvData, json.getString("corporateName")));
					userProfile.setCommercialName(getValue(cdvData, json.getString("commercialName")));
				}
				
				users.add(userProfile);
			}
		}
		
		return users;
	}
	
	public JSONArray getCDVData(String authHeader) throws UnexpectedErrorException {
		final Client client = ClientBuilder.newClient();
		final WebTarget target = client.target(Config.getInstance().getWeLiveAPI());
		
		final Response response = target.path("cdv/userdata/get/all")
			.request(MediaType.APPLICATION_JSON)
			.header("Authorization", authHeader)
			.get();
		
		if (response.getStatus() == Status.OK.getStatusCode()) {
			final String data = response.readEntity(String.class);
			return new JSONArray(data); 
		} else {
			throw new UnexpectedErrorException("Could not retrieve information from CDV");
		}
	}
	
	public List<ExtendedAttendant> getExtendedAttendants(String authHeader, int eventId) throws EventException {
		final List<ExtendedAttendant> extendedAttendants = new ArrayList<ExtendedAttendant>();
		
		final EventAPI eventAPI = new EventAPI(); 
		final AttendantList attendantList = eventAPI.getAttendantList(authHeader, eventId);
		final JSONArray cdvData = getCDVData(authHeader);
		for (final Attendant att: attendantList.getAttendants()) {
			final ExtendedAttendant extendedAttendant = getExtendedAttendant(att, cdvData);		
			extendedAttendants.add(extendedAttendant);
		}
		
		return extendedAttendants;
	}
}
