/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.events.auth.filter;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;

import eu.welive.bb.events.auth.securitycontext.AnonSecurityContext;
import eu.welive.bb.events.auth.securitycontext.OAuthSecurityContext;
import eu.welive.bb.events.auth.token.TokenManager;
import eu.welive.bb.events.auth.user.OAuthUserInfo;

@PreMatching
public class SecurityRequestFilter implements ContainerRequestFilter {

	public static final String AUTHORIZATION_HEADER = "Authorization";
	private static final String BEARER_PREFIX = "Bearer ";
	
	@Override
	public void filter(final ContainerRequestContext requestContext) throws IOException {
		final String authHeader = requestContext.getHeaderString(AUTHORIZATION_HEADER);
		if (authHeader == null)
		{
			requestContext.setSecurityContext(new AnonSecurityContext());
		} else {
			if (authHeader.toLowerCase().startsWith(BEARER_PREFIX.toLowerCase())) {
				final TokenManager tokenManager = new TokenManager();
				final OAuthUserInfo userInfo = tokenManager.validateToken(authHeader);
				final OAuthSecurityContext securityContext = new OAuthSecurityContext(userInfo);
				requestContext.setSecurityContext(securityContext);
			} else {
				requestContext.setSecurityContext(new AnonSecurityContext());
			}
		}
	}
}
