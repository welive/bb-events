/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.events.auth.aac;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.json.JSONObject;

public class AccountProfile {

	public static final String ACCOUNTS = "accounts";
	
	private Map<String, Provider> providers = new HashMap<String, Provider>();
	
	public AccountProfile() {
		
	}
	
	public void loadJSON(JSONObject json) {
		if (json.has(ACCOUNTS)) {
			final JSONObject accounts = json.getJSONObject(ACCOUNTS);
			for (Object key : accounts.keySet()) {
				final String accountType = (String)key;
				final JSONObject account = accounts.getJSONObject(accountType);
				if (accountType.equals(GoogleProvider.ACCOUNT_TYPE)) {
					final Provider provider = new GoogleProvider(account);
					addProvider(provider);
				}
				
				if (accountType.equals(WeLiveProvider.ACCOUNT_TYPE)) {
					final Provider provider = new WeLiveProvider(account);
					addProvider(provider);
				}
				
				if (accountType.equals(FacebookProvider.ACCOUNT_TYPE)) {
					final Provider provider = new FacebookProvider(account);
					addProvider(provider);
				}
			}
		}
	}
		
	public void addProvider(Provider provider) {
		providers.put(provider.getAccountType(), provider);
	}
	
	public Set<String> getProviders() {
		return providers.keySet();
	}
	
	public Set<String> getEmails() {
		final Set<String> emails = new HashSet<String>();
		for (Provider provider : providers.values()) {
			emails.add(provider.getEmail());
		}
		
		return emails;
	}
	
	public String getEmail() {
		final Set<String> emails = getEmails();
		if (!emails.isEmpty()) {
			return emails.iterator().next();
		} else {
			return "";
		}
	}
}
