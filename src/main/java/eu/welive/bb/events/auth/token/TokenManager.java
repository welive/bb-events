/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.events.auth.token;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import eu.welive.bb.events.auth.aac.AACManager;
import eu.welive.bb.events.auth.aac.ExtendedProfile;
import eu.welive.bb.events.auth.user.OAuthUserInfo;
import eu.welive.bb.events.exception.UnexpectedErrorException;

public class TokenManager {
	
	private static final Logger logger = LogManager.getLogger(TokenManager.class);
	
	private final AACManager aacManager;
	
	public TokenManager() {
		aacManager = new AACManager();
	}
	
	protected TokenManager(AACManager aacManager) {
		this.aacManager = aacManager;
	}
	
	public OAuthUserInfo validateToken(String token) {
		if (token == null) {
			return new OAuthUserInfo();
		} else {			
			try {
				final ExtendedProfile extendedProfile = aacManager.getExtendedProfile(token);
				final String weliveID = extendedProfile.getBasicProfile().getUserId();
				final String email = extendedProfile.getAccountProfile().getEmail();
				return new OAuthUserInfo(weliveID, email, token);
			} catch (UnexpectedErrorException e) {
				logger.error("Problem obtaining information from AAC. " + e.getMessage());
				return new OAuthUserInfo();
			}
		}
	}
}
