/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.events.auth.aac;

import org.json.JSONObject;

public abstract class Provider {
	
	public static final String GIVEN_NAME = "eu.trentorise.smartcampus.givenname";
	public static final String SURNAME = "eu.trentorise.smartcampus.surname";
	
	private String givenName = "";
	private String surname = "";
	
	public Provider(String givenName, String surname) {
		this.givenName = givenName;
		this.surname = surname;
	}
	
	public Provider(JSONObject json) {
		if (json.has(GIVEN_NAME)) {
			givenName = json.getString(GIVEN_NAME);
		}
		
		if (json.has(SURNAME)) {
			surname = json.getString(SURNAME);
		}
	}
	
	public abstract String getAccountType();
	
	public String getGivenName() {
		return givenName;
	}

	public String getSurname() {
		return surname;
	}
	
	public abstract String getEmail();
}
