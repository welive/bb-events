/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.events.auth.aac;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONObject;

import eu.welive.bb.events.config.Config;
import eu.welive.bb.events.exception.UnexpectedErrorException;

public class AACManager {
	
	private static final String BASIC_PROFILE_ME = "basicprofile/me";
	private static final String ACCOUNT_PROFILE_ME = "accountprofile/me";
	private static final String AUTHORIZATION_HEADER = "Authorization";

	public BasicProfile getBasicProfile(String token) throws UnexpectedErrorException {
		try {
			final Client client = ClientBuilder.newClient();
			final WebTarget target = client.target(Config.getInstance().getAACAPIURL());
			
			final Response response = target.path(BASIC_PROFILE_ME)
				.request(MediaType.APPLICATION_JSON)
				.header(AUTHORIZATION_HEADER, token)
				.get();
			
			if (response.getStatus() == Status.OK.getStatusCode()) {
				return response.readEntity(BasicProfile.class);			
			} else {
				throw new UnexpectedErrorException("Could not read basic profile from AAC");
			}
		} catch (Exception e) {
			throw new UnexpectedErrorException("Could not read basic profile from AAC"); 
		}
	}
	
	public AccountProfile getAccountProfile(String token) throws UnexpectedErrorException {
		try {
			final Client client = ClientBuilder.newClient();
			final WebTarget target = client.target(Config.getInstance().getAACAPIURL());
			
			final Response response = target.path(ACCOUNT_PROFILE_ME)
				.request(MediaType.APPLICATION_JSON)
				.header(AUTHORIZATION_HEADER, token)
				.get();
			
			if (response.getStatus() == Status.OK.getStatusCode()) {
				final String data = response.readEntity(String.class);
				final JSONObject json = new JSONObject(data);
				final AccountProfile accountProfile = new AccountProfile();
				accountProfile.loadJSON(json);
				return accountProfile;
			} else {
				throw new UnexpectedErrorException("Could not read account profile from AAC");
			}
		} catch (Exception e) {
			throw new UnexpectedErrorException("Could not read account profile from AAC");
		}
	}
	
	public ExtendedProfile getExtendedProfile(String token) throws UnexpectedErrorException {
		final BasicProfile basicProfile = getBasicProfile(token);
		final AccountProfile accountProfile = getAccountProfile(token);
		return new ExtendedProfile(basicProfile, accountProfile);
	}
}
