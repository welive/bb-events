/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.events.api;

import java.io.IOException;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.json.JSONArray;

import eu.welive.bb.events.auth.user.UserInfo;
import eu.welive.bb.events.auth.user.UserRoles;
import eu.welive.bb.events.config.Config;
import eu.welive.bb.events.data.DataWrapper;
import eu.welive.bb.events.data.DataWrapperException;
import eu.welive.bb.events.data.qm.QMConnector;
import eu.welive.bb.events.exception.EntityNotFoundException;
import eu.welive.bb.events.exception.EventException;
import eu.welive.bb.events.exception.ForbiddenErrorException;
import eu.welive.bb.events.exception.UnexpectedErrorException;
import eu.welive.bb.events.profile.ProfileManager;
import eu.welive.bb.events.serialization.Attendant;
import eu.welive.bb.events.serialization.AttendantList;
import eu.welive.bb.events.serialization.BasicEvent;
import eu.welive.bb.events.serialization.Event;
import eu.welive.bb.events.serialization.Event.Language;
import eu.welive.bb.events.serialization.EventList;
import eu.welive.bb.events.serialization.EventTranslation;
import eu.welive.bb.events.serialization.ExtendedAttendant;
import eu.welive.bb.events.serialization.LanguageList;
import eu.welive.bb.events.serialization.Message;
import eu.welive.bb.web.controller.users.UserProfile;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Path("event")
@Api(value = "event")
public class EventAPI {
	
	private static final String OPERATION_SUCCESSFUL = "Operation successful";

	private static final String APPLICATION_JSON_UTF8 = MediaType.APPLICATION_JSON + "; charset=utf-8";
	
	private final DataWrapper dataWrapper;
	private final Language defaultLang;
	private final Set<String> adminUsers;
	
	public EventAPI() throws EventException {
		try {			
			final QMConnector qmConnector = new QMConnector(Config.getInstance().getWeLiveAPI());
			defaultLang = Config.getInstance().getDefaultLang();
			adminUsers = Config.getInstance().getAdminUsers();
			
			dataWrapper = new DataWrapper(Config.getInstance().getDataset(), Config.getInstance().getResource(), qmConnector);
    	} catch (IOException e) {
    		throw new EventException(Status.INTERNAL_SERVER_ERROR.getStatusCode(), "Could not load properties from conf file" + e.getMessage());
    	}
	}
	
	public Language getDefaultLang() {
		return defaultLang;
	}
	
	public EventAPI(DataWrapper dataWrapper, List<String> adminUsers) {
		this.dataWrapper = dataWrapper;
		this.adminUsers = Config.getInstance().getAdminUsers();
		this.defaultLang = Language.ES;
	}
	
	boolean isValidDate(String date) {
		final SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		try {
			parser.parse(date);
			return true;
		} catch (ParseException e) {
			return false;
		}
	}
	
	boolean isValidRange(String start, String end) {
		final SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		
		try {
			final Date startDate = parser.parse(start);
			final Date endDate = parser.parse(end);			
			return endDate.after(startDate);
		} catch (ParseException e) {
			return false;
		}
	}

	@GET
	@Path("/search")
	@Produces(APPLICATION_JSON_UTF8)
	@ApiOperation(value = "Obtains the list of avaible events filtered by date or location", 
		response = EventList.class)
	public EventList getEventList(
			@ApiParam(value = "Optional. Range beginning date to filter events based on their start date")
			@QueryParam("startFrom") @DefaultValue("") String startFrom,
			@ApiParam(value = "Optional. Range finishing date to filter events based on their start date")
			@QueryParam("startTo") @DefaultValue("") String startEnd,
			@ApiParam(value = "Optional. North latitude value to filter events")
			@QueryParam("north") @DefaultValue("") String north,
			@ApiParam(value = "Optional. South latitude value to filter events")
			@QueryParam("south") @DefaultValue("") String south,
			@ApiParam(value = "Optional. East latitude value to filter events")
			@QueryParam("east") @DefaultValue("") String east,
			@ApiParam(value = "Optional. West latitude value to filter events")
			@QueryParam("west") @DefaultValue("") String west,
			@ApiParam(value = "The language for the retrieved events")
			@QueryParam("lang") @DefaultValue("ES") Language lang) throws EventException {
		
		if (!startFrom.isEmpty() && !isValidDate(startFrom)) {
			throw new EventException(Status.INTERNAL_SERVER_ERROR.getStatusCode(), "Invalid range starting date. Use yyyy-MM-ddTHH:mm:ss");
		}
		
		if (!startEnd.isEmpty() && !isValidDate(startEnd)) {
			throw new EventException(Status.INTERNAL_SERVER_ERROR.getStatusCode(), "Invalid range ending date. Use yyyy-MM-ddTHH:mm:ss");
		}
		
		if (!startFrom.isEmpty() && !startEnd.isEmpty() && !isValidRange(startFrom, startEnd)) {
			throw new EventException(Status.INTERNAL_SERVER_ERROR.getStatusCode(), "Invalid date range. Ending date must be greater than starting date");
		}
		
		try {
			final List<Event> events = dataWrapper.filterEvents(startFrom, startEnd, north, south, east, west, lang);
			return new EventList(events);
		} catch (DataWrapperException e) {
			throw new EventException(e);
		}
	}
	
	void checkEventData(BasicEvent event) throws EventException {		
		if (event.getStart().isEmpty() || !isValidDate(event.getStart())) {
			throw new EventException(Status.INTERNAL_SERVER_ERROR.getStatusCode(), "Invalid starting date. Use yyyy-MM-ddTHH:mm:ss");
		}
		
		if (event.getEnd().isEmpty() || !isValidDate(event.getEnd())) {
			throw new EventException(Status.INTERNAL_SERVER_ERROR.getStatusCode(), "Invalid ending date. Use yyyy-MM-ddTHH:mm:ss");
		}
	}
	
	@PUT
	@Path("/{id}")
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@Consumes(APPLICATION_JSON_UTF8)
	@Produces(APPLICATION_JSON_UTF8)
	@ApiOperation(value = "Updates an existing event basic information. Authorization required",
		response = Message.class)
	public Message updateEvent(@Context SecurityContext sc,
		@PathParam("id") int id,
		BasicEvent event) throws EventException {
		
		return updateEvent(((UserInfo)sc.getUserPrincipal()).getAuthToken(), id, event);
	}
	
	public Message updateEvent(String authHeader, int id, BasicEvent event) throws EventException {	
		checkEventData(event);
		
		try {
			event.setId(id);
			dataWrapper.getQMConnector().setAuthHeader(authHeader);
			dataWrapper.updateEvent(event);
			return new Message(OPERATION_SUCCESSFUL);
		} catch (DataWrapperException e) {
			throw new EventException(e);
		}
	}
	
	@PUT
	@Path("/{id}/translation/{lang}")
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@Consumes(APPLICATION_JSON_UTF8)
	@Produces(APPLICATION_JSON_UTF8)
	@ApiOperation(value = "Updates an translation. Authorization required",
		response = Message.class)
	public Message updateTranslation(@Context SecurityContext sc,
		@PathParam("id") int eventId,
		@PathParam("lang") Language lang,
		EventTranslation eventTranslation) throws EventException {
		
		return updateTranslation(((UserInfo)sc.getUserPrincipal()).getAuthToken(), eventId, lang, eventTranslation);
	}
	
	public Message updateTranslation(String authHeader, int id, Language lang, EventTranslation event) throws EventException {		
		try {
			event.setEventId(id);
			dataWrapper.getQMConnector().setAuthHeader(authHeader);
			dataWrapper.updateTranslation(event, lang);
			return new Message(OPERATION_SUCCESSFUL);
		} catch (DataWrapperException e) {
			throw new EventException(e);
		}
	}
	
	@POST
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@Consumes(APPLICATION_JSON_UTF8)
	@Produces(APPLICATION_JSON_UTF8)
	@ApiOperation(value = "Adds a new event using the default language (ES). Authorization required.",
		response = Message.class)
	public Message saveEvent(@Context SecurityContext sc,
		Event event) throws EventException {
				
		return saveEvent(((UserInfo)sc.getUserPrincipal()).getAuthToken(), event);
	}
	
	public Message saveEvent(String authHeader, Event event) throws EventException {
		checkEventData(event);
		
		try {			
			dataWrapper.getQMConnector().setAuthHeader(authHeader);
			event.setLang(defaultLang);
			dataWrapper.saveEvent(event);
			return new Message(OPERATION_SUCCESSFUL);
		} catch (DataWrapperException e) {
			throw new EventException(e);
		}
	}
	
	@DELETE
	@Path("/{id}")
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@ApiOperation(value = "Deletes the specified event. Authorization required.",
		response = Message.class)
	public Message deleteEvent(@Context SecurityContext sc,
			@PathParam("id") int id) throws EventException {
		return deleteEvent(((UserInfo)sc.getUserPrincipal()).getAuthToken(), sc.getUserPrincipal().getName(), id);
	}
	
	public Message deleteEvent(String authHeader, String userId, int id) throws EventException {
		if (!adminUsers.contains(userId)) {
			throw new ForbiddenErrorException();
		}
		
		try {
			dataWrapper.getQMConnector().setAuthHeader(authHeader);
			final int updatedRows = dataWrapper.deleteEvent(id);
			if (updatedRows > 0) {
				return new Message(OPERATION_SUCCESSFUL); 
			} else {
				throw new EntityNotFoundException("Could not find event with id: " + id);
			}
		} catch (DataWrapperException e) {
			throw new EventException(e);
		}
	}
	
	@POST
	@Path("/{id}/translation/{lang}")
	@Consumes(APPLICATION_JSON_UTF8)
	@Produces(APPLICATION_JSON_UTF8)
	@ApiOperation(value = "Adds the translation to the specified event",
		response = Message.class)
	public Message saveTranslation(@Context SecurityContext sc,
		@PathParam("id") int id,
		@PathParam("lang") Language lang,
		EventTranslation eventTranslation) throws EventException {
		
		return saveTranslation(((UserInfo)sc.getUserPrincipal()).getAuthToken(), id, lang, eventTranslation);
	}
	
	public Message saveTranslation(String authHeader, int eventId, Language lang, EventTranslation eventTranslation) throws EventException {			
		try {
			final Event e = dataWrapper.getEvent(eventId, lang);
			if (e.getId() == 0) {
				throw new EntityNotFoundException("Could not find event with id: " + eventId);
			} else {
				dataWrapper.getQMConnector().setAuthHeader(authHeader);
				eventTranslation.setEventId(eventId);
				dataWrapper.saveTranslation(eventTranslation, lang);
				return new Message(OPERATION_SUCCESSFUL); 
			}
		} catch (DataWrapperException e) {
			throw new EventException(e);
		}
	}
	
	@DELETE
	@Path("/{id}/translation/{lang}")
	@Consumes(APPLICATION_JSON_UTF8)
	@Produces(APPLICATION_JSON_UTF8)
	@ApiOperation(value = "Deletes the specified translation from the specified event. Authorization required.", 
		response = Message.class)
	public Message deleteTranslation(@Context SecurityContext sc,
			@PathParam("id") int id, 
			@PathParam("lang") Language lang) throws EventException {
		return deleteTranslation(((UserInfo)sc.getUserPrincipal()).getAuthToken(), sc.getUserPrincipal().getName(), id, lang);
	}
	
	public Message deleteTranslation(String authHeader, String userId, int id, Language lang) throws EventException {
		if (!adminUsers.contains(userId)) {
			throw new ForbiddenErrorException();
		}
		
		try {
			dataWrapper.getQMConnector().setAuthHeader(authHeader);
			final int updatedRows = dataWrapper.deleteTranslation(id, lang);
			if (updatedRows > 0) {
				return new Message(OPERATION_SUCCESSFUL); 
			} else {
				throw new EntityNotFoundException("Could not find translation with lang: " + lang + " for event with id: " + id);
			}
		} catch (DataWrapperException e) {
			throw new EventException(e);
		}
	}
	
	@GET
	@Path("/{id}")
	@Produces(APPLICATION_JSON_UTF8)
	@ApiOperation(value = "Obtains an existing event with the specified translation",
		response = Event.class)
	public Event getEvent(@PathParam("id") int id, @QueryParam("lang") @DefaultValue("ES") Language lang) throws EventException {	
		try {
			final Event event = dataWrapper.getEvent(id, lang);
			if (event.getId() == 0) {
				throw new EntityNotFoundException("Could not find event with id: " + id);
			} else {
				return event;
			}
		} catch (DataWrapperException e) {
			throw new EventException(e);
		}
	}
	
	@GET
	@Path("/{id}/attendant/all")
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@Produces(APPLICATION_JSON_UTF8)
	@ApiOperation(value = "Obtains the list of attendants to the event. Authorization required.",
		response = AttendantList.class)
	public AttendantList getAttendantList(@Context SecurityContext sc,
			@PathParam("id") int id) throws EventException {
		return getAttendantList(((UserInfo)sc.getUserPrincipal()).getAuthToken(), id);
	}
	
	public AttendantList getAttendantList(String authToken, int id) throws EventException {
		try {
			final Event event = dataWrapper.getEvent(id, Language.ES);
			if (event.getId() == 0) {
				throw new EntityNotFoundException("Could not find event with id: " + id);
			} else {
				dataWrapper.getQMConnector().setAuthHeader(authToken);
				final List<Attendant> attendants = dataWrapper.getAttendants(id);
				return new AttendantList(attendants);
			}
		} catch (DataWrapperException e) {
			throw new EventException(e);
		}
	}
	
	@GET
	@Path("/{id}/attendant/me")
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@Produces(APPLICATION_JSON_UTF8)
	@ApiOperation(value = "Obtains the list of events that a user is registered to. Authorization required.",
		response = AttendantList.class)
	public AttendantList getAttendantList(@Context SecurityContext sc) throws EventException {
		final UserInfo userInfo = (UserInfo) sc.getUserPrincipal();
		return getUserEvents(userInfo.getAuthToken(), userInfo.getName());
	}
	
	public AttendantList getUserEvents(String authToken, String userId) throws EventException {
		try {
			dataWrapper.getQMConnector().setAuthHeader(authToken);
			final List<Attendant> attendants = dataWrapper.getUserEvents(userId);
			return new AttendantList(attendants);
		} catch (DataWrapperException e) {
			throw new EventException(e);
		}
	}
	
	@PUT
	@Path("/{id}/attendant/{userId}")
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@Produces(APPLICATION_JSON_UTF8)
	@ApiOperation(value = "Adds the specified user as attendant to the event. Authorization required.",
		response = Message.class)
	public Message addUserAsAttendant(@Context SecurityContext sc,
			@PathParam("id") int eventId,
			@PathParam("userId") String userId) throws EventException  {
		
		return addAttendant(((UserInfo)sc.getUserPrincipal()).getAuthToken(), eventId, userId);
	}
	
	@PUT
	@Path("/{id}/attendant/me")
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@Produces(APPLICATION_JSON_UTF8)
	@ApiOperation(value = "Adds the current user as attendant to the event. Authorization required.",
		response = Message.class)
	public Message addUserAsAttendant(@Context SecurityContext sc,
			@PathParam("id") int eventId) throws EventException  {
		
		return addAttendant(((UserInfo)sc.getUserPrincipal()).getAuthToken(), eventId, sc.getUserPrincipal().getName());
	}
	
	public Message addAttendant(String authHeader, int eventId, String userId) throws EventException {
		try {
			final Event event = dataWrapper.getEvent(eventId, Language.ES);
			if (event.getId() == 0) {
				throw new EntityNotFoundException("Could not find event with id: " + eventId);
			} else {
				dataWrapper.getQMConnector().setAuthHeader(authHeader);
				if (!dataWrapper.isAttendant(eventId, userId)) {
					dataWrapper.addAttendant(eventId, userId);
				}
				return new Message(OPERATION_SUCCESSFUL);
			}
		} catch (DataWrapperException e) {
			throw new EventException(e);
		}
	}
	
	@DELETE
	@Path("/{id}/attendant/me")
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@Produces(APPLICATION_JSON_UTF8)
	@ApiOperation(value = "Removes the current user as attendant from the event. Authorization required.",
		response = Message.class)
	public Message removeUserAsAttendant(@Context SecurityContext sc, 
			@PathParam("id") int eventId) throws EventException {
		
		return removeAttendant(((UserInfo)sc.getUserPrincipal()).getAuthToken(), eventId, sc.getUserPrincipal().getName());
	}
	
	@DELETE
	@Path("/{id}/attendant/{userId}")
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@Produces(APPLICATION_JSON_UTF8)
	@ApiOperation(value = "Removes the specified attendant from the event. Authorization required.",
		response = Message.class)
	public Message removeUserAsAttendant(@Context SecurityContext sc, 
			@PathParam("id") int eventId,
			@PathParam("userId") String userId) throws EventException {

		return removeAttendant(((UserInfo)sc.getUserPrincipal()).getAuthToken(), eventId, userId);
	}
	
	public Message removeAttendant(String authHeader, int eventId, String userId) throws EventException {
		try {
			final Event event = dataWrapper.getEvent(eventId, Language.ES);
			if (event.getId() == 0) {
				throw new EntityNotFoundException("Could not find event with id: " + eventId);
			} else {
				dataWrapper.getQMConnector().setAuthHeader(authHeader);
				if (dataWrapper.isAttendant(eventId, userId)) {
					dataWrapper.removeAttendant(eventId, userId);
					return new Message(OPERATION_SUCCESSFUL);
				} else {
					throw new EntityNotFoundException("Could not find attendant to event: " + eventId + " with user_id: " + userId);
				}
			}
		} catch (DataWrapperException e) {
			throw new EventException(e);
		}
	}
	
	@PUT
	@Path("/{id}/attendant/{userid}/checkin")
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@Produces(APPLICATION_JSON_UTF8)
	@ApiOperation(value = "Marks the specified attendant as checked in. Authorization required.",
		response = Message.class)
	public Message checkinAttendant(@Context SecurityContext sc, 
			@PathParam("id") int eventId,
			@PathParam("userid") String userId) throws EventException {
		if (!adminUsers.contains(sc.getUserPrincipal().getName())) {
			throw new ForbiddenErrorException();
		}
		
		try {
			final Event event = dataWrapper.getEvent(eventId, Language.ES);
			if (event.getId() == 0) {
				throw new EntityNotFoundException("Could not find event with id: " + eventId);
			} else {
				dataWrapper.getQMConnector().setAuthHeader(((UserInfo)sc.getUserPrincipal()).getAuthToken());
				if (dataWrapper.isAttendant(eventId, userId)) {
					dataWrapper.checkinAttendant(eventId, userId);
					return new Message(OPERATION_SUCCESSFUL);
				} else {
					throw new EntityNotFoundException("Could not find attendant to event: " + eventId + " with user_id: " + userId);
				}	
			}
		} catch (DataWrapperException e) {
			throw new EventException(e);
		}
	}
	
	@PUT
	@Path("/{id}/attendant/{userid}/checkout")
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@Produces(APPLICATION_JSON_UTF8)
	@ApiOperation(value = "Marks the specified attendant as checkout in. Authorization required.", 
		response = Message.class)
	public Message checkoutAttendant(@Context SecurityContext sc,
			@PathParam("id") int eventId,
			@PathParam("userid") String userId) throws EventException {
		if (!adminUsers.contains(sc.getUserPrincipal().getName())) {
			throw new ForbiddenErrorException();
		}
		
		try {
			final Event event = dataWrapper.getEvent(eventId, Language.ES);
			if (event.getId() == 0) {
				throw new EntityNotFoundException("Could not find event with id: " + eventId);
			} else {
				dataWrapper.getQMConnector().setAuthHeader(((UserInfo)sc.getUserPrincipal()).getAuthToken());
				if (dataWrapper.isAttendant(eventId, userId)) {
					dataWrapper.checkoutAttendant(eventId, userId);
					return new Message(OPERATION_SUCCESSFUL);
				} else {
					throw new EntityNotFoundException("Could not find attendant to event: " + eventId + " with user_id: " + userId);
				}	
			}
		} catch (DataWrapperException e) {
			throw new EventException(e);
		}
	}
	
	@PUT
	@Path("/{id}/attendant/{userid}/accept")
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@Produces(APPLICATION_JSON_UTF8)
	@ApiOperation(value = "Marks the specified attendant as accepted. Authorization required.",
		response = Message.class)
	public Message acceptAttendant(@Context SecurityContext sc, 
			@PathParam("id") int eventId,
			@PathParam("userid") String userId) throws EventException {
		if (!adminUsers.contains(sc.getUserPrincipal().getName())) {
			throw new ForbiddenErrorException();
		}
		
		try {
			final Event event = dataWrapper.getEvent(eventId, Language.ES);
			if (event.getId() == 0) {
				throw new EntityNotFoundException("Could not find event with id: " + eventId);
			} else {
				dataWrapper.getQMConnector().setAuthHeader(((UserInfo)sc.getUserPrincipal()).getAuthToken());
				if (dataWrapper.isAttendant(eventId, userId)) {
					dataWrapper.acceptAttendant(eventId, userId);
					return new Message(OPERATION_SUCCESSFUL);
				} else {
					throw new EntityNotFoundException("Could not find attendant to event: " + eventId + " with user_id: " + userId);
				}	
			}
		} catch (DataWrapperException e) {
			throw new EventException(e);
		}
	}
	
	@PUT
	@Path("/{id}/attendant/{userid}/reject")
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@Produces(APPLICATION_JSON_UTF8)
	@ApiOperation(value = "Marks the specified attendant as rejected. Authorization required.", 
		response = Message.class)
	public Message rejectAttendant(@Context SecurityContext sc,
			@PathParam("id") int eventId,
			@PathParam("userid") String userId) throws EventException {
		if (!adminUsers.contains(sc.getUserPrincipal().getName())) {
			throw new ForbiddenErrorException();
		}
		
		try {
			final Event event = dataWrapper.getEvent(eventId, Language.ES);
			if (event.getId() == 0) {
				throw new EntityNotFoundException("Could not find event with id: " + eventId);
			} else {
				dataWrapper.getQMConnector().setAuthHeader(((UserInfo)sc.getUserPrincipal()).getAuthToken());
				if (dataWrapper.isAttendant(eventId, userId)) {
					dataWrapper.rejectAttendant(eventId, userId);
					return new Message(OPERATION_SUCCESSFUL);
				} else {
					throw new EntityNotFoundException("Could not find attendant to event: " + eventId + " with user_id: " + userId);
				}	
			}
		} catch (DataWrapperException e) {
			throw new EventException(e);
		}
	}
	
	@GET
	@Path("/event/{id}/translation/all")
	@Produces(APPLICATION_JSON_UTF8)
	@ApiOperation(value = "Marks the specified attendant as checkout in. Authorization required.",
		response = LanguageList.class)
	public LanguageList getTranslations(@PathParam("id") int eventId) throws EventException {
		try {
			final Event event = dataWrapper.getEvent(eventId, Language.ES);
			if (event.getId() == 0) {
				throw new EntityNotFoundException("Could not find event with id: " + eventId);
			} else {
				final LanguageList languageList = new LanguageList();
				languageList.setLanguages(dataWrapper.getAvailableTranslations(eventId));
				return languageList;
			}
		} catch (DataWrapperException e) {
			throw new EventException(e);
		}
	}
	
	final String getCategory(String category) {
		switch (category) {
			case "company":	return "empresa";
			case "entrepreneur": return "emprendedor";
			default: return "";
		}
	}
		
	public String getCSV(String authHeader, int eventId) throws EventException {			
		final StringWriter out = new StringWriter();
		
		final String[] headers = new String[] { "Categoría", "Contacto", "E-mail", "Tlfno", "Confirmado", "Asistente" };
		final AttendantList attendantList = getAttendantList(authHeader, eventId);
		try (final CSVPrinter printer = CSVFormat.DEFAULT.withHeader(headers).print(out)) {			
			
			final ProfileManager profileManager = new ProfileManager();
			final JSONArray cdvData = profileManager.getCDVData(authHeader);
			for (final Attendant att: attendantList.getAttendants()) {
				final ExtendedAttendant extendedAttendant = profileManager.getExtendedAttendant(att, cdvData);
				final String fullName = extendedAttendant.getContactPerson();
				final String phone = extendedAttendant.getPhone();
				final String accepted = att.isAccepted()? "Si": "No";
				final String checkin = att.isCheckin()? "Si": "No";
				final String category = getCategory(extendedAttendant.getCategory());
				printer.printRecord(new Object[] { category, fullName, extendedAttendant.getEmail(), phone, accepted, checkin });
			}
			
			return out.toString();
			
		} catch (IOException e) {
    		throw new UnexpectedErrorException(e.getMessage());
		}
	}

	public String getAllUsersCSV(String authHeader) throws EventException {
		final StringWriter out = new StringWriter();
		final String[] headers = new String[] { "Categoría", "Contacto", "E-mail", "Tlfno", "Web o RSS", "Sector Actividad", "Actividad Empresa", "Nombre Corporativo", "Nombre Comercial"};
		
		try (final CSVPrinter printer = CSVFormat.DEFAULT.withHeader(headers).print(out)) {
			final ProfileManager profileManager = new ProfileManager();
			final List<UserProfile> users = profileManager.getAllUsers(authHeader);
			for (final UserProfile user : users) {
				final String fullName = user.getContactPerson();
				final String email = user.getEmail();
				final String category = user.getCategory();
				final String phone = user.getPhone();
				final String webOrRRSS = user.getWebOrRss();
				final String activitySector = user.getActivitySector();
				final String companyActivity = user.getCompanyActivity();
				final String corporateName = user.getCorporateName();
				final String commercialName = user.getCommercialName();
				printer.printRecord(new Object[] { category, fullName, email, phone, webOrRRSS, activitySector, companyActivity, corporateName, commercialName });
			}
			
			return out.toString();
		} catch (IOException e) {
    		throw new UnexpectedErrorException(e.getMessage());
		}
	} 
	
}
