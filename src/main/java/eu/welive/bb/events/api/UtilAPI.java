/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.events.api;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import eu.welive.bb.events.exception.EventException;

@Path("util")
public class UtilAPI {
	
	private static String weliveApi;
	
	public UtilAPI() throws EventException {
		try {
			final Properties p = new Properties();
			try (final InputStream in = getClass().getResourceAsStream("/conf.properties")) {
				p.load(in);
			}
			
			weliveApi = p.getProperty("weliveapi");
			
		} catch (IOException e) {
    		throw new EventException(Status.INTERNAL_SERVER_ERROR.getStatusCode(), "Could not load properties from conf file" + e.getMessage());
    	}
	}

	@GET
	@Path("/basicprofile/me")
	public String getBasicProfile(@HeaderParam("Authorization") String authToken) {
		final Client client = ClientBuilder.newClient();
		final WebTarget target = client.target(weliveApi).path("/aac/basicprofile/me");
		
		final Response response = target.request(MediaType.APPLICATION_JSON)
									.header("Authorization", authToken)
									.get();
		
		return response.readEntity(String.class);
	}
}
