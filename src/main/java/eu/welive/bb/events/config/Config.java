/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.events.config;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;

import eu.welive.bb.events.serialization.Event.Language;

public class Config {

	private static Config instance = null;
	
	public static Config getInstance() {
		if (instance == null) {
			instance = new Config();
		}
		
		return instance;
	}

	private String apiPath;
	private String weliveAPI;
	private String aacServiceURL;
	private String clientId;
	private String clientSecret;
	
	private String bbSurveyURL;
	private String bbBigKlubURL;
	
	private URL baseURL;
	private String homePath;
	
	private Set<String> adminUsers;
	private Language defaultLang;
	private String dataset;
	private String resource;
	
	private String notifierToken;
	
	protected Config() {
		final Properties p = new Properties();
		try (final InputStream in = Config.class.getResourceAsStream("/conf.properties")) {
			p.load(in);
			
			apiPath = p.getProperty("apipath");
			weliveAPI = p.getProperty("weliveapi");
			aacServiceURL = p.getProperty("aacserviceurl");
			clientId = p.getProperty("clientid");
			clientSecret = p.getProperty("clientsecret");
			baseURL = new URL(p.getProperty("baseurl"));
			bbSurveyURL = p.getProperty("bbsurveyurl");
			bbBigKlubURL = p.getProperty("bigklubbburl");
			homePath = p.getProperty("homepath");
			
			final String users = p.getProperty("adminusers");
			adminUsers = getAdminUsers(users);
			
			final String defaultlangStr = p.getProperty("defaultlang");
			defaultLang = Language.valueOf(defaultlangStr);
			
			dataset = p.getProperty("dataset");
			resource = p.getProperty("resource");
			
			notifierToken = p.getProperty("notifiertoken");
		} catch (IOException e) { 
			e.printStackTrace();
		}
	}
	
	protected static Set<String> getAdminUsers(String users) {
		final Set<String> adminUsers = new HashSet<String>();
		
		final StringTokenizer tokenizer = new StringTokenizer(users, ",");
		while(tokenizer.hasMoreTokens()) {
			adminUsers.add(tokenizer.nextToken());
		}
		
		return adminUsers; 
	}
	
	public String getWeLiveAPI() {
		return weliveAPI;
	}

	public String getAacServiceURL() {
		return aacServiceURL;
	}
	
	public String getAACAPIURL() {
		return weliveAPI + "/aac";
	}

	public String getClientId() {
		return clientId;
	}

	public String getClientSecret() {
		return clientSecret;
	}
	
	public String getBigKlubBBURL() {
		return bbBigKlubURL;
	}

	public String getBBSurveyURL() {
		return bbSurveyURL;
	}
	
	public String getHost() {
		return baseURL.getHost() + ((baseURL.getPort() == -1)?"":(":" + baseURL.getPort()));
	}
	
	public String getHomeURL() {
		return baseURL + homePath;
	}
	
	public String getSwaggerPath() {
		return baseURL.getPath() + apiPath;
	}
	
	public Set<String> getAdminUsers() {
		return adminUsers;
	}
	
	public Language getDefaultLang() {
		return defaultLang;
	}
	
	public String getDataset() {
		return dataset;
	}
	
	public String getResource() {
		return resource;
	}
	
	public String getNotifierToken() {
		return notifierToken;
	}
}
