var mymap;
var marker;
var onClickCallback;

var defaultZoom = 15;
var disabled = false;

var mapInit = false;

function initMap(coords, onClick) {
  if (!mapInit) {
    onClickCallback = onClick;
    mymap = L.map('mapid').setView(coords, defaultZoom);

    L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/streets-v10/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoidWFndWlsZXIiLCJhIjoiY2lsYzY2d2NiMDA2OHZxbHo4YmthbHZlaiJ9.ibhGE5H1Qz2HgLRP4y4ajg', {
      maxZoom: 18,
      attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
        '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
        'Imagery © <a href="http://mapbox.com">Mapbox</a>',
    }).addTo(mymap);

    marker = L.marker(coords).addTo(mymap);

    mymap.on('click', onMapClick);

    mapInit = true;
  }
}

function disableInteraction() {
  disabled = true;
  mymap.dragging.disable();
  mymap.touchZoom.disable();
  mymap.doubleClickZoom.disable();
  mymap.scrollWheelZoom.disable();
  mymap.boxZoom.disable();
  mymap.keyboard.disable();
  $(".leaflet-control-zoom").css("visibility", "hidden");
}

function centerMap(lat, lng) {
  var coords = [lat, lng];
  mymap.setView(coords, defaultZoom);
  marker.remove();
  marker = L.marker(coords).addTo(mymap);
}

function onMapClick(e) {
  if (!disabled) {
    centerMap(e.latlng.lat, e.latlng.lng);
    onClickCallback(e.latlng.lat, e.latlng.lng);
  }
}
