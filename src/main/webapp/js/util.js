function getEventID(url) {
  var startIndex = url.indexOf("event_id=");
  if (startIndex > -1) {
    var event_id = url.substring(startIndex + "event_id=".length, url.length);
    return event_id;
  } else {
    return 0;
  }
}

function postEventData(event, edit) {
  $.ajax({
    type: "POST",
    url: "saveEvent?edit=" + edit,
    data: JSON.stringify(event),
    contentType: "application/json; charset=utf-8",
    success: function(data) {
      changesSaved();
    },
    error: function(data) {
      errorMessage("Error. No se han podido guardar los datos");
    }
  });
}

function editButton(eventId) {
  location.href = 'editEvent?event_id=' + eventId;
}

function listAttendants(eventId) {
  location.href = 'listAttendants?event_id=' + eventId;
}

function langClick(e) {
  location.href = 'editEvent?event_id=' + $("#id").text() + '&lang=' + $(e).text();
}

function checkinUser(eventId, userId) {
  var putURL = "api/event/" + eventId + "/attendant/" + userId + "/checkin";

  $.ajax({
    type: "PUT",
    url: putURL,
    headers: {
        'Authorization': 'Bearer ' + $("#token").val()
    },
    success: function() {
      location.href = 'listAttendants?event_id=' + eventId;
    },
    error: function(data) {
      errorMessage("Error. No se han podido guardar los datos");
    }
  });
}

function checkoutUser(eventId, userId) {
  var putURL = "api/event/" + eventId + "/attendant/" + userId + "/checkout";

  $.ajax({
    type: "PUT",
    url: putURL,
    headers: {
        'Authorization': 'Bearer ' + $("#token").val()
    },
    success: function() {
      location.href = 'listAttendants?event_id=' + eventId;
    },
    error: function(data) {
      errorMessage("Error. No se han podido guardar los datos");
    }
  });
}

function clickCheckinCheckBox(eventId, userId) {
  if ($("#checkin-checkBox-" + userId).prop("checked")) {
    checkinUser(eventId, userId);
  } else {
    checkoutUser(eventId, userId);
  }
}

function acceptUser(eventId, userId) {
  var putURL = "api/event/" + eventId + "/attendant/" + userId + "/accept";

  $.ajax({
    type: "PUT",
    url: putURL,
    headers: {
        'Authorization': 'Bearer ' + $("#token").val()
    },
    success: function() {
      location.href = 'listAttendants?event_id=' + eventId;
    },
    error: function(data) {
      errorMessage("Error. No se han podido guardar los datos");
    }
  });
}

function rejectUser(eventId, userId) {
  var putURL = "api/event/" + eventId + "/attendant/" + userId + "/reject";

  $.ajax({
    type: "PUT",
    url: putURL,
    headers: {
        'Authorization': 'Bearer ' + $("#token").val()
    },
    success: function() {
      location.href = 'listAttendants?event_id=' + eventId;
    },
    error: function(data) {
      errorMessage("Error. No se han podido guardar los datos");
    }
  });
}

function clickAcceptedCheckBox(eventId, userId) {
  if ($("#accepted-checkBox-" + userId).prop("checked")) {
    acceptUser(eventId, userId);
  } else {
    rejectUser(eventId, userId);
  }
}

function pendingChanges() {
  $("#message").removeClass("green");
  $("#message").addClass("red");
  $("#saveButton").prop("disabled", false);
  $("#message").text("Hay cambios pendientes de guardar");
}

function changesSaved() {
  $("#saveButton").prop("disabled", true);
  $("#message").removeClass("red");
  $("#message").addClass("green");
  $("#message").text("Cambios guardados correctamente");
}

function errorMessage(message) {
  $("#message").removeClass("red");
  $("#message").text(message);
}

function removeTimePeriod(num) {
  $("#schedule-group-" + num).remove();
  pendingChanges();
}

function createDaySelector(id) {
  var select = $("<select/>");
  select.attr("id", id);
  select.addClass("form-control");

  var days = {
    'MONDAY': 'Lunes',
    'TUESDAY': 'Martes',
    'WEDNESDAY': 'Miércoles',
    'THURSDAY': 'Jueves',
    'SATURDAY': 'Sábado',
    'SUNDAY': 'Domingo'
  };

  var keys = Object.keys(days);
  for (var i = 0; i < keys.length; i++) {
    var key = keys[i];

    var option = $("<option/>");
    option.attr("value", keys[i]);
    option.text(days[key]);
    select.append(option);
  }

  return select;
}

function addDateComponents(mainDiv, type, tNum) {
  var dayDiv = $("<div/>");
  dayDiv.addClass("col-md-4");
  mainDiv.append(dayDiv);

  var select = createDaySelector("timeperiod-day-" + type + "-" + tNum);
  dayDiv.append(select);

  var dateDiv = $("<div/>");
  dateDiv.addClass("col-md-4");
  mainDiv.append(dateDiv);

  var dateInput = $("<input/>");
  dateInput.attr("id", "timeperiod-date-" + type + "-" + tNum);
  dateInput.attr("type", "text");
  dateInput.addClass("form-control");
  addDatePicker(dateInput);
  dateDiv.append(dateInput);

  var timeDiv = $("<div/>");
  timeDiv.addClass("col-md-4");
  mainDiv.append(timeDiv);

  var timeInput = $("<input/>");
  timeInput.attr("id", "timeperiod-time-" + type + "-" + tNum);
  timeInput.attr("type", "text");
  timeInput.addClass("form-control");
  addTimePicker(timeInput);
  timeDiv.append(timeInput);
}

function addTimePeriod() {
  var numTimePeriods = $("[id^='schedule-group-']").length;
  var tNum = numTimePeriods + 1;

  var formGroup = $("<div/>");
  formGroup.attr("id", "schedule-group-" + tNum);
  formGroup.addClass("form-group");

  var startDiv = $("<div/>");
  startDiv.addClass("col-md-5");
  formGroup.append(startDiv);

  addDateComponents(startDiv, "start", tNum);

  var endDiv = $("<div/>");
  endDiv.addClass("col-md-5");
  formGroup.append(endDiv);

  addDateComponents(endDiv, "end", tNum);

  var removeDiv = $("<div/>");
  removeDiv.addClass("col-md-2 text-center");

  var removeButton = $("<input/>");
  removeButton.attr("id", "remove-time-period-" + tNum);
  removeButton.attr("onclick", "removeTimePeriod(" + tNum + ")");
  removeButton.addClass("btn btn-danger valid");
  removeButton.prop("type", "button");
  removeButton.val("-");

  removeDiv.append(removeButton);
  formGroup.append(removeDiv);

  $("#collapse1").append(formGroup);

  pendingChanges();
}

function addDatePicker(e) {
  $(e).datetimepicker({
    format: "Y-m-d",
    timepicker: false,
    todayButton: true,
    startDate:'+2017/01/01',
    dayOfWeekStart: 1,
    onChangeDateTime: function(currentDateTime, $input) {
      var type = $input.attr('id').split('-')[2];
      var num = $input.attr('id').split('-')[3];
      pendingChanges();
    }
  });
}

function addTimePicker(e) {
  $(e).datetimepicker({
    format: "H:i:00",
    datepicker: false,
    todayButton: true,
    startDate:'+2017/01/01',
    dayOfWeekStart: 1,
    onChangeDateTime: function(currentDateTime) {
      pendingChanges();
    }
  });
}
